<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">
<title>
  Java PDF Viewer | Silent Print PDF | View PDF from browser | Print PDF from browser | print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>

</head>

<body bgcolor="#FFFFFF" text="#666699"
topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">

<?php

//Params exteaction
$licenseKey="A8EC-73AF-64A9-0375";
$docList=$_GET["DOC_LIST"];
$docId=$_GET["DOC_ID"];
$printerName=$_GET["PRINTER_NAME"];
$printerNameSubstringMatch=$_GET["PRINTER_NAME_SUBSTRING_MATCH"];
$paper=$_GET["PAPER"];
$copies=$_GET["COPIES"];
$jobName=$_GET["JOB_NAME"];
$showPrinterDialog=$_GET["SHOW_PRINT_DIALOG"];
$autoMatchPaper=$_GET["AUTO_MATCH_PAPER"];
$pageScaling=$_GET["PAGE_SCALING"];
$autoRotateAndCenter=$_GET["AUTO_ROTATE_AND_CENTER"];
$usePrinterMargins=$_GET["IS_USE_PRINTER_MARGINS"];
$singlePrintJob=$_GET["SINGLE_PRINT_JOB"];
$collateCopies=$_GET["COLLATE_COPIES"];
$showErrorDialog=$_GET["SHOW_PRINT_ERROR_DIALOG"];
$password = $_GET["PASSWORD"];
$urlAuthId = $_GET["URL_AUTH_ID"];
$urlAuthPassword = $_GET["URL_AUTH_PASSWORD"];
$printQuality=$_GET["PRINT_QUALITY"];
$side=$_GET["SIDE_TO_PRINT"];
$statusUpdateEnabled=$_GET["STATUS_UPDATE_ENABLED"];
$debug=$_GET["DEBUG"];

$successPage=$_GET["ON_SUCCESS_SHOW_PAGE"];
$successPageTarget=$_GET["ON_SUCCESS_PAGE_TARGET"];
$failurePage=$_GET["ON_FAILURE_SHOW_PAGE"];
$failurePageTarget=$_GET["ON_FAILURE_PAGE_TARGET"];
$serverCallBackUrl=$_GET["SERVER_CALL_BACK_URL"];
$isShowPrintPreview=$_GET["IS_SHOW_PRINT_PREVIEW"];
$viewerPage=$_GET["VIEWER_PAGE"];
$viewerControls=$_GET["VIEWER_CONTROLS"];


///Test the params////
//echo "docList=${docList}<br>\r\n";
//echo "printerName=${printerName}<br>\r\n";
//echo "printerNameSubstringMatch=${printerNameSubstringMatch}<br>\r\n";
//echo "papers=${paper}<br>\r\n";
//echo "copies=${copies}<br>\r\n";
//echo "jobName=${jobName}<br>\r\n";
//echo "showPrinterDialog=${showPrinterDialog}<br>\r\n";
//echo "autoMatchPaper=${autoMatchPaper}<br>\r\n";
//echo "pageScaling=${pageScaling}<br>\r\n";
//echo "autoRotateAndCenter=${autoRotateAndCenter}<br>\r\n";
//echo "usePrinterMargins=${usePrinterMargins}<br>\r\n";
//echo "singlePrintJob=${singlePrintJob}<br>\r\n";
//echo "collateCopies=${collateCopies}<br>\r\n";
//echo "showErrorDialog=${showErrorDialog}<br>\r\n";
//echo "password=${password}<br>\r\n";
//echo "printQuality=${printQuality}<br>\r\n";
//echo "side=${side}<br>\r\n";
//echo "statusUpdateEnabled=${statusUpdateEnabled}<br>\r\n";
//echo "debug=${debug}<br>\r\n";
?>


<BODY link="#009900" vlink="#009900" TEXT="#4b73af" >

<OBJECT
  codeBase="http://java.sun.com/products/plugin/autodl/jinstall-1_4_2_06-windows-i586.cab#Version=1,4,2,06"
  width="100%" height="100%" classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93">
  <PARAM NAME="type" VALUE="application/x-java-applet;version=1.4.2">
  <PARAM NAME="name" VALUE="smartj">
  <PARAM NAME="alt" VALUE="PDF - Print and View PDF from browser">
  <PARAM NAME="CODEBASE" VALUE="browser_lib/">
  <PARAM NAME="CODE" VALUE="com.activetree.pdfprint.WebPdfViewer">
  <PARAM NAME="ARCHIVE" VALUE="">
  <PARAM NAME="cache_option" VALUE="Plugin">
  <PARAM NAME="cache_archive" VALUE="bc.jar,jai_codec.jar,jai_core.jar,sjpdf_license.jar,sjpdf_browser.jar">
  <PARAM NAME="LICENSE_KEY" VALUE=<?php echo "\"${licenseKey}\""; ?>>
  <PARAM NAME="DOC_LIST" VALUE=<?php echo "\"${docList}\""; ?>>
  <PARAM NAME="DOC_ID" VALUE=<?php echo "\"${docId}\""; ?>>
  <PARAM NAME="PAGE_SCALING" VALUE=<?php echo "\"${pageScaling}\""; ?>>
  <PARAM NAME="AUTO_ROTATE_AND_CENTER" VALUE=<?php echo "\"${autoRotateAndCenter}\""; ?>>
  <PARAM NAME="AUTO_MATCH_PAPER" VALUE=<?php echo "\"${autoMatchPaper}\""; ?>>
  <PARAM NAME="IS_USE_PRINTER_MARGINS" VALUE=<?php echo "\"${usePrinterMargins}\""; ?>>
  <PARAM NAME="PRINTER_NAME" VALUE=<?php echo "\"${printerName}\""; ?>>
  <PARAM NAME="PRINTER_NAME_SUBSTRING_MATCH" VALUE=<?php echo "\"${printerNameSubstringMatch}\""; ?>>
  <PARAM NAME="PAPER" VALUE=<?php echo "\"${paper}\""; ?>>
  <PARAM NAME="COPIES" VALUE=<?php echo "\"${copies}\""; ?>>
  <PARAM NAME="COLLATE_COPIES" VALUE=<?php echo "\"${collateCopies}\""; ?>>
  <PARAM NAME="JOB_NAME" VALUE=<?php echo "\"${jobName}\""; ?>>
  <PARAM NAME="SHOW_PRINT_DIALOG" VALUE=<?php echo "\"${showPrinterDialog}\""; ?>>
  <PARAM NAME="SINGLE_PRINT_JOB" VALUE=<?php echo "\"${singlePrintJob}\""; ?>>
  <PARAM NAME="SHOW_PRINT_ERROR_DIALOG" VALUE=<?php echo "\"${showErrorDialog}\""; ?>>
  <PARAM NAME="PASSWORD" VALUE=<?php echo "\"${password}\""; ?>>
  <PARAM NAME="PRINT_QUALITY" VALUE=<?php echo "\"${printQuality}\""; ?>>
  <PARAM NAME="SIDE_TO_PRINT" VALUE=<?php echo "\"${side}\""; ?>>
  <PARAM NAME="STATUS_UPDATE_ENABLED" VALUE=<?php echo "\"${statusUpdateEnabled}\""; ?>>
  <PARAM NAME="DEBUG" VALUE=<?php echo "\"${debug}\""; ?>>

  <PARAM NAME="ON_SUCCESS_SHOW_PAGE" VALUE=<?php echo "\"${successPage}\""; ?>>
  <PARAM NAME="ON_SUCCESS_PAGE_TARGET" VALUE=<?php echo "\"${successPageTarget}\""; ?>>
  <PARAM NAME="ON_FAILURE_SHOW_PAGE" VALUE=<?php echo "\"${failurePage}\""; ?>>
  <PARAM NAME="ON_FAILURE_PAGE_TARGET" VALUE=<?php echo "\"${failurePageTarget}\""; ?>>
  <PARAM NAME="SERVER_CALL_BACK_URL" VALUE=<?php echo "\"${serverCallBackUrl}\""; ?>>
  <PARAM NAME="IS_SHOW_PRINT_PREVIEW" VALUE=<?php echo "\"${isShowPrintPreview}\""; ?>>
  <PARAM NAME="VIEWER_PAGE" VALUE=<?php echo "\"${viewerPage}\""; ?>>
  <PARAM NAME="VIEWER_CONTROLS" VALUE=<?php echo "\"${viewerControls}\""; ?>>

  <COMMENT>
    <EMBED
      type="application/x-java-applet;version=1.4.2"
      name="smartj"
      alt="PDF - Print and View PDF from browser"
      pluginspage="http://java.sun.com/j2se/"
      CODEBASE="browser_lib/"
      CODE="com.activetree.pdfprint.WebPdfViewer"
      ARCHIVE=""
      cache_option="Plugin"
      cache_archive="bc.jar,jai_codec.jar,jai_core.jar,sjpdf_license.jar,sjpdf_browser.jar"
      WIDTH="100%"
      HEIGHT="100%"
      LICENSE_KEY=<?php echo "\"${licenseKey}\"\r\n"; ?>
      DOC_LIST=<?php echo "\"${docList}\"\r\n"; ?>
      DOC_ID=<?php echo "\"${docId}\"\r\n"; ?>
      PAGE_SCALING=<?php echo "\"${pageScaling}\"\r\n"; ?>
      AUTO_ROTATE_AND_CENTER=<?php echo "\"${autoRotateAndCenter}\"\r\n"; ?>
      AUTO_MATCH_PAPER=<?php echo "\"${autoMatchPaper}\"\r\n"; ?>
      IS_USE_PRINTER_MARGINS=<?php echo "\"${usePrinterMargins}\"\r\n"; ?>
      PRINTER_NAME=<?php echo "\"${printerName}\"\r\n"; ?>
      PRINTER_NAME_SUBSTRING_MATCH=<?php echo "\"${printerNameSubstringMatch}\"\r\n"; ?>
      PAPER=<?php echo "\"${paper}\"\r\n"; ?>
      COPIES=<?php echo "\"${copies}\"\r\n"; ?>
      COLLATE_COPIES=<?php echo "\"${collateCopies}\"\r\n"; ?>
      JOB_NAME=<?php echo "\"${jobName}\"\r\n"; ?>
      SHOW_PRINT_DIALOG=<?php echo "\"${showPrinterDialog}\"\r\n"; ?>
      SINGLE_PRINT_JOB=<?php echo "\"${singlePrintJob}\"\r\n"; ?>
      SHOW_PRINT_ERROR_DIALOG=<?php echo "\"${showErrorDialog}\"\r\n"; ?>
      PASSWORD=<?php echo "\"${password}\"\r\n"; ?>
      PRINT_QUALITY=<?php echo "\"${printQuality}\""; ?>
      SIDE_TO_PRINT=<?php echo "\"${side}\""; ?>
      STATUS_UPDATE_ENABLED=<?php echo "\"${statusUpdateEnabled}\""; ?>
      DEBUG=<?php echo "\"${debug}\""; ?>

      ON_SUCCESS_SHOW_PAGE=<?php echo "\"${successPage}\""; ?>
      ON_SUCCESS_PAGE_TARGET=<?php echo "\"${successPageTarget}\""; ?>
      ON_FAILURE_SHOW_PAGE=<?php echo "\"${failurePage}\""; ?>
      ON_FAILURE_PAGE_TARGET=<?php echo "\"${failurePageTarget}\""; ?>
      SERVER_CALL_BACK_URL=<?php echo "\"${serverCallBackUrl}\""; ?>
      IS_SHOW_PRINT_PREVIEW=<?php echo "\"${isShowPrintPreview}\""; ?>
      VIEWER_PAGE=<?php echo "\"${viewerPage}\""; ?>
      VIEWER_CONTROLS=<?php echo "\"${viewerControls}\""; ?>
      >

      <NOEMBED>
        <p>No java runtime</p>
      </NOEMBED>
    </EMBED>
    </COMMENT>
</OBJECT>

</body>
</html>

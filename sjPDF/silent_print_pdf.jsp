<%@ page import="java.io.IOException" %>
<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">

<style type="text/css">
  body {
    margin-top: 5px;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 5px;
  }

  textarea{
    width:100%;
  }  
</style>
<title>
  Silent Print PDF | Silent Print PDF from browser | Print PDF from browser | print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>

<script type="text/javascript" language="javascript">
function showMessage(msg) {
  if (msg=='') {
    document.getElementById("status").innerHTML=msg;
    return;
  }
  if (document.createTextNode){
    var mytext=document.createTextNode(msg+"\r");
    document.getElementById("status").appendChild(mytext);
  } else{
    document.getElementById("status").innerHTML=msg;
  }
}
</script>
</head>

<BODY link="#009900" vlink="#009900" TEXT="#4b73af">

<%
  //License key
  String licenseKey = getLicenseKey(request, response);
  String jSessionId = request.getSession().getId();
  //System.out.println("licenseKey=" + licenseKey);
  String successPage = request.getParameter("ON_SUCCESS_SHOW_PAGE");
  String successPageTarget = request.getParameter("ON_SUCCESS_PAGE_TARGET");
  String failurePage = request.getParameter("ON_FAILURE_SHOW_PAGE");
  String failurePageTarget = request.getParameter("ON_FAILURE_PAGE_TARGET");
  String serverCallBackUrl = request.getParameter("SERVER_CALL_BACK_URL");
  String isShowPrintPreview = request.getParameter("IS_SHOW_PRINT_PREVIEW");
  String viewerPage = request.getParameter("VIEWER_PAGE");
  String viewerControls = request.getParameter("VIEWER_CONTROLS");
  
  //Documents to print
  //TODO -- must use QueryString -- look for NOTE in source code
  String docList = request.getQueryString(); //.getParameter("DOC_LIST");
  //TODO -- end
  //System.out.println("docList=" + docList);
  //out.println("<br>docList: <br>" + docList + "<br>");

  //PAPER to apply as default
  String docId = request.getParameter("DOC_ID");
  String printerName = request.getParameter("PRINTER_NAME");
  String printer_name_substring_match = request.getParameter("PRINTER_NAME_SUBSTRING_MATCH");
  String paper = request.getParameter("PAPER");
  String copies = request.getParameter("COPIES");
  String jobName = request.getParameter("JOB_NAME");
  String showPrinterDialog = request.getParameter("SHOW_PRINT_DIALOG");
  String printDialogTitle = request.getParameter("PRINT_DIALOG_TITLE");
  String autoMatchPaper = request.getParameter("AUTO_MATCH_PAPER");
  String pageScaling = request.getParameter("PAGE_SCALING");
  String autoRotateAndCenter = request.getParameter("AUTO_ROTATE_AND_CENTER");
  String usePrinterMargins = request.getParameter("IS_USE_PRINTER_MARGINS");
  String singlePrintJob = request.getParameter("SINGLE_PRINT_JOB");
  String collateCopies = request.getParameter("COLLATE_COPIES");
  String showErrorDialog = request.getParameter("SHOW_PRINT_ERROR_DIALOG");
  String password= request.getParameter("PASSWORD");
  String urlAuthId = request.getParameter("URL_AUTH_ID");
  String urlAuthPassword = request.getParameter("URL_AUTH_PASSWORD");
  String printQuality = request.getParameter("PRINT_QUALITY");
  String side = request.getParameter("SIDE_TO_PRINT");
  String enablePrintStatusUpdate = request.getParameter("STATUS_UPDATE_ENABLED");
  String debug = request.getParameter("DEBUG");
  String codeBase = getCodeBaseURLString(request);
  String codeBaseUrlString = codeBase + "browser_lib/";
%>

<%!
  String getLicenseKey(HttpServletRequest req, HttpServletResponse res) throws Throwable {
    //Get license key from license file
    RequestDispatcher dp1 = req.getRequestDispatcher("key.jsp");
    dp1.include(req, res);
    String newLKey = (String) req.getAttribute("LICENSE_KEY");
    return newLKey;
  }
%>


<%!
  String getCodeBaseURLString(HttpServletRequest req) throws IOException {
    String contextPath = req.getContextPath();  //e.g. "/sample_app"
    String protocol = req.getProtocol();
    String scheme = req.getScheme();
    if (scheme != null && scheme.length() > 0) {
      protocol = scheme;
    }
    if (protocol != null && protocol.equalsIgnoreCase("https")) {
      protocol = "https";
    } else if (protocol != null && protocol.equalsIgnoreCase("http")) {
      protocol = "http";
    } else {
      protocol = "http";
    }
    String host = req.getServerName();
    String port = ":" + req.getServerPort();
    if (port.equalsIgnoreCase(":80")) {
      port = ""; //need not show port 80
    }
    String baseUrlStr = protocol + "://" + host + port + contextPath + "/";
    return baseUrlStr;
  }
%>


<OBJECT
  codeBase="http://java.sun.com/products/plugin/autodl/jinstall-1_4_2_06-windows-i586.cab#Version=1,4,2,06"
  width="2" height="2" classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93">
  <PARAM NAME="type" VALUE="application/x-java-applet;version=1.4.2">
  <PARAM NAME="name" VALUE="smartj">
  <PARAM NAME="jsessionid" VALUE="<%=jSessionId%>">
  <PARAM NAME="alt" VALUE="PDF - Print and View from browser">
  <PARAM NAME="CODEBASE" VALUE="<%=codeBaseUrlString%>">
  <PARAM NAME="CODE" VALUE="com.activetree.pdfprint.WebSilentPrintPdf">
  <PARAM NAME="ARCHIVE" VALUE="">
  <PARAM NAME="cache_option" VALUE="Plugin">
  <PARAM NAME="cache_archive" VALUE="bc.jar,jai_codec.jar,jai_core.jar,sjpdf_license.jar,sjpdf_browser.jar">
  <PARAM NAME="LICENSE_KEY" VALUE="<%=licenseKey%>">
  <PARAM NAME="DOC_LIST" VALUE="<%=docList%>">
  <PARAM NAME="DOC_ID" VALUE="<%=docId%>">
  <PARAM NAME="PAGE_SCALING" VALUE="<%=pageScaling%>">
  <PARAM NAME="AUTO_ROTATE_AND_CENTER" VALUE="<%=autoRotateAndCenter%>">
  <PARAM NAME="AUTO_MATCH_PAPER" VALUE="<%=autoMatchPaper%>">
  <PARAM NAME="IS_USE_PRINTER_MARGINS" VALUE="<%=usePrinterMargins%>">
  <PARAM NAME="PRINTER_NAME" VALUE="<%=printerName%>">
  <PARAM NAME="PRINTER_NAME_SUBSTRING_MATCH" VALUE="<%=printer_name_substring_match%>">
  <PARAM NAME="PAPER" VALUE="<%=paper%>">
  <PARAM NAME="COPIES" VALUE="<%=copies%>">
  <PARAM NAME="COLLATE_COPIES" VALUE="<%=collateCopies%>">
  <PARAM NAME="JOB_NAME" VALUE="<%=jobName%>">
  <PARAM NAME="SHOW_PRINT_DIALOG" VALUE="<%=showPrinterDialog%>">
  <PARAM NAME="PRINT_DIALOG_TITLE" VALUE="<%=printDialogTitle%>">
  <PARAM NAME="SINGLE_PRINT_JOB" VALUE="<%=singlePrintJob%>">
  <PARAM NAME="SHOW_PRINT_ERROR_DIALOG" VALUE="<%=showErrorDialog%>">
  <PARAM NAME="PASSWORD" VALUE="<%=password%>">
  <PARAM NAME="URL_AUTH_ID" VALUE="<%=urlAuthId%>">
  <PARAM NAME="URL_AUTH_PASSWORD" VALUE="<%=urlAuthPassword%>">
  <PARAM NAME="PRINT_QUALITY" VALUE="<%=printQuality%>">
  <PARAM NAME="SIDE_TO_PRINT" VALUE="<%=side%>">
  <PARAM NAME="STATUS_UPDATE_ENABLED" VALUE="<%=enablePrintStatusUpdate%>">
  <PARAM NAME="ON_SUCCESS_SHOW_PAGE" VALUE="<%=successPage%>">
  <PARAM NAME="ON_SUCCESS_PAGE_TARGET" VALUE="<%=successPageTarget%>">
  <PARAM NAME="ON_FAILURE_SHOW_PAGE" VALUE="<%=failurePage%>">
  <PARAM NAME="ON_FAILURE_PAGE_TARGET" VALUE="<%=failurePageTarget%>">
  <PARAM NAME="SERVER_CALL_BACK_URL" VALUE="<%=serverCallBackUrl%>">
  <PARAM NAME="IS_SHOW_PRINT_PREVIEW" VALUE="<%=isShowPrintPreview%>">
  <PARAM NAME="VIEWER_PAGE" VALUE="<%=viewerPage%>">
  <PARAM NAME="VIEWER_CONTROLS" VALUE="<%=viewerControls%>">
  <PARAM NAME="DEBUG" VALUE="true">

  <COMMENT>
    <EMBED
      type="application/x-java-applet;version=1.4.2"
      name="Java PDF Print"
      jsessionid="<%=jSessionId%>"
      alt="PDF - Print and View PDF from browser"
      pluginspage="http://java.sun.com/j2se/"
      CODEBASE="<%=codeBaseUrlString%>"
      CODE="com.activetree.pdfprint.WebSilentPrintPdf"
      ARCHIVE=""
      cache_option="Plugin"
      cache_archive="bc.jar,jai_codec.jar,jai_core.jar,sjpdf_license.jar,sjpdf_browser.jar"
      WIDTH="2"
      HEIGHT="2"
      LICENSE_KEY="<%=licenseKey%>"
      DOC_LIST="<%=docList%>"
      DOC_ID="<%=docId%>"
      PAGE_SCALING="<%=pageScaling%>"
      AUTO_ROTATE_AND_CENTER="<%=autoRotateAndCenter%>"
      AUTO_MATCH_PAPER="<%=autoMatchPaper%>"
      IS_USE_PRINTER_MARGINS="<%=usePrinterMargins%>"
      PRINTER_NAME="<%=printerName%>"
      PRINTER_NAME_SUBSTRING_MATCH="<%=printer_name_substring_match%>"
      PAPER="<%=paper%>"
      COPIES="<%=copies%>"
      COLLATE_COPIES="<%=collateCopies%>"
      JOB_NAME="<%=jobName%>"
      SHOW_PRINT_DIALOG="<%=showPrinterDialog%>"
      PRINT_DIALOG_TITLE="<%=printDialogTitle%>"
      SINGLE_PRINT_JOB="<%=singlePrintJob%>"
      SHOW_PRINT_ERROR_DIALOG="<%=showErrorDialog%>"
      PASSWORD="<%=password%>"
      URL_AUTH_ID="<%=urlAuthId%>"
      URL_AUTH_PASSWORD="<%=urlAuthPassword%>"
      PRINT_QUALITY="<%=printQuality%>"
      SIDE_TO_PRINT="<%=side%>"
      STATUS_UPDATE_ENABLED="<%= enablePrintStatusUpdate %>"
      ON_SUCCESS_SHOW_PAGE="<%= successPage %>"
      ON_SUCCESS_PAGE_TARGET="<%= successPageTarget %>"
      ON_FAILURE_SHOW_PAGE="<%= failurePage %>"
      ON_FAILURE_PAGE_TARGET="<%= failurePageTarget %>"
      SERVER_CALL_BACK_URL="<%= serverCallBackUrl %>"
      IS_SHOW_PRINT_PREVIEW="<%= isShowPrintPreview %>"
      VIEWER_PAGE="<%= viewerPage %>"
      VIEWER_CONTROLS="<%= viewerControls %>"
      DEBUG="true"
      >

      <NOEMBED>
        <p>No java runtime</p>
      </NOEMBED>
    </EMBED>
  </COMMENT>
</OBJECT>

<!--This section is for message display on browser page -->
<table border="0" width="100%" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <fieldset>
        <legend>Print status </legend>
        <textarea id="status" rows="6" cols="80"  readonly="true"></textarea>
      </fieldset>
    </td>
  </tr>
</table>
<!-- end message display -->

</body>
</html>


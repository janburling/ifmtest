<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.ByteArrayOutputStream" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="java.net.URL" %>

<html>
<head>
<title>
  Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>
</head>

<body bgcolor="#FFFFFF" text="#666699">

<%
  writeOutput(request, response);
%>

<%!
  public void writeOutput(HttpServletRequest request, HttpServletResponse response) throws Throwable {
    //Write the PDF header for the browser
    StringBuffer sbFilename = new StringBuffer();
    sbFilename.append("filename_");
    sbFilename.append(System.currentTimeMillis());
    sbFilename.append(".pdf");

    response.setHeader("Cache-Control", "max-age=30");
    response.setContentType("application/pdf");

    StringBuffer sbContentDispValue = new StringBuffer();
    sbContentDispValue.append("inline");
    sbContentDispValue.append("; filename=");
    sbContentDispValue.append(sbFilename);
    response.setHeader("Content-disposition", sbContentDispValue.toString());

    //Now populate a report model for generating a PDF from.
    byte[] docContent = getDocumentContent(request, response);

    //TODO: Generate the PDF for the browser.
    ServletOutputStream outStream = response.getOutputStream();
    outStream.write(docContent);
    outStream.flush();
    outStream.close();
  }

  public byte[] getDocumentContent(HttpServletRequest req, HttpServletResponse res) throws IOException {
    //You might get parameters value here in order to decide the PDF source;
    //For examples if the PDF is in a Database, static PDF in a private area only a server
    //can access it, PDF content generated from another report API, or some other stream.
    byte[] content = null;

    //TODO - 1: HAVE YOUR OWN IMPLEMENTATION HERE////////////////////////////////
    //TODO - 2: URL might be password protected -- you need to provide authorization
    //For demo purpose this sample reads one of the static PDF in the WAR archive and writes
    //the content back to the browser as reply.
    String codeBase = getCodeBaseURLString(req);
    String sampleDoc = codeBase + "demo/sample_pdf/sample1.pdf";
    URL url = new URL(sampleDoc);
    InputStream in = url.openStream();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    byte buf[] = new byte[40960];
    for (int bytesCount = 0; (bytesCount = in.read(buf)) != -1;) {
      out.write(buf, 0, bytesCount);
    }
    in.close();
    out.close();
    content = out.toByteArray();
    //TODO - END//////////////////////////////////////////////////////////////

    return content;
  }


  String getCodeBaseURLString(HttpServletRequest req) throws IOException {
    String contextPath = req.getContextPath();  //e.g. "/sample_app"
    String protocol = req.getProtocol();
    String scheme = req.getScheme();
    if (scheme != null && scheme.length() > 0) {
      protocol = scheme;
    }
    if (protocol != null && protocol.equalsIgnoreCase("https")) {
      protocol = "https";
    } else if (protocol != null && protocol.equalsIgnoreCase("http")) {
      protocol = "http";
    } else {
      protocol = "http";
    }
    String host = req.getServerName();
    String port = ":" + req.getServerPort();
    if (port.equalsIgnoreCase(":80")) {
      port = ""; //need not show port 80
    }
    String baseUrlStr = protocol + "://" + host + port + contextPath + "/";
    return baseUrlStr;
  }
%>

</body>
</html>
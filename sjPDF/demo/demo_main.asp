<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
  <link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
  <style type="text/css">
    body {
      margin-top: 5px;
      margin-left: 5px;
      margin-right: 5px;
      margin-bottom: 5px;
    }
  </style>
  <title>
    Java PDF Print | Java PDF view| Silent Print PDF | Silent PDF from Java | Silent Print PDF from J2EE | Silent Print PDF from Browser| Print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
  </title>

</head>
<body link="#009900" vlink="#009900" TEXT="#4b73af">

<div class="xxxsquare">
<h2>Java PDF - Silent Print and View PDF from Browser</h2>

100% pure Java API for PDF Printing, Viewing, Convertion of your existing PDF documents.
Convert PDF to documents such as TIFF, Post Script and images such as PNG, BMP, GIF, JPEG/JPG and more.
<br>
<b><font color="red">
  NOTE: You <u>DO NOT</u> need any other software to Print, View and Convert your PDF documents. </font>
</b>
</div>
<p></p>

<div class=square>
  <h2>Silent Print and View PDF from Browser - Samples</h2>
  <li>Demo-1: Silent print PDF from browser web application sample (demo 1).
    (<a href="demo/demo1.asp" class="bottomlinks1" target="window1">Try demo 1</a>) <br>[default printing parameters]
  </li>
  <li>Demo-2: Silent print PDF from browser web application sample where user can select printing parameters. (<a
    href="demo/demo2.asp" class="bottomlinks1" target="window2">Try demo 2</a>) <br>[choose  printing parameters]
  </li>

  <!--
  <li>Demo-3: Silent print dynamically generated PDF from browser - e.g. InputStream, Database or PDF byte[] content from other
    source which can not be referred as an URL directly from the browser. Instead they can be accessed through a server program
    (an URL) in order to access those kind of PDFs. (<a href="demo/demo3.asp" class="bottomlinks1" target="window3">Try demo 3</a>) <br>
  </li>
  <li><font color=red> (New!) </font> Demo: Secure printing and viewing of your PDF documents.
     (<a href="demo/secure_silent_print_demo.asp" class="bottomlinks1" target="window4">Try secure silent print demo</a>) <br>
  </li>
  ->
  
</div>

<div class=square>
  <h2>Tutorial- Browser Based Silent Printing and Viewing PDF</h2>
  It lets you control printing and viewing of the documents by way of passing the parameters name
  and value as part of the URL.
  <p>
    This is a self sufficient WAR archive and do not need any other software for its functioning.
    In most cases this sample will be good enough to solve your need to silent print or view documents
    using this API. However, you might consider learning more about the parameters and how to
    change them so you can change one or more of the existing behavior. In addition this tutorial
    did address some of the basic questions and queries about deploying and customizing it.
    <br><br>
    <a href="docs/silent_print_pdf_from_browser.html" class="bottomlinks1" target="window3">Read Tutorial...</a>
  </p>
</div>

Copyright &#169; 2008 Activetree, Inc. All rights reserved. <br>
Web: <a href="http://www.activetree.com" class="bottomlinks1">http://www.activetree.com</a><br>
Email: sales@activetree.com<br>
Tel: +1 408 716 8414 Fax: +1 408 716 8450<br>

</body>
</html>

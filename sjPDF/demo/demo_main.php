<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->

<html>
<head>
  <link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
  <style type="text/css">
    body {
      margin-top: 5px;
      margin-left: 5px;
      margin-right: 5px;
      margin-bottom: 5px;
    }
  </style>
  <title>
    Java PDF Printing and Viewing API.
  </title>

</head>
<body link="#009900" vlink="#009900" TEXT="#4b73af">

<?php
//empty
?>

<b><font color="red">
NOTE: You <u>DO NOT</u> need any other software to print or view your PDF documents. </font>
</b>

<h2>Silent print and view PDF from browser</h2>
<div class=square>
  <h2>Try these sample web applications</h2>
  <li>Demo-1: Silent print PDF from browser web application sample (demo 1).
    (<a href="demo/demo1.php" class="bottomlinks1" target="window1">Try demo 1</a>) <br>[default printing parameters]
  </li>
  <li>Demo-2: Silent print PDF from browser web application sample where user can select printing parameters. (<a
    href="demo/demo2.php" class="bottomlinks1" target="window2">Try demo 2</a>) <br>[choose the printing parameters]
  </li>
  <!--
  <li>Demo-3: Silent print dynamically generated PDF from browser - e.g. InputStream, Database or PDF byte[] content from other
    source which can not be referred as an URL directly from the browser. Instead they can be accessed through a server program
    (an URL) in order to access those kind of PDFs. (<a href="demo/demo3.php" class="bottomlinks1" target="window3">Try demo 3</a>) <br>
  </li>
  <li><font color=red> (New!) </font> Demo: Secure printing and viewing of your PDF documents.
     (<a href="demo/secure_silent_print_demo.php" class="bottomlinks1" target="window4">Try secure silent print demo</a>) <br>
  </li>
  -->
</div>

<div class=square>
  <h2>Tutorial - Browser Based Silent Printing and Viewing PDF</h2>
  It lets you control printing and viewing of the documents by way of passing the parameters name
  and value as part of the URL.
  <p>
    This is a self sufficient WAR archive and do not need any other software for its functioning.
    In most cases this sample will be good enough to solve your need to silent print or view documents
    using this API. However, you might consider learning more about the parameters and how to
    change them so you can change one or more of the existing behavior. In addition this tutorial
    did address some of the basic questions and queries about deploying and customizing it.
    <br><br>
    <a href="docs/silent_print_pdf_from_browser.html" class="bottomlinks1" target="window3">Read Tutorial...</a>
    </p>
</div>

<i>Copyright &#169; 2008 Activetree, Inc. All rights reserved.</i> <br>
Web: <a href="http://www.activetree.com" class="bottomlinks1">http://www.activetree.com</a><br>
Email: sales@activetree.com<br>
Tel: +1 408 716 8414 Fax: +1 408 716 8450<br>

</body>
</html>
rem compile a class - typically to compile one from demo from the download archive
rem arguments oneOrMoreSourceNames
@echo off
set CLASSPATH=".;.\..\..\..\browser_lib\sjpdf_license.jar;.\..\..\..\browser_lib\bc.jar;.\..\..\..\browser_lib\jai_codec.jar;.\..\..\..\browser_lib\jai_core.jar;.\..\..\..\browser_lib\cidf.jar;.\..\..\..\browser_lib\sjpdf_browser.jar"

echo %JAVA_HOME%/bin/javac -classpath %CLASSPATH% -d . %*
%JAVA_HOME%/bin/javac -classpath %CLASSPATH% -d . %*

/**
 * Copyright � Activetree, Inc.  All rights reserved.
 * http://www.activetree.com
 *
 * This is a sample program demonstrating the usage of the relevant APIs and carries no warranty.
 */
package demo.activetree.pdfprint.browser;

import com.activetree.pdfprint.WebSilentPrintPdf;

public class MySilentPrintFromBrowser extends WebSilentPrintPdf {
  public MySilentPrintFromBrowser() {
    super();
  }

  public void init() {
    super.init();
    //self
  }

  public void start() {
    super.start();
    //self if any
  }

  public void stop() {
    super.stop();
    //self if any
  }

  public void destroy() {
    super.destroy();
    //self if any
  }
}

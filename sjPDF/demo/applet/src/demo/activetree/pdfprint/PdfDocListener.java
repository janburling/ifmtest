/**
 * Copyright � Activetree, Inc.  All rights reserved.
 * http://www.activetree.com
 *
 * This is a sample program demonstrating the usage of the relevant APIs and carries no warranty.
 */
package demo.activetree.pdfprint;

import com.activetree.common.doc.DocEvent;
import com.activetree.common.doc.DefaultDocListener;

/**
 * Demo PDF listener that tracks about PDF loading, viewing and printing.
 */
public class PdfDocListener extends DefaultDocListener {

  protected void pageRenderedForViewer(DocEvent evt, int pageIndex) {
    System.out.println("PAGE RENDERED (for viewing) index of page:" + pageIndex);
  }

  protected void pageRenderedForThumbnailView(DocEvent evt, int thumbnailIndex) {
    System.out.println("PAGE RENDERED (for thumbnail loading) index of page:" + thumbnailIndex);
  }

  protected void pageLoaded(DocEvent evt, int pageIndex) {
    System.out.println("PAGE LOADED (index of page):" + pageIndex);
  }

  protected void pageCount(DocEvent evt, int pageCount) {
    System.out.println(evt.getSource());
    System.out.println("PAGE COUNT: " + pageCount);
  }

  protected void pageConverted(DocEvent evt, int pageCount) {
    Object info = evt.getInfo();
    System.out.println("PAGE RENDERED (for convertion to " + info + ") index " + evt.getValue() + ", page# " + pageCount);
  }

  protected void printStarting(DocEvent evt, Object source, String printerName) {
    System.out.println("Printing STARTING (pages=" + evt.getValue() + ", printerName=" + printerName + ")");
  }

  protected void printableCalled(DocEvent evt, int pageIndex) {
    //todo: when a getPrintable().print(Graphics g) - called by JRE
    System.out.println("Page printed (index of page):" + pageIndex);
  }

  protected void pagePrinted(DocEvent evt, int pageCount) {
    System.out.println("PAGE RENDERED (for printing) index " + evt.getValue() + ", page# " + pageCount);
  }

  protected void printFinished(DocEvent evt, Object source, String printerName) {
    System.out.println("Printing DONE (pages=" + evt.getValue() + ", printerName=" + printerName + ")");
  }

  //already added the failedDoc to the list.
  protected void docError(DocEvent evt, Object failedDoc, int value, Object details) {
    System.out.println("DOC_ERROR: [source=" + failedDoc +"][value="+value +"][message="+details +"]");
  }

  protected void jobFinished(DocEvent evt) {
    Object source = evt.getSource();
    System.out.println("JOB_FINISHED: [source=" + source +"][value="+ evt.getValue() +"][details="+ evt.getDetails() +"]");
    System.out.println("---Job Finished---");
    System.out.println("Following docs processed.\n" + source);
    System.out.println("Pages processed: " + evt.getValue());
    if (failedDocs.size() > 0) {
      System.err.println(">>\nFollwings docs failed. \n" + failedDocs + "\n<<");
    }else {
      System.out.println("All docs successfully processed!");
    }
  }

}
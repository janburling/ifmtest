<%@ page import="java.io.IOException" %>
<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">

<style type="text/css">
  body {
    margin-top: 5px;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 5px;
  }
</style>
<title>
  Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>
</head>

<body link="#009900" vlink="#009900" text="#4b73af">

<%
  String docList = request.getParameter("DOC_LIST");
  String docId = request.getParameter("DOC_ID");
  String doc = docId;
  if (doc == null) {
    doc = docList;
  }
  String date_time = request.getParameter("DATE_TIME");
  String printerName = request.getParameter("PRINTER_NAME");
  String pageCount = request.getParameter("PAGE_COUNT");
  String jobStatus = request.getParameter("JOB_STATUS");
  System.out.println("JOB_STATUS=" + jobStatus);
  String message = "";
  if (jobStatus != null && jobStatus.equalsIgnoreCase("CANCELED")) {
    message = "Print cancelled.";
  }else {
    message = "Successfuly printed to: " + printerName + "<br>\n";
    message += "Total pages printed: " + pageCount + "<br>\n";
    message += "Date and time printed: " + date_time + "<br>\n";
  }
%>
<h3>Print status</h3>
<hr size="0.5>">
Documents: <%=doc%> <br>
<%=message%>
<hr size="0.5>">
</body>
</html>

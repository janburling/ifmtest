<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.io.ObjectInputStream" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.io.ObjectOutputStream" %>
<%@ page import="java.util.Iterator" %>
<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">

<style type="text/css">
  body {
    margin-top: 5px;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 5px;
  }
</style>
<title>
  Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>

</head>
<BODY link="#009900" vlink="#009900" TEXT="#4b73af" >

<%!

  //One can customize a lot of stuff here
  //For example - if some one is printing documents - may control number of copies allowed to print
  public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
    System.out.println("##########  BEGIN - server.service() ##########");

    Map clientParams = null;
    try {
      clientParams = readClientParams(req);
    } catch (Throwable t) {
      t.printStackTrace();
      //TODO: You may process your error here
    }
    if (clientParams == null) {
      clientParams = new HashMap();
    }

    //Called when?
    String requestName = (String)clientParams.get("REQUEST_NAME");
    System.out.println("REQUEST_NAME=" + requestName);

    System.out.println("Client request parameters");
    debugParams(clientParams);

    //Current doc
    Object docList = clientParams.get("DOC_LIST");
    System.out.println("DOCL_LIST=" + docList);

    HashMap replyParams = new HashMap();

    //TODO: If you do not want the argument Document to be printed by client
    //Add a non-empty value for the "SERVER_ERROR_MESSAGE"
    //replyParams.put("SERVER_ERROR_MESSAGE", "-1");  //-1 for not printing it
    //Default is assugmed to be "1"

    //If the document is PASSWORD protected and the URL params did not have PASSWORD param value set that here.
    //replyParams.put("PASSWORD", "currDocPassword");

    //Doc URL if password protected, you can provide that too here
    //replyParams.put("URL_AUTH_ID", "docUrlAuthId");
    //replyParams.put("URL_AUTH_PASSWORD", "urlAuthPassword");  

    //Reply client with params such as Document PASSWORD, URL_AUTH_ID, URL_AUTH_PASSWORD
    //These information may not be embedded with the URL as parameters.
    //Instead they should be replyed when a page like this one is called from the URL
    //TODO


    //Similarly, if client request has DOC_ID parameter with a ID value
    //It is expecting the this server to find the real identification of the doc against the ID
    //you may seek the actual content as byte[] or put the URL itself to the reply map.
    //Return as reply for the client. This way a client in the browser will not know
    //real identify of the document or its secuirty information
    String codeBase = getCodeBaseURLString((HttpServletRequest) req);
    if (requestName.equalsIgnoreCase("DOC_ID")) {
      String docId = (String) clientParams.get("DOC_ID");
      System.out.println("docId=" + docId);

      //Waht is the ID - it may not matter here - just a demo
      //for your application you will have a map maintained somewhere
      //key of the Map is this ID set in the URL ; ?DOC_ID=ID
      //When it comes down to here by a call from client- get the value using
      //qId and return it in the replyMap.

      //SO what you will do is something like this
      //String docId = (String)clientParams.get("DOC_ID");
      //Object hiddenDoc = myDocIdMap.get(docId);

      //For DEMO sample we are hard coding some here.
      //Lets always test with a sample real PDF under demo directory
      String existingPdfUrlStr = codeBase + "/demo/sample_pdf/sample3.pdf";
      //OR
      //this URL JSP page that dynamically generated one when called.
      String dynaPdfUrlStr = codeBase + "/" + "demo/stream_print_demo.jsp";

      String doc = dynaPdfUrlStr;
      String docPassword = null;
      String docUrlAuthId = null;
      String docUrlAuthPassword = null;

      //QString identify
      //        (<a class="bottomlinks1" onMouseDown="javascript:document.secure.currdoc.value=document.secure.f2.value;" href="javascript:printDoc('
      //Set the qString for client
      replyParams.put("DOC_LIST", doc);
      replyParams.put("PASSWORD", docPassword); //or some value
      replyParams.put("URL_AUTH_ID", docUrlAuthId); //or some value
      replyParams.put("URL_AUTH_PASSWORD", docUrlAuthPassword); //or some value
      System.out.println("sent...[" + doc +"][pass=" + docPassword +"][docUrlAuthId=" +docUrlAuthId +"][docUrlAuthPassword="+docUrlAuthPassword +"]");
    }

    //debug server params
    System.out.println("Reply parameters"); // + replyParams);
    debugParams(replyParams);

    try {
      sendReplyToClient(res, replyParams);
    } catch (Throwable t) {
      //t.printStackTrace();
      //TODO: You may process the error here
      System.out.println("sendClientReply() got exception: " + t.getMessage());
    }
    System.out.println("##########  END - server.service() ##########");
  }

  protected Map readClientParams(ServletRequest req) throws Throwable {
    ObjectInputStream in = new ObjectInputStream(req.getInputStream());
    Object clientReqObject = in.readObject();
    Map params = (Map) clientReqObject;
    in.close();
    return params;
  }

  protected void sendReplyToClient(ServletResponse res, Object replyData) throws Throwable {
    ObjectOutputStream out = new ObjectOutputStream(res.getOutputStream());
    out.writeObject(replyData);
    out.flush();
    out.close();
  }

  private void debugParams(Map paramsMap) {
    if (paramsMap == null || paramsMap.size() == 0) {
      System.out.println("Empty params.");
      return;
    }
    Iterator keyIter = paramsMap.keySet().iterator();
    while (keyIter.hasNext()) {
      Object key = keyIter.next();
      Object value = paramsMap.get(key);
      System.out.println(key + "=" + value);
    }
  }

  String getCodeBaseURLString(HttpServletRequest req) throws IOException {
    String contextPath = req.getContextPath();  //e.g. "/sample_app"
    String protocol = req.getProtocol();
    String scheme = req.getScheme();
    if (scheme != null && scheme.length() > 0) {
      protocol = scheme;
    }
    if (protocol != null && protocol.equalsIgnoreCase("https")) {
      protocol = "https";
    } else if (protocol != null && protocol.equalsIgnoreCase("http")) {
      protocol = "http";
    } else {
      protocol = "http";
    }
    String host = req.getServerName();
    String port = ":" + req.getServerPort();
    if (port.equalsIgnoreCase(":80")) {
      port = ""; //need not show port 80
    }
    String baseUrlStr = protocol + "://" + host + port + contextPath; // + "/";
    return baseUrlStr;
  }
%>

</body>
</html>

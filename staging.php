<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/../yii_113/framework/yii.php';
$config=dirname(__FILE__).'/staging/config/main.php';

// remove the following line when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);

require_once($yii);
Yii::createWebApplication($config)->run();

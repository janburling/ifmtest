<?php 

$modelName = $model->modelName;

$tabs = array();
        
$tabModel = new ReportDefinition;
$tabId = $tabModel->modelName.'-tab';
$tabModel->reportFolderId = $model->reportFolderId;
$tabs[$tabId] =
        array(
			'id'=>$tabId,
			'label'=>$tabModel->modelsLabel,
			'content'=>$this->renderPartial('/common/_grid', array('model'=>$tabModel), true),
			'active'=>!$activeTab || $activeTab == $tabId,
        );

$this->widget('TbTabs', array('tabs'=>$tabs, 'encodeLabel'=>false));

echo '<div class="form-actions">';
	$this->widget('TbButton', array('buttonType'=>'link', 'type'=>'primary', 'label'=>'Update', 'url'=>array('update', 'id'=>$model->primaryKey)));
	$this->widget('TbButton', array('buttonType'=>'link', 'label'=>'Clone', 'url'=>array('clone', $model->primaryKeyName=>$model->primaryKey)));
echo '</div>';
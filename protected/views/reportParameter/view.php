<?php 

$modelName = $model->modelName;

$this->breadcrumbs = array(
	$model->ReportDefinition->ReportFolder->name=>array('/reportFolder/view', 'id'=>$model->ReportDefinition->reportFolderId),
	$model->ReportDefinition->name=>array('/reportDefinition/view', 'id'=>$model->reportDefinitionId),
	$model->name,
);

$tabs = array();

$tabId = $modelName.'-tab'; 
$tabs[$tabId] = 
	array(
		'id'=>$tabId,
		'label'=>'Detail',
		'content'=>$this->renderPartial('/common/_detail', array('model'=>$model), true),
		'active'=>!$activeTab || $activeTab == $tabId,
	);

$this->widget('TbTabs', array('tabs'=>$tabs, 'encodeLabel'=>false));

echo '<div class="form-actions">';
	$this->widget('TbButton', array('buttonType'=>'link', 'type'=>'primary', 'label'=>'Update', 'url'=>array('update', 'id'=>$model->primaryKey)));
	$this->widget('TbButton', array('buttonType'=>'link', 'label'=>'Clone', 'url'=>array('clone', $model->primaryKeyName=>$model->primaryKey)));
echo '</div>';
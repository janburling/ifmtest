<?php
	echo $form->dropdownListRow($model, 'reportDefinitionId',
		CHtml::listData(ReportDefinition::model()->sort()->findAll(),'reportDefinitionId','name')
	);
	echo $form->textFieldRow($model,'name',array('maxlength'=>45));
	echo $form->textFieldRow($model,'value',array('maxlength'=>45));
	echo $form->checkboxRow($model,'promptUser');
	echo $form->checkboxRow($model,'isStatic');
	echo $form->checkboxRow($model,'allowMultipleValues');
	echo $form->checkboxRow($model,'isDate');
<script type="text/javascript" src="/assets/1ddc9772/jquery.js"></script>
<script type="text/javascript" src="/assets/1ddc9772/jquery.yii.js"></script>

<?php
	$this->renderFile('assets/jqgrid/include.html');
	$listModel = $this->createModel();
?>

<h2><?php echo $listModel->modelLabel ?> Pick List</h2>
<?php $listModel->listType = 'pick' ?>
<script type="text/javascript"> 
$(document).ready(function() {
	var mygrid = jQuery("#jqgrid1").jqGrid({ 
	    theme: 'redmond',
	    mtype: 'POST',
		datatype: 'xml',
	    useNavBar: true,
	    multiselect: true,
	    viewrecords: true,
	    viewsortcols: true,
	    gridview: true,
		width: '100%',
		height: '100%',
        onSelectRow: function()
        {
        	pickIt( jQuery('#jqgrid1').getGridParam('selarrrow'),
                jQuery('#jqgrid1').getRowData( jQuery('#jqgrid1').getGridParam('selarrrow') )['name']);
         },
        rowNum: <?php echo Yii::app()->params['defaults']['listRowSize']; ?>,
	    rowList: [20,100,500],
		pager: '#jqpager1',
		url:'<?php
		// remove empty values or arrays, as they cause issues in createUrl
		$listDataParams = array('ids'=>array(), 'showNavigation'=>'false', 'fromListPick'=>'true');
		foreach($listDataParams as $index=>$value)
		{
			if( $value == '' || ( is_array($value) && empty($value) ) )
			{
				unset( $listDataParams[$index] );
			}
		}
		echo $this->createUrl('listXml', $listDataParams ); ?>',
		caption:'<?php echo $listModel->modelsLabel; ?>',
	    sortname: '<?php echo $listModel->defaultSort['name']; ?>',
		sortorder: '<?php echo $listModel->defaultSort['order']; ?>',
		colNames:[ <?php
		$vals = array();
		foreach($listModel->getListViewAttributes('pick') as $columnName=>$columnValues)
		{
			$vals[] =  "'{$columnValues['label']}'";
		}
		echo implode( ',', $vals);?>],
		colModel:[
			<?php
			$vals = array();
			foreach($listModel->getListViewAttributes('pick') as $columnName=>$columnValues)
			{
				$isHidden = isset($columnValues['hidden']) ? ", hidden: true" : '';
				if( isset($hideNavigation) )
				{
					$isSearchable	= ", search: false";
				}
				elseif( isset($columnValues['search']) )
				{
					$isSearchable	= ", search: " . ($columnValues['search']?'true':'false' );
				}
				else
				{
					$isSearchable	= '';
				}
				
				// add drop down search
				$isBool = $listModel->getFieldType($columnName) == 'bool';
				if( isset($columnValues['searchDropDown']) || $isBool )
				{
					$dropValues		= isset($columnValues['searchDropDown'])? $columnValues['searchDropDown'] : array('true'=>'Yes', 'false'=>'No');  
					$dropdown		= array(':-- All --');
					foreach( $dropValues as $key=>$value ) $dropdown[] = "$key:$value"; 
					$dropdown		= implode(';', $dropdown);
					$isSearchable	=", search: 'true', stype: 'select', editoptions:{value:\"{$dropdown}\"}";
				}
				
				$vals[]="{name:'" . $columnName . "', " .
						"index:'" . $columnName . "', " .
						"width:'" . $columnValues['width'] . "' " .
						$isHidden .
						$isSearchable .  
						"}"; 
			}
			echo implode( ',', $vals);
			?>
		] 
	})
	.navGrid('#jqgrid1',{edit:false,add:false,del:false,search:false,refresh:false})
	.navButtonAdd("#jqpager1",{caption:"Toggle",title:"Toggle Search Toolbar", buttonicon :'ui-icon-pin-s',
		onClickButton:function(){
			mygrid[0].toggleToolbar();
		}
	})
	.navButtonAdd("#jqpager1",{caption:"Clear",title:"Clear Search",buttonicon :'ui-icon-refresh',
		onClickButton:function(){
			mygrid[0].clearToolbar();
		}
	});
	mygrid.filterToolbar();	 
});

</script>

<table id="jqgrid1"></table>
<div id="jqpager1"></div>
<div id="filter" style="margin-left:30%;display:none">Search</div>

<script type="text/javascript">
function pickIt(id, name) {
	var <?php echo "idElement = '" . $_GET['idElement'] . "';\n"; ?>
	var <?php echo "nameElement = '" . $_GET['nameElement'] . "';\n"; ?>
	//alert( 'id: ' + id + ', name:' + name);
	window.opener.document.getElementById(idElement).value = id;
	window.opener.document.getElementById(nameElement).value = name;
	window.close();
};
</script>

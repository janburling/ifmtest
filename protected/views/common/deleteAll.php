<?php

	echo '<h2>Delete matched ' . $model->modelsLabel . '?</h2>'; //header

	echo CHtml::beginForm( array('deleteAll'), 'post', array('id'=>'deleteAll') ) .
					CHtml::hiddenField( 'criteria', $criteria, array('id' => 'deleteAllCriteria') ) .
					CHtml::hiddenField( 'confirmDeleteAll' ) .
					CHtml::submitButton( 'Delete All' ) .					
					CHtml::endForm();
	//$this->renderPartial('/common/showActionBar', array('model'=>$model));
	
?>

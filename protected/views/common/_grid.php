
<?php 

echo '<div class="form-actions" style="
	padding: 4px 20px 4px;
	margin-top: 0px;
	margin-bottom: 0px;
	border-bottom: 1px solid #e5e5e5;
">';
echo $model->gridActions;
echo '</div>';

$this->widget('TbGridView', array(
	'id'=>$model->modelName.'-index',
    'type'=>'striped condensed',
    'dataProvider'=>$model->search(),
	'filter'=>$model->hideFilters ? null : $model,
    'template'=>"{summary}{pager}{items}{pager}",
	'columns'=>$model->gridColumns,
));

echo '<div class="form-actions">';
echo $model->gridActions;
echo '</div>';

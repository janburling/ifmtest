<?php 

$form = $this->beginWidget('TbActiveForm',array(
	'id'=>'employee-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
));

echo $form->errorSummary($model);	
$this->renderPartial('_form', array('model'=>$model, 'form'=>$form));

echo '<div class="form-actions">';
	$this->widget('TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Save'));
	$this->widget('TbButton', array('buttonType'=>'button', 'label'=>'Cancel', 
		'htmlOptions'=>array('submit'=>Yii::app()->user->returnUrl)));
echo '</div>';
	
$this->endWidget();
<?php
if( $model->modelName == 'Payroll' && $model->status == 'L')
	Yii::app()->clientScript->registerScript("$model->primaryKey-refresh", "setTimeout('location.reload(true);', 10000);", CClientScript::POS_LOAD);
	
$form = $model->form;
echo "<h2>View {$model->modelLabel} - {$model->name}</h2>";

$this->beginClip('Information');

$this->renderPartial('/common/showActionBar', array('model'=>$model));

//echo $model->form->renderShowElementsGroup();
//$form->renderTabs = 0; // columns 
if( $form->renderTabs )
{
	$this->renderGroupsTabs( $model->showFieldsGrouping, $form, false );
}
else
{
	$this->renderGroupsColumns( $model->showFieldsGrouping, $form, false );
}

$this->renderPartial('/common/showActionBar', array('model'=>$model));

$this->endClip();

	$this->beginClip('Report');
		$this->renderPartial('/common/contextReports', array('model'=>$model));	
	$this->endClip();	
	
	$clips		= $this->clips;	
	$viewData	= array ('model'=>$model,);
	$tabs		= array (
				'tab1'=>array('title'=>'Information'	, 'content'=>$clips['Information']),
				'tab4'=>array('title'=>'Reports'		, 'content'=>$clips['Report']),
	);
 
	$this->widget('CTabView', array('tabs'=>$tabs, 'viewData'=>$viewData));
?>
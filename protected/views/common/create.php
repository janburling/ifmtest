<?php 

$form = $this->beginWidget('TbActiveForm',array(
	'id'=>'standard-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'enableClientValidation'=>true,
));

echo $form->errorSummary($model);	

echo "<h3>Create {$model->modelLabel} </h3>";

$this->renderPartial('_form', array('model'=>$model, 'form'=>$form));

echo '<div class="form-actions">';
	$this->widget('TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Save'));
	$this->widget('TbButton', array('buttonType'=>'submit', 'label'=>'Save and Clone', 'htmlOptions'=>array('name'=>'saveAndClone')));
	echo ' ';
	$this->widget('TbButton', array('buttonType'=>'linkButton', 'label'=>'Cancel',
		'url'=>isset($returnUrl) ? $returnUrl : Yii::app()->user->returnUrl
	));
echo '</div>';
	
$this->endWidget();
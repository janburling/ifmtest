<?php

			
		$reports = ReportDefinition::model()->sort()->findAllByAttributes(array('contextObject'=>$model->modelName));
		foreach($reports as $report) {
				$params = array();
				if($report->isExtractReport) {

					if($model->hasAttribute('entityId')) $entityId = $model->entityId;
					elseif($model->modelName == 'Entity') $entityId = $model->id; 
					$params[] 							= 'BillExtract/createAll';
					$params['redirectId'] 				= $report->id;
					$params['redirectAction']			= urlencode('/reportDefinition/ExecuteReport/contextIds/' . $model->primaryKey . '/salt/' . time() );
					$params['BillExtract[entityIds]']	= isset($entityId) ? array($entityId) : array(); 
					if(isset($model->asOfDate))
						$params['BillExtract[asOfDate]']	= $model->asOfDate ? Yii::app()->dateFormatter->format('yyyy-MM-dd', $model->asOfDate) : date('Y-m-d');
				}
				else { //no extract
					$params = array();					
					$params[] 							= 'ReportDefinition/executeReport';
					$params[$report->primaryKeyName]	= $report->primaryKey;
					$params['contextIds']				= $model->primaryKey;
					$params['salt']						= time();
					if( isset($model->asOfDate))
					{
						$params['BillExtract[asOfDate]']	= $model->asOfDate ? Yii::app()->dateFormatter->format('yyyy-MM-dd', $model->asOfDate) : date('Y-m-d');
					}
				}					
				
			  	echo '<p>' . 
					CHtml::link($report->name,
						$params, 
						array('target'=>'_blank')
					) .
					'</p>';
			
		}
		if( !$reports )
		{
			echo '<p>No reports found</p>';
		}
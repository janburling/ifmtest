<?php $withForm = isset($withForm)? $withForm : true;?>
<script language="javascript" type="text/javascript">
skipformatCurrency = 0;
function formatCurrency(num)
{
	if( skipformatCurrency && num == '' )
	{
		return num;
	}
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + '$' + num + '.' + cents);
}

function getDollarValue( str ){
	if ( str == '' )
	{
		return 0;
	}
	var dollarValue	= str.replace("$", '' );
	dollarValue		= dollarValue.replace(",", '' );
	dollarValue		= parseFloat( dollarValue );
	return dollarValue;	
}

function pickIt(object, idElement, nameElement){
  var url = "/" + object + "/listPick/?";
  url += "idElement=" + idElement + "&";
  url += "nameElement=" + nameElement;
  window.open(url,"pickAnEntity","alwaysRaised=yes,toolbar=no,menubar=no,status=no,resizable=yes,width=600,height=500");
  //window.open(url,"Pick an Entity","alwaysRaised=yes,toolbar=no,menubar=no,status=no,resizable=yes,width=600,height=500");
}

function clearZeroField( ele ){	
	if( getDollarValue( $(ele).val() ) == 0 )
	{
		$(ele).val( '' );
	}
}

</script>

<?php 
// custom javascript for Payment model
if( $model->modelName == 'Payment'){ ?>
<script type="text/javascript">
function updatePaymentFields( ele ){
	// skip if default payment method was updated
	if ( $(ele).attr('id') !=  defaultMethodId )
	{
		var othersTotal = 0;
		paymentFields.each(function(index, item) {
			if( $(item).attr('id') !=  defaultMethodId )
			{
				othersTotal	+= getDollarValue( $(item).val() );
			}		
		});
		
		var newDefaultValue = totalDue - othersTotal;
		if( newDefaultValue < 0 )
		{
			newDefaultValue = 0;
		}
		$( '#' + defaultMethodId )
		.val( formatCurrency( newDefaultValue ) );
	}
	updateTotal();
}

function updateTotal(){	
	var total = 0;
	paymentFields.each(function(index, item) {
		total	+= getDollarValue( $(item).val() );
	});
	
	totalField.val( formatCurrency( total ) );		
	dueField.val( formatCurrency( totalDue - total ) );
}

// on load
$(function() {
	// blank out paid by fields
	alreadyBlanked	= 0; // global

	// update totals
	paymentFields	= $( '#Payment_cash, #Payment_check, #Payment_creditCard, #Payment_moneyOrder, #Payment_otherAmount');
	defaultMethodId = 'Payment_<?php echo Organization::model()->findByPk(Yii::app()->user->organizationId)->defaultPaymentMethod; ?>';

	totalField		= $( '#Payment_amountPaid' );
	dueField		= $( '#Payment_amountDue' );
	//statusField	= $( '#Payment_status' );
	totalField.attr(	'disabled', true);
	dueField.attr(		'disabled', true);
	//statusField.attr(	'disabled', true);
		
	totalDue		= parseFloat(<?php echo $model->amountDue; ?>);

	/*
	// clear zero fields on focus
	paymentFields.focus(function(){
		if( getDollarValue( $(this).val() ) == 0 )
		{
			$(this).val( '' );
		}
	});
	*/

	// paid by fields
	inputDefaults	= [];
	paidByFields	= $( "input[id^=Payment_paidBy]");

	paidByFields.focus(function(){
		if( alreadyBlanked || $(this).val() == '' )
		{
			return;
		}
		paidByFields.each(function(index, item) {			
			$(item).val('');
			alreadyBlanked	= 1;
		});
	});
	// store default values and disable fields, only in create view (not update)
	<?php if( !$update ){?>
	paidByFields.each(function(index, item) {
		inputDefaults[ $(item).attr( 'id' ) ] = $(item).val();			
		$(item).attr( 'disabled', true);
	});
	
	$( '#overridePaidBy' ).change( function(){
		paidByFields.each(function(index, item) {
			var newState = !($(item).attr('disabled')); 			
			$(item).attr( 'disabled', newState );
			if( newState == true )
			{
				alreadyBlanked	= 0;
				$(item).val( inputDefaults[ $(item).attr( 'id' ) ] );
			}
		});
	});
	<?php } // end if( $update )?>

	paymentFields.change( function(){
		updatePaymentFields( this );
	});

	// capture form submission
	$( '#payForm').submit( function(){
		updateTotal();
		// case overpayment
		if( getDollarValue( dueField.val() ) < 0 ){
			var absValue	= Math.abs( getDollarValue(dueField.val()) );  
			var isConfirm	= confirm('Change due: ' +
					formatCurrency( absValue ) +
					"\nClick OK if change given or \nCancel to add overpayment");
			$( "#Payment_isChangeDue" ).val( isConfirm );
		}
		// case partial payment
		if( getDollarValue( dueField.val() ) > 0 ){
			var partialConfirm	= confirm('Partial payment: ' +
					"\nClick OK to proceed \nCancel to go back");
			return partialConfirm;
		}
		//alert('reached: before end, return nothing'); // debugging
		//return false;
	});
	updateTotal();

}); // end document ready


</script>

<?php } // end if( $model->modelName == 'Payment') ?>

<?php if( $withForm ) {?>

<div class="yiiForm">
<p>
Fields with <span class="required">*</span> are required.
</p>
<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>

<?php } // end if( $withForm ) ?>

<?php 
	
	$currentColumn = 1;
	$maxColumn = 1;

	echo '<table class="columns"><tr>';
	
	while($currentColumn <= $maxColumn) {

		echo '<td>';
		foreach($model->viewAttributes as $fieldName=>$fieldAttributes) {
			//check whether to use tabs and if to display on this tab
			$displayFieldOnTab = false;
			if(isset($tab)) { // is the model view defining a tab?
				if(isset($fieldAttributes['tab'])) { // does the field define a tab?
					if($tab === $fieldAttributes['tab']) // is the field's tab the one being requested by the model view?
						$displayFieldOnTab = true; // display field if tabs match
				}
				else $displayFieldOnTab = true; // if the field doesn't doesn't define a tab, display field
			}
			else $displayFieldOnTab = true;  // if the model view doesn't use tabs, display field
	
			#if($fieldAttributes['type'] == 'hidden') $displayFieldOnTab = false;		
		
			// skip special fields needed only in special cases

			$htmlOptions	= array();
			$fieldColumn	= isset($fieldAttributes['column']) 		? $fieldAttributes['column']		: 0;			
			$displayField	= isset($fieldAttributes['readOnly'])		? !$fieldAttributes['readOnly']		: true;			
			$hidden			= isset($fieldAttributes['hidden']) 		? $fieldAttributes['hidden']		: false;
			$isPersistent	= isset($fieldAttributes['isPersistent'])	? $fieldAttributes['isPersistent'] 	: true;
			$isVisible 		= isset($fieldAttributes['visible']) 		? $fieldAttributes['visible'] 		: true;
			
			if( $withForm	&& isset($fieldAttributes['withoutFormOnly'])	) continue; // non persistent, without form tags
			if( $update		&& !$isPersistent		) continue; // non persistent, in create page only
			if( !$isVisible	) continue; 
			
			if(isset($fieldAttributes['disabled'])) $htmlOptions['readonly'] = $fieldAttributes['disabled'];			
			
			if($fieldColumn > $maxColumn) $maxColumn = $fieldColumn;
			
			if($fieldColumn == $currentColumn && $displayField && $displayFieldOnTab) {
				
				echo '<div class="simple">';
							
				if($fieldAttributes['type'] == 'blank') {
					echo CHtml::label('-', 'false');
				}
								
				elseif($fieldAttributes['type'] == 'date') {
					echo CHtml::activeLabelEx($model, $fieldName);
					$this->widget('application.extensions.jui.EDatePicker',
						array_merge(
							Yii::app()->params['defaults']['datePicker'],
							array(
								'name'	=> $model->tableName() . '[' . $fieldName . ']',
				            	'value'	=> ($model->$fieldName ? Yii::app()->dateFormatter->format('MM/dd/yyyy', $model->$fieldName) : ''),
							)
						)
					);
				}

				// dependent combos
				elseif($fieldAttributes['type'] == 'dependentList') {
					echo
						CHtml::activeLabelEx($model, $fieldName) .
						CHtml::activeDropDownList($model, $fieldName, $fieldAttributes['options'],
					array(
					'prompt'=> '[Select]',
					'ajax'	=> array(
								'type'		=>'POST', //request type
								'url'		=>'/' . $model->modelName .  '/dynamic' . ucfirst($fieldAttributes['dependentField']), //url to call
								'update'	=>'#' . $model->modelName . '_' . $fieldAttributes['dependentField'], //selector *id* to update
								//'data'=>'js:javascript statement' 
								//leave out the data key to pass all form values through
								)
					)); 
				}
				
				elseif($fieldAttributes['type'] == 'keyList') {
					$relatedModel = $model->getRelatedModel($fieldName);
					$criteria = new CDbCriteria(array('order'=>'"' . $relatedModel->defaultSort['name'] . '" ' . $relatedModel->defaultSort['order']));
					
					//Yii::trace(print_r($criteria, true));
 					echo
 						CHtml::activeLabelEx($model, $fieldName) . 
						CHtml::activeDropDownList(
							$model,
							$fieldName . 'Id',
							CHtml::listData($relatedModel->filtered()->findAll($criteria),
								'id','name'),
							array('empty'=>'')
						);
				}

				elseif($fieldAttributes['type'] == 'list') {
					if(!$model->$fieldName && isset($fieldAttributes['options'][0]))
						$model->$fieldName = 0;
					$htmlOptions = array();
					
					if(isset($fieldAttributes['multiple'])) $htmlOptions['multiple'] = 'true';
					else $htmlOptions['prompt'] = '[Select]';
					
 					echo
 						CHtml::activeLabelEx($model, $fieldName) . 
						CHtml::activeDropDownList(
							$model,
							$fieldName,
							$fieldAttributes['options'],
							$htmlOptions
						);
				}
								
				elseif($fieldAttributes['type'] == 'keyLookup') {
					$lookupId = $fieldName . 'Id';
					$lookupName = $fieldName . 'Name';
					if(!$hidden) {
	 					echo					
	 						CHtml::activeLabelEx($model, $fieldName) . 
	 						CHtml::textField($model->modelName . '_' . $lookupName, $model->$lookupId ? $model->$lookupName : '', array('disabled'=>'disabled')) .
	 						CHtml::activeHiddenField($model, $lookupId ) .
							CHtml::link('Pick', '#', array('onclick'=>'pickIt("' . $fieldName . '", "' . $model->tableName() . '_' . $lookupId . '", "' . $model->tableName() . '_' . $lookupName . '")'));
					}
					else echo CHtml::activeHiddenField($model, $lookupId);
				}
				
				elseif($fieldAttributes['type'] == 'textArea') {
					echo
						CHtml::activeLabelEx($model, $fieldName) .
						CHtml::activeTextArea($model,$fieldName);
				}
				
				elseif($fieldAttributes['type'] == 'currency') {
					$model->$fieldName = Yii::app()->numberFormatter->formatCurrency($model->$fieldName, 'USD');
					echo
						CHtml::activeLabelEx($model, $fieldName) . 
						CHtml::activeTextField($model,$fieldName, array(
							'onBlur'=>'this.value=formatCurrency(this.value);',
							'onLoad'=>'this.value=formatCurrency(this.value);',
							'onFocus'=>'clearZeroField(this);',
					));
					#$this->widget('CMaskedTextField',array(
					#	'model'=>$model,
					#	'attribute'=>$fieldName,
					#	'mask'=>'$9',
					#));	
				}

				elseif($fieldAttributes['type'] == 'bool') {
					echo
						CHtml::activeLabelEx($model, $fieldName) .
						CHtml::activeCheckBox($model,$fieldName);
				}
				// static case				
				elseif($fieldAttributes['type'] == 'boolStatic') {
					echo
						CHtml::label( $fieldAttributes['label'], $fieldName ) .
						CHtml::checkBox( $fieldName, true, array('id'=>$fieldName) );
				}				
				
				elseif($fieldAttributes['type'] == 'password') {
					echo
						CHtml::activeLabelEx($model, $fieldName) .
						CHtml::activePasswordField($model,$fieldName);
				}	

											
				elseif($fieldAttributes['type'] == 'hidden') {
 					echo					
 						CHtml::activeHiddenField($model, $fieldName);
				}				
				
				else {
					echo
						CHtml::activeLabelEx($model, $fieldName) .
						CHtml::activeTextField($model,$fieldName);
				}
				
				echo '</div>';
			} // end column filter if
		} // end field loop
		echo '</td>';
		
		if($currentColumn == $maxColumn) { //little hack to do zero column as separate table
			$currentColumn = -1;
			echo '</tr></table><br/><table class="columns"><tr>';
		} elseif($currentColumn == 0) break;
		
		$currentColumn++;
	} // end column loop
	
	echo '</tr></table><br/>';
	
?>

<?php if( $withForm ) {?>

<div class="action">
<?php
	$modelId = is_array($model->id) ? implode('_', $model->id) : $model->id;
	echo CHtml::hiddenField('selectedRows', $modelId);
	
	$isApportion = isset( $_REQUEST['clonedIds'] );
	if( $isApportion )
	{
		foreach( $_REQUEST['clonedIds'] as $index=>$value )
		{
			if( $model->id == $value )
			{
				$currentCloneIndex = $index;
				echo CHtml::hiddenField("currentCloneIndex", $currentCloneIndex ) . "\n";
			}
			echo CHtml::hiddenField("clonedIds[$index]", $value ) . "\n";
		}
		
		echo CHtml::submitButton($update ? "Save ($currentCloneIndex of " . count($_REQUEST['clonedIds']) . ")": 'Create');
		echo CHtml::button('Cancel Apportion', array('submit'=>array('deletePendingApportion', 'id'=>$model->originalEntityId)));
	}
	else // normal update
	{
		echo CHtml::submitButton($update ? 'Save' : 'Create');
		echo CHtml::button('Cancel', array('submit'=>CHttpRequest::getUrlReferrer()));
		//echo CHtml::button('Cancel', array('submit'=>array('list','redirectAction'=>$update ? 'show' : 'list')));
	}


?>
</div>
<?php echo CHtml::endForm(); ?>

</div><!-- yiiForm -->
<?php } // end if( $withForm ) ?>

<div class="actionBar">
<?php
$params		= array('model'=>$model);
$actionBar	= new PHForm( $model->actionBarConfig , $model );

echo $actionBar->renderBegin();
// display buttons based on rbac permissions
foreach( $actionBar->buttons as $button )
{
	$operation	= "{$model->modelName} " . $model->generateAttributeLabel( $button->name );
	$hasAccess	= Yii::app()->user->checkAccess($operation, $params);
	if( !$hasAccess )
	{
		$button->attributes['disabled'] = 'disabled';
		$button->attributes['title'] = "You have no access to $operation";
		unset( $button->attributes['submit'] );
	}
	echo $button->render();
}
echo $actionBar->renderEnd();
?>
</div>




<?php 

if(Yii::app()->user->id == 1)
	echo '<div class="operationError" id="configWarming"><strong>Current Subdomain: ' . Yii::app()->user->subdomain . '</strong></div>';

$message	= Yii::app()->user->getState('operationMessage', '');
$isError	= Yii::app()->user->getState('isError', false);
if( $message == '') return;

Yii::app()->user->setState('operationMessage', null );
Yii::app()->user->setState('isError', null );

$css = $isError ? 'operationError' : 'operationSuccess';
?>
<div
class="<?php echo $css; ?>"><span style="float: right">[<a href="#" onclick="return dismiss<?php echo $css; ?>();">Dismiss</a>]</span>
<?php echo $message; ?></div>
<script language="javascript" type="text/javascript">
function dismiss<?php echo $css; ?>()
{
	$(".<?php echo $css; ?>").hide("normal" ,
			function(){
				$(this).remove();
			});
	return false;
}
$(window).load( function(){
	$(".<?php echo $css; ?>").fadeTo("normal",0).fadeTo("normal",1);
});
</script>
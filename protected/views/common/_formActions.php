<?php
	echo '<div class="form-actions">';
		$this->widget('TbButton', array('buttonType'=>'link', 'type'=>'primary', 'label'=>'Update', 'url'=>array('update', 'id'=>$model->primaryKey)));
		$this->widget('TbButton', array('buttonType'=>'link', 'label'=>'Clone', 'url'=>array('clone', 'id'=>$model->primaryKey)));
		$this->widget('TbButton', array('buttonType'=>'link', 'label'=>'Delete', 'url'=>array('delete', 'id'=>$model->primaryKey)));
	echo '</div>';
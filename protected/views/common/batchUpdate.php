<script language="javascript" type="text/javascript">
skipformatCurrency = 0;
function formatCurrency(num)
{
	if( skipformatCurrency && num == '' )
	{
		return num;
	}
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + '$' + num + '.' + cents);
}

function getDollarValue( str ){
	if ( str == '' )
	{
		return 0;
	}
	var dollarValue	= str.replace("$", '' );
	// replace ALL occurances of ','
	while( dollarValue.indexOf(',') != -1 ) dollarValue = dollarValue.replace(",", '' );
	dollarValue		= parseFloat( dollarValue );
	return dollarValue;	
}

function clearZeroField( ele ){	
	if( getDollarValue( $(ele).val() ) == 0 )
	{
		$(ele).val( '' );
	}
}

// on page load
$(document).ready( function() {
	$('input.currency').each(function(index, item) {
		$(item).val( formatCurrency( $(item).val() ) );
		
		$(item).blur( function() {
			$(this).val( formatCurrency( $(this).val() ) );
		});
		
		$(item).focus( function() {
			clearZeroField( $(this) );
		});
	});
});

</script>
<style>
#updateTable
{
	padding: 0;
}
#updateTable, #updateTable td, #updateTable th
{
	border: 1px solid #4F81BD;
	border-collapse: collapse;
	padding: 0 2px;
	background-color: #EBF4FB;
}
#updateTable th
{
	background-color: #D3DFEE;
	padding: 2px;
}
</style>
<h2><?php echo "Batch Update {$models[0]->modelsLabel}"; ?></h2>
<?php echo CHtml::beginForm(null); ?>
<div class="yiiForm" style="display: table; width: auto;">
<?php 
foreach ($models as $model)
{
	echo CHtml::errorSummary($model, '');
}
?>
<table id="updateTable">
<?php
$elements = array();
foreach ($models[0]->showFieldsGrouping as $group )
{
	if( $group['visibleOnForm'] ) // render only editable field groups
		$elements = array_merge( $elements, $group['elements']);
}
//$elements = $models[0]->showFieldsGrouping['Information']['elements'];

//header row
echo '<tr>';
foreach ($elements as $element)
{
	if( !$models[0]->form[$element]->visible ) continue;
	
	echo '<th nowrap="nowrap">' . $models[0]->form[$element]->label . '</th>';
}
echo '</tr>';

foreach ($models as $model)
{
	echo '<tr>';
	$form = $model->form;
	foreach ($elements as $element)
	{
		if(!$form[$element]->visible) continue;
		
		// index by model id to build unique forms
		if( !( isset($form[$element]->attributes['showOnly']) && $form[$element]->attributes['showOnly'] ) )
		{
			$nameIndex = "[{$model->primaryKey}]{$form[$element]->name}";
			$form[$element]->name = $nameIndex;
		}
		echo '<td nowrap="nowrap">' . $form[$element]->renderInput() . '</td>';
		//echo '<pre>' . print_r($form[$element],1) . '</pre>';
	}
	echo '</tr>';
}
?>
</table>
</br>
<?php
echo CHtml::submitButton('Update All');
echo CHtml::button('Cancel', array('submit'=>CHttpRequest::getUrlReferrer() ) );
echo CHtml::endForm();
?>
</div>

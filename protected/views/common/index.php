<?php 

$modelName = $model->modelName;

$this->widget('TbAlert', array('id'=>'flashMessage'));
	
$tabId = $modelName.'-tab'; 
$tabs[$tabId] = 
	array(
		'id'=>$tabId,
		'label'=>$model->modelLabel.' Index',
		'content'=>$this->renderPartial('/common/_grid', array('model'=>$model), true),
		'active'=>!$activeTab || $activeTab == $tabId,
	);

$tabId = 'reports-tab'; 	
$tabs[$tabId] = 
	array(
		'id'=>$tabId,
		'label'=>'Reports',
		'content'=>$this->renderPartial('/reportDefinition/_contextReports', array('model'=>$model), true),
		'active'=>$activeTab == $tabId,
	);

$this->widget('TbTabs', array('tabs'=>$tabs, 'encodeLabel'=>false));
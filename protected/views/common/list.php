<?php
$this->renderFile('assets/jqgrid/include.html');

if(!isset($ids)) $ids = array(); // set the ids variable to null if it was not passed

$listDataParams			= array();
$createParams			= array();
 
$listViewConfig			= $listModel->listViewConfig;

// list display defaults
foreach( $listViewConfig['elements'] as $columnName=>$columnValues )
{
	if( !isset( $columnValues['label'] ) )
	{
		$listViewConfig['elements'][$columnName]['label'] = isset( $listModel->form[$columnName]->label )? $listModel->form[$columnName]->label : $listModel->generateAttributeLabel($columnName);
	}
	if( !isset( $columnValues['width'] ) ) $listViewConfig['elements'][$columnName]['width'] = '100';
}
if( !isset( $listViewConfig['hideNavigation'] ) )	$listViewConfig['hideNavigation']	= false;
if( !isset( $listViewConfig['criteria'] ) )			$listViewConfig['criteria'] 		= new CDbCriteria;

$title 					= isset( $listViewConfig['title'] ) ? $listViewConfig['title'] : $listModel->modelsLabel . ' List';
$listViewAttributes		= $listViewConfig['elements'];
$hideNavigation			= $listViewConfig['hideNavigation'];

if( !isset( $listType ) ) $listType = 'default'; // used for namespacing
Yii::app()->user->setState("listType", $listType );
Yii::app()->user->setState("{$listModel->modelName}_{$listType}_listConfig", $listViewConfig );

//Yii::trace("from list view, listViewConfig['title']: " . $listViewConfig['title'] );
$isRelatedList			= $this instanceof PHList;
$isPending				= isset( $_REQUEST['status'] ) && ( $_REQUEST['status'] == 5 );
$showReportTab			= !$isRelatedList;

if($isRelatedList)
{
	$createParams	= array('name'=>$this->model->modelName,'value'=>$this->model->primaryKey);
}

// confirm pending action
$pendingOperation		= "{$listModel->modelName} Delete Pending";
$params					= array('model'=>$listModel);
$hasDeletePendingAccess	= Yii::app()->user->checkAccess($pendingOperation, $params); 

// hard delete action
$hardDeleteAction 		= 'delete';
$hardDeleteLabel		= $listModel->generateAttributeLabel( $hardDeleteAction );
$hardDeleteButton 		= CHtml::button($hardDeleteLabel	, array('submit'=>array($listModel->modelName.'/list'	,'redirectAction'=>$hardDeleteAction),'confirm'=>"Are you sure you want to $hardDeleteAction the selected {$listModel->modelsLabel}?"));
$hardDeleteOperation	= "{$listModel->modelName} " . $hardDeleteLabel;
$hasHardDeleteAccess	= Yii::app()->user->checkAccess($hardDeleteOperation, $params); 

// standard delete action
$deleteAction 			= $listModel->standardDeleteAction;
$deleteLabel			= $listModel->generateAttributeLabel( $deleteAction );
$deleteButton 			= CHtml::button($deleteLabel	,array('submit'=>array($listModel->modelName.'/list'	,'redirectAction'=>$deleteAction),'confirm'=>"Are you sure you want to $deleteAction the selected {$listModel->modelsLabel}?"));
$deleteOperation		= "{$listModel->modelName} " . $deleteLabel;
$hasDeleteAccess		= Yii::app()->user->checkAccess($deleteOperation, $params); 

// if passed to list view, pass along to XML action
foreach( array( '_search', 'status', 'username', 'paid', 'object', 'objectId', 'filterCriteria', 'modifiedUser') as $passParam )
{
	if( isset($_REQUEST[$passParam]) )	$listDataParams[$passParam]	= $_REQUEST[$passParam];
}
?>

<?php
if( $listModel->modelName == 'Payment' )
{
	if( isset( $_REQUEST['DOC_LIST'] ) )
	{
		$this->renderPartial('/reportDefinition/print');
	}
}
?>
<?php echo $title ? "<h2>$title</h2>" : ''; ?>
<?php
	if( $hideNavigation ) // used to suppress hidden forms that cause IE to crash
	{
		$listDataParams['hideNavigation'] = $hideNavigation;
	}
//Yii::trace('From list view, $_REQUEST: ' . print_r($_REQUEST,1) )
?>		
<script type="text/javascript">
function updateSelected<?php echo $listModel->modelName; ?>Rows( rowid, status){
	var grid					= $( '#<?php echo $listModel->modelName . '_grid'; ?>' );
	var selectedRowsParameter	= $( '#<?php echo $listModel->modelName . 'SelectedRows'; ?>' );
	
	if( status ) // row selected
	{
		if( selectedRowsParameter.val() == '' ) // initial assignment
		{
			selectedRowsParameter.val( rowid );
		}
		else
		{
			selectedRowsParameter.val( selectedRowsParameter.val() + ',' + rowid );
		}
	}
	else // row deselected
	{
		var ids = selectedRowsParameter.val();
		ids = ids.split(",");
		var uniqueIds	= [];
		var uniqueArray	= [];
		for(var i=0, n=ids.length; i<n; i++)
		{
			if( ids[i] == '' || ids[i] == rowid )continue; // skip empty and deselected row
			uniqueIds[ ids[i] ] = 1;
		}
		for( id in uniqueIds )
		{
			uniqueArray.push(id);
		}
		// store passed ids in *SelectedRows again
		selectedRowsParameter.val( uniqueArray.join(',') );
	}
	
	// store selected rows in other fields, to be passed to a report or bill extract
	 $( '#contextIds' )		.val( selectedRowsParameter.val() );
	 $( '#entityIds' )		.val( selectedRowsParameter.val() );
	 $( '#redirectAction' )	.val( '/reportDefinition/ExecuteReport/contextIds/' + selectedRowsParameter.val() );
}
$(document).ready(function() {
	var mygrid = jQuery("#<?php echo $listModel->modelName . '_grid' ?>").jqGrid({ 
	    theme: 'redmond',
	    mtype: 'POST',
		datatype: 'xml',
	    useNavBar: true,
	    multiselect: true,
	    viewrecords: true,
	    viewsortcols: true,
	    gridview: true,
		width: '100%',
		height: '100%',
		gridComplete: function(){
						var ids	= $( '#<?php echo $listModel->modelName ?>SelectedRows' ).val();
						var preSelected = '<?php echo isset( $_REQUEST['preSelected'] )? $_REQUEST['preSelected'] : '';?>';
						if( preSelected != '' ) ids = preSelected + ',' + ids;
						if( ids == '' )return;

						var grid = $( '#<?php echo $listModel->modelName . '_grid' ?>' );
						ids = ids.split(",");
						var uniqueIds	= [];
						var uniqueArray	= [];
						for(var i=0, n=ids.length; i<n; i++)
						{
							if( ids[i] == '' )continue;
							uniqueIds[ ids[i] ] = 1;
						}
						for( id in uniqueIds )
						{
							grid.setSelection(id);
							uniqueArray.push(id);
						}
						// store passed ids in *SelectedRows again
						$( '#<?php echo $listModel->modelName ?>SelectedRows' )
						.val( uniqueArray.join(',') );
					},
        onSelectRow: updateSelected<?php echo $listModel->modelName; ?>Rows,
        onSelectAll: updateSelected<?php echo $listModel->modelName; ?>Rows,
		rowNum: <?php echo Yii::app()->params['defaults']['listRowSize']; ?>,
	    rowList: [20,100,500],
		pager: '#<?php echo $listModel->modelName . '_pager' ?>',
		url:'<?php
		//Yii::trace( '$listDataParams from list view, before clean up: ' . print_r($listDataParams,1));
		// remove empty values or arrays, as they cause issues in createUrl
		foreach($listDataParams as $index=>$value)
		{
			if( $value == '' || ( is_array($value) && empty($value) ) )
			{
				unset( $listDataParams[$index] );
			}
		}
		//Yii::trace( '$listDataParams from list view, after clean up: ' . print_r($listDataParams,1));
		echo Yii::app()->createUrl("/{$listModel->modelName}/listXml", $listDataParams); ?>',
		caption:'<?php echo $listModel->modelsLabel; ?>',
	    sortname: '<?php echo $listModel->defaultSort['name']; ?>',
		sortorder: '<?php echo $listModel->defaultSort['order']; ?>',
		colNames:[ '', <?php foreach($listViewAttributes as $columnName=>$columnValues)
		{
			echo "'" . $listViewAttributes[$columnName]['label'] . "',";
		}?>],
		colModel:[ {name:'show', index:'show', align:'center', width:<?php echo $listModel->modelName == 'Bill'? 40:20; ?>, search: false},
					<?php
					$childModels	= array();
					if($isRelatedList)
					{
						$listModelMetaData	= $listModel->getMetaData();
						$childModels		= $parentModel->{$listModel->modelsName};
					}
						
					foreach($listViewAttributes as $columnName=>$columnValues)
					{
						$isHidden		= isset($columnValues['hidden']) ? ", hidden: true" : '';
						if( $hideNavigation )
						{
							$isSearchable	= ", search: false";
						}
						elseif( isset($columnValues['search']) )
						{
							if( $columnValues['search'] )
								$isSearchable	= ", search: true";
							else
								$isSearchable	= ", search: false, sortable: false";
						}
						else
						{
							$isSearchable	= '';
						}
						
						// K: building the search drop down needs major code cleanup 
						// add drop down search
						$isBool				= false;
						$isEnumeration		= false;
						$doSearch			= false;
						$hasColumnValues	= isset($columnValues['searchDropDown']);
						
						if( isset( $listModel->formConfig['elements'][$columnName]['isModel'] ) )
						{
							$isEnumeration	= !$listModel->formConfig['elements'][$columnName]['isModel'];
							if(!$hasColumnValues)
								$columnValues['searchDropDown']	= $listModel->form[$columnName]->items;
						}
						else if( isset( $listModel->formConfig['elements'][$columnName]['type'] ) )
						{
							$columnType		= $listModel->getFieldType($columnName);
							$isBool			= $columnType == 'checkbox';
							
							if(!$hasColumnValues && $isBool)
							{
								$columnValues['searchDropDown']	= array('true'=>'Yes', 'false'=>'No');
							}
							// for related lists, filter search drop down values by parent id
							else if( !$hasColumnValues && $columnType == 'dropdownlist' )
							{
								$foreignModels	= array();
								if( $isRelatedList && $childModels )
								{
									//Yii::trace("columnName: $columnName, listModel: {$listModel->modelName}, parentModel: {$parentModel->modelName}");
									$foreignPrimaryKeys	= array();
									foreach ($childModels as $index=>$childModel)
									{
										if($childModel->$columnName)
											$foreignPrimaryKeys[]	= $childModel->$columnName;
									}
									if($foreignPrimaryKeys)
									{
										$doSearch			= true;
										$foreignModelName	= PHModel::getForeignModelName($columnName);
										$relation			= $listModelMetaData->relations[$foreignModelName];
										$relatedClass		= $relation->className;
										$foreignModel		= new $relatedClass;
										$foreignModels		= $foreignModel->findAllByPk($foreignPrimaryKeys);
										$columnValues['searchDropDown']	= CHtml::listData($foreignModels, $foreignModel->primaryKeyName, 'name');
									}
								}
								// normal list view
								else
								{
									$doSearch			= true;
									$columnValues['searchDropDown']	= $listModel->form[$columnName]->items;
								}
							}
						}
						
						$hasSearchDropDown	= $hasColumnValues || $doSearch || $isBool || $isEnumeration;
						
						if($hasSearchDropDown)
						{
							$dropValues		= $columnValues['searchDropDown'];
							$dropdown		= array(':-- All --');
							foreach( $dropValues as $key=>$value ) $dropdown[] = "$key:$value";
							$dropdown		= implode(';', $dropdown);
							$dropdown		= str_replace(array('"', "'"), '\'', $dropdown); // escape quotes 
							$isSearchable	= ", search: 'true', stype: 'select', editoptions:{value:\"{$dropdown}\"}";
						}
						
						echo 	"{name:'" . $columnName . "', " .
								"index:'" . $columnName . "', " .
								"width:'" . $columnValues['width'] . "' " .
								$isHidden .
								$isSearchable .  
								"},\n"; 
					} ?>
		],
		footerrow : <?php echo $listModel->displayListFooter ? 'true' : 'false' ?>,
		userDataOnFooter : <?php echo $listModel->displayListFooter ? 'true' : 'false' ?>
	});
	
	mygrid.navGrid('#<?php echo $listModel->modelName . '_grid' ?>',{edit:false,add:false,del:false,search:false,refresh:false});
	<?php if( !$hideNavigation ) {?>
	/*
	mygrid.navButtonAdd("#<?php echo $listModel->modelName . '_pager' ?>",{caption:"Toggle",title:"Toggle Search Toolbar", buttonicon :'ui-icon-pin-s',
		onClickButton:function(){
			mygrid[0].toggleToolbar();
		}
	});
	*/
	mygrid.navButtonAdd("#<?php echo $listModel->modelName . '_pager' ?>",{caption:"Clear",title:"Clear Search",buttonicon :'ui-icon-refresh',
		onClickButton:function(){
			mygrid[0].clearToolbar();
		}
	});
	<?php if( $isPending ) {?>
	mygrid.navButtonAdd("#<?php echo $listModel->modelName . '_pager' ?>",
			{	
				caption:"<?php echo 'Confirm Pending';?>",
				title:"Confirm matching pending <?php echo $listModel->modelsLabel; ?>",
				onClickButton:function(){
					if( confirm('Confirm ' + $('#confirmPending<?php echo $listModel->modelsName; ?>Count').val() + ' matching pending <?php echo $listModel->modelsLabel; ?>?' ) )
					{
						$('#confirmPending<?php echo $listModel->modelsName; ?>').submit();
					}					
				}
	});
	<?php } ?>
	<?php
	// standard delete
	if( $hasDeleteAccess && ($deleteAction != $hardDeleteAction) && !$isPending ) {	?>
	mygrid.navButtonAdd("#<?php echo $listModel->modelName . '_pager' ?>",
			{	
				caption:"<?php echo $deleteLabel . ' All';?>",
				title:"<?php echo $deleteLabel;?> matching <?php echo $listModel->modelsLabel; ?>",
				buttonicon :'ui-icon-trash',
				onClickButton:function(){
					if( confirm('<?php echo $deleteLabel;?> ' + $('#deleteAll<?php echo $listModel->modelsName; ?>Count').val() + ' matching <?php echo $listModel->modelsLabel; ?>?' ) )
					{
						$('#deleteAll<?php echo $listModel->modelsName; ?>').submit();
					}					
				}
	});
	<?php
	} // end if ?>
	<?php
	// hard delete
	if( $hasHardDeleteAccess || ( $hasDeletePendingAccess && $isPending ) ) 
	{
		$hardDeleteLabel = 'Delete'; ?>
	mygrid.navButtonAdd("#<?php echo $listModel->modelName . '_pager' ?>",
			{	
				caption:"<?php echo $hardDeleteLabel . ' All';?>",
				title:"<?php echo $hardDeleteLabel;?> matching <?php echo $listModel->modelsLabel; ?>",
				buttonicon :'ui-icon-trash',
				onClickButton:function(){
					if( confirm('<?php echo $hardDeleteLabel;?> ' + $('#hardDeleteAll<?php echo $listModel->modelsName; ?>Count').val() + ' matching <?php echo $listModel->modelsLabel; ?>?' ) )
					{
						$('#hardDeleteAll<?php echo $listModel->modelsName; ?>').submit();
					}					
				}
	});
	<?php } // end if ?>
	<?php } // end if( !$hideNavigation ) ?>
	mygrid.filterToolbar();
	<?php if( $hideNavigation ) { // hide search toolbar?>
	mygrid[0].toggleToolbar();
	<?php } ?>

}); 

</script>
<?php 
if( $showReportTab ) $this->beginClip('Information'); // removed the reports tab for now
?>
<table id="<?php echo $listModel->modelName . '_grid' ?>"></table>
<div id="<?php echo $listModel->modelName . '_pager' ?>"></div>
<div id="filter" style="margin-left:30%;display:none">Search</div>

<?php echo '<table><tr><td>'; ?>
<?php
$hiddenSelectedRows = CHtml::hiddenField("{$listModel->modelName}SelectedRows");
if(!$hideNavigation)
{
?>
<?php echo CHtml::beginForm(); ?>
<?php echo $hiddenSelectedRows; ?>

<?php echo CHtml::button('Show'		, array('submit'=>array("{$listModel->modelName}/list"	, 'redirectAction'=>'show'))); ?>
<?php echo CHtml::button('Update'	, array('submit'=>array("{$listModel->modelName}/list"	, 'redirectAction'=>'update'))); ?>
<?php echo CHtml::button('Create'	, array('submit'=>array("{$listModel->modelName}/create", 'key'=>$createParams))); ?>
<?php
// hard delete button
if( $hasHardDeleteAccess	|| ( $hasDeletePendingAccess && $isPending ) )	echo $hardDeleteButton;
// standard delete button
if( $hasDeleteAccess		&& ( $deleteAction != $hardDeleteAction ) )		echo $deleteButton;
?>
<?php
if($isPending)
{
	echo CHtml::button('Confirm Pending', array('submit'=>array("{$listModel->modelName}/confirmPending"), 'confirm'	=> "Confirm selected pending {$listModel->modelsLabel}?" ));
}
?>
<?php echo CHtml::endForm(); ?>
<?php
}
else
{
	echo $hiddenSelectedRows;
}
?>
<?php echo '</td></tr></table>'; ?>
<br />
<?php 
if( $showReportTab )
{
	$this->endClip();
	$this->beginClip('Report');
		$reports = ReportDefinition::model()->findAll('"contextObject" = :contextObject', array('contextObject'=>$listModel->modelName));

		echo CHtml::beginForm('', 'post', array('target' => '_blank') );
		echo CHtml::hiddenField('contextIds'				, '', array('id'=>'contextIds') );
		echo CHtml::hiddenField('BillExtract[entityIds]'	, '', array('id'=>'entityIds') ); // will be set to the same value as contextIds
		echo CHtml::hiddenField('redirectAction'			, '', array('id'=>'redirectAction') );
		foreach($reports as $report)
		{
			$params = array();
			if(!isset($model)) $model = $listModel; // used for asOfDate
			if($report->isExtractReport && $listModel->modelName == 'Entity' )
			{
				$route	 							= array( 'BillExtract/createAll' );
				$params['redirectId'] 				= $report->primaryKey;
				$params['BillExtract[asOfDate]']	= $model->asOfDate ? Yii::app()->dateFormatter->format('yyyy-MM-dd', $model->asOfDate) : date('Y-m-d');
			}
			else //no extract
			{ 
				$route	 							= array( 'ReportDefinition/executeReport', $report->primaryKeyName => $report->primaryKey );
				if( isset($model->asOfDate))
				{
					$params['BillExtract[asOfDate]']	= $model->asOfDate ? Yii::app()->dateFormatter->format('yyyy-MM-dd', $model->asOfDate) : date('Y-m-d');
				}
			}					
			
		  	echo '<p>'; 
			echo CHtml::link($report->name, '#',
					array(
					'submit'	=> $route,
					'params'	=> $params,
					'target'	=>'_blank',
					)
				);
			echo '</p>';
		}
		echo CHtml::beginForm();
		
		if( !$reports )
		{
			echo '<p>No reports found</p>';
		}
		
	$this->endClip();
	
	$clips = $this->clips;
	
	$viewData = array ('model'=>$listModel,);

	$tabs = array (
				'tab1'=>array('title'=>'Information'	, 'content'=>$clips['Information']),
				'tab2'=>array('title'=>'Reports'		, 'content'=>$clips['Report']),
	);
 
	$this->widget('CTabView', array('tabs'=>$tabs, 'viewData'=>$viewData));
}
?>
<?php
	echo $form->textFieldRow($model,'username',array('maxlength'=>45));
	echo $form->passwordFieldRow($model,'password',array('maxlength'=>45));
	echo $form->textFieldRow($model,'name',array('maxlength'=>45));
	echo $form->textFieldRow($model,'email',array('maxlength'=>45));
	echo $form->dropdownListRow($model,'departmentIds',
				CHTml::listData(Department::model()->sort()->findAll(), 'departmentId', 'name'), array('multiple'=>true, 'empty'=>'[ALL]'));
	echo $form->dropdownListRow($model,'groupId',
				CHTml::listData(Group::model()->sort()->findAll(), 'groupId', 'name'));

<h2>Update <?php echo $model->tableName() . ' - ' . $model->id; ?></h2>

<?php echo $this->renderPartial('/common/form', array(
	'model'=>$model,
	'update'=>true,
)); ?>
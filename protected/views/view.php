<?php 

$modelName = $model->modelName;

$this->breadcrumbs = array(
	'Employees'=>array('Employee/index'),
	$model->Employee->name=>array('Employee/view', 'id'=>$model->employeeId),
	'Deductions'=>array('Deduction/index'),
	$model->Deduction->name=>array('Deduction/view', 'id'=>$model->deductionId),
	$model->modelsLabel=>array('index'),
	$model->name,
);

$this->widget('TbAlert', array('id'=>'flashMessage'));

$tabs = array();

$tabId = $modelName.'-tab'; 
$tabs[$tabId] = 
	array(
		'id'=>$tabId,
		'label'=>'Detail',
		'content'=>$this->renderPartial('/common/_detail', array('model'=>$model), true),
		'active'=>!$activeTab || $activeTab == $tabId,
	);

$tabId = 'ContextReport-tab'; 	
$tabs[$tabId] = 
	array(
		'id'=>$tabId,
		'label'=>'Reports',
		'content'=>$this->renderPartial('/common/contextReports', array('model'=>$model), true),
		'active'=>$activeTab == $tabId,
	);

$this->widget('TbTabs', array('tabs'=>$tabs, 'encodeLabel'=>false));

echo '<div class="form-actions">';
	$this->widget('TbButton', array('buttonType'=>'link', 'type'=>'primary', 'label'=>'Update', 'url'=>array('update', 'id'=>$model->primaryKey)));
	$this->widget('TbButton', array('buttonType'=>'link', 'label'=>'Clone', 'url'=>array('clone', $model->primaryKeyName=>$model->primaryKey)));
echo '</div>';
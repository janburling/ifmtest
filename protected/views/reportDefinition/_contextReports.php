<?php
	$reports = ReportDefinition::model()->sort()->findAllByAttributes(array('contextObject'=>$model->modelName));
	
	echo CHtml::beginForm('', 'post', array('target' => '_blank') );
	echo CHtml::hiddenField('contextIds'				, '', array('id'=>'contextIds') );
	echo CHtml::hiddenField('BillExtract[entityIds]'	, '', array('id'=>'entityIds') ); // will be set to the same value as contextIds
	echo CHtml::hiddenField('redirectAction'			, '', array('id'=>'redirectAction') );
	foreach($reports as $report)
	{
		$params = array();
		$route	 							= array( 'ReportDefinition/executeReport', $report->primaryKeyName => $report->primaryKey );
		if( isset($model->asOfDate))
		{
			$params['BillExtract[asOfDate]']	= $model->asOfDate ? Yii::app()->dateFormatter->format('yyyy-MM-dd', $model->asOfDate) : date('Y-m-d');
		}			
		
		echo '<p>'; 
		echo CHtml::link($report->name, '#',
				array(
				'submit'	=> $route,
				'params'	=> $params,
				'target'	=>'_blank',
				)
			);
		echo '</p>';
	}
	echo CHtml::endForm();
	
	if( !$reports )
	{
		echo '<p>No reports found.</p>';
	}

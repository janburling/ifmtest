<?php 

$modelName = $model->modelName;

$this->breadcrumbs = array(
	$model->ReportFolder->name=>array('/reportFolder/view', 'id'=>$model->reportFolderId),
	$model->name,
);

$tabs = array();

$tabId = $modelName.'-tab'; 
$tabs[$tabId] = 
	array(
		'id'=>$tabId,
		'label'=>'Detail',
		'content'=>$this->renderPartial('/common/_detail', array('model'=>$model), true),
		'active'=>!$activeTab || $activeTab == $tabId,
	);
        
$tabModel = new ReportParameter;
$tabId = $tabModel->modelName.'-tab';
$tabModel->reportDefinitionId = $model->reportDefinitionId;
$tabs[$tabId] =
        array(
              	'id'=>$tabId,
                'label'=>$tabModel->modelsLabel,
                'content'=>$this->renderPartial('/common/_grid', array('model'=>$tabModel), true),
		'active'=>$activeTab == $tabId,
        );

$this->widget('TbTabs', array('tabs'=>$tabs, 'encodeLabel'=>false));

echo '<div class="form-actions">';
	$this->widget('TbButton', array('buttonType'=>'link', 'type'=>'primary', 'label'=>'Update', 'url'=>array('update', 'id'=>$model->primaryKey)));
	$this->widget('TbButton', array('buttonType'=>'link', 'label'=>'Clone', 'url'=>array('clone', $model->primaryKeyName=>$model->primaryKey)));
echo '</div>';
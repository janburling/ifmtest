<?php
	echo $form->dropdownListRow($model, 'reportFolderId',
		CHtml::listData(ReportFolder::model()->sort()->findAll(),'reportFolderId','name')
	);
	echo $form->textFieldRow($model,'name',array('maxlength'=>45));
	echo $form->textFieldRow($model,'location',array('maxlength'=>45));
	echo $form->dropdownListRow($model, 'contextObject', $model->modelNames, array('empty'=>'-'));
	echo $form->dropdownListRow($model, 'format', $model->listReportFormats);
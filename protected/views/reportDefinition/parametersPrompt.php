<h3>Input Parameters</h3>
<div class="form">

<?php 
	echo CHtml::errorSummary($model);
	echo CHtml::beginForm(array('executeReport', $model->primaryKeyName => $model->primaryKey), 'get');

foreach($params as $id=>$param) { ?>
	<div class="simple">
	<?php
	echo CHtml::label( CModel::generateAttributeLabel( $param->name ), $param->name);
	$paramValue = isset( $_REQUEST[$param->name] ) ? $_REQUEST[$param->name] : $param->value;
	if( $param->isDate )
	{				
		$this->widget('application.extensions.jui.EDatePicker',
						array_merge(
							Yii::app()->params['defaults']['datePicker'],
							array(
								'name'			=> $param->name,
				            	'value'			=> date('m/d/Y'),
							)
						)
					);
	}
	else
	{
		echo CHtml::textField($param->name, $paramValue);
	}
	?>
	</div>
<?php	
} ?>
<div class="action">
<?php echo CHtml::hiddenField('contextIds', $model->contextIds); ?>
<?php echo CHtml::submitButton('Run Report'); ?>
</div>
<?php echo CHtml::endForm(); ?>
</div>
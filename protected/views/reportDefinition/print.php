<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<script type="text/javascript" language="javascript">
function showMessage(msg) {
  if (msg=='') {
    document.getElementById("status").innerHTML=msg;
    return;
  }
  if (document.createTextNode){
    var mytext=document.createTextNode(msg+"\r");
    document.getElementById("status").appendChild(mytext);
  } else{
    document.getElementById("status").innerHTML=msg;
  }
}
$( function(){
	//showMessage('Printing status... ' );
});
</script>

<?php
//Params exteaction
$licenseKey					= "A8EC-73AF-64A9-0375";
$docList					= isset( $_GET["DOC_LIST"] )						? $_GET["DOC_LIST"]						: '';

$docId						= isset( $_GET["DOC_ID"] )							? $_GET["DOC_ID"]						: '';
$printerName				= isset( $_GET["PRINTER_NAME"] )					? $_GET["PRINTER_NAME"]					: '';
$printerNameSubstringMatch	= isset( $_GET["PRINTER_NAME_SUBSTRING_MATCH"] )	? $_GET["PRINTER_NAME_SUBSTRING_MATCH"]	: '';
$paper						= isset( $_GET["PAPER"] )							? $_GET["PAPER"]						: '';
$copies						= isset( $_GET["COPIES"] )							? $_GET["COPIES"]						: '';
$jobName					= isset( $_GET["JOB_NAME"] )						? $_GET["JOB_NAME"]						: '';
$showPrinterDialog			= isset( $_GET["SHOW_PRINT_DIALOG"] )				? $_GET["SHOW_PRINT_DIALOG"]			: '';
$autoMatchPaper				= isset( $_GET["AUTO_MATCH_PAPER"] )				? $_GET["AUTO_MATCH_PAPER"]				: '';
$pageScaling				= isset( $_GET["PAGE_SCALING"] )					? $_GET["PAGE_SCALING"]					: '';
$autoRotateAndCenter		= isset( $_GET["AUTO_ROTATE_AND_CENTER"] )			? $_GET["AUTO_ROTATE_AND_CENTER"]		: '';
$usePrinterMargins			= isset( $_GET["IS_USE_PRINTER_MARGINS"] )			? $_GET["IS_USE_PRINTER_MARGINS"]		: '';
$singlePrintJob				= isset( $_GET["SINGLE_PRINT_JOB"] )				? $_GET["SINGLE_PRINT_JOB"]				: '';
$collateCopies				= isset( $_GET["COLLATE_COPIES"] )					? $_GET["COLLATE_COPIES"]				: '';
$showErrorDialog			= isset( $_GET["SHOW_PRINT_ERROR_DIALOG"] )			? $_GET["SHOW_PRINT_ERROR_DIALOG"]		: '';
$password					= isset( $_GET["PASSWORD"] )						? $_GET["PASSWORD"]						: '';
$urlAuthId					= isset( $_GET["URL_AUTH_ID"] )						? $_GET["URL_AUTH_ID"]					: '';
$urlAuthPassword			= isset( $_GET["URL_AUTH_PASSWORD"] )				? $_GET["URL_AUTH_PASSWORD"]			: '';
$printQuality				= isset( $_GET["PRINT_QUALITY"] )					? $_GET["PRINT_QUALITY"]				: '';
$side						= isset( $_GET["SIDE_TO_PRINT"] )					? $_GET["SIDE_TO_PRINT"]				: '';
$statusUpdateEnabled		= isset( $_GET["STATUS_UPDATE_ENABLED"] )			? $_GET["STATUS_UPDATE_ENABLED"]		: '';
$debug						= isset( $_GET["DEBUG"] )							? $_GET["DEBUG"]						: '';

$successPage				= isset( $_GET["ON_SUCCESS_SHOW_PAGE"] )			? $_GET["ON_SUCCESS_SHOW_PAGE"]			: '';
$successPageTarget			= isset( $_GET["ON_SUCCESS_PAGE_TARGET"] )			? $_GET["ON_SUCCESS_PAGE_TARGET"]		: '';
$failurePage				= isset( $_GET["ON_FAILURE_SHOW_PAGE"] )			? $_GET["ON_FAILURE_SHOW_PAGE"]			: '';
$failurePageTarget			= isset( $_GET["ON_FAILURE_PAGE_TARGET"] )			? $_GET["ON_FAILURE_PAGE_TARGET"]		: '';
$serverCallBackUrl			= isset( $_GET["SERVER_CALL_BACK_URL"] )			? $_GET["SERVER_CALL_BACK_URL"]			: '';
$isShowPrintPreview			= isset( $_GET["IS_SHOW_PRINT_PREVIEW"] )			? $_GET["IS_SHOW_PRINT_PREVIEW"]		: '';
$viewerPage					= isset( $_GET["VIEWER_PAGE"] )						? $_GET["VIEWER_PAGE"]					: '';
$viewerControls				= isset( $_GET["VIEWER_CONTROLS"] )					? $_GET["VIEWER_CONTROLS"]				: '';

///Test the params////
//echo "docList=${docList}<br>\r\n";
//echo "printerName=${printerName}<br>\r\n";
//echo "printerNameSubstringMatch=${printerNameSubstringMatch}<br>\r\n";
//echo "papers=${paper}<br>\r\n";
//echo "copies=${copies}<br>\r\n";
//echo "jobName=${jobName}<br>\r\n";
//echo "showPrinterDialog=${showPrinterDialog}<br>\r\n";
//echo "autoMatchPaper=${autoMatchPaper}<br>\r\n";
//echo "pageScaling=${pageScaling}<br>\r\n";
//echo "autoRotateAndCenter=${autoRotateAndCenter}<br>\r\n";
//echo "usePrinterMargins=${usePrinterMargins}<br>\r\n";
//echo "singlePrintJob=${singlePrintJob}<br>\r\n";
//echo "collateCopies=${collateCopies}<br>\r\n";
//echo "showErrorDialog=${showErrorDialog}<br>\r\n";
//echo "password=${password}<br>\r\n";
//echo "printQuality=${printQuality}<br>\r\n";
//echo "side=${side}<br>\r\n";
//echo "statusUpdateEnabled=${statusUpdateEnabled}<br>\r\n";
//echo "debug=${debug}<br>\r\n";
?>

<?php
$baseURL = 'http://' . $_SERVER['HTTP_HOST'];
if( PHModel::getIsDevServer() ) // dev
{ 
	$appletPath = "$baseURL/sjPDF";
}
else
{
	$appletPath = "$baseURL/sjPDFbrowser_2.5_01_po";
} // end if dev
?>

<OBJECT
  codeBase="http://java.sun.com/products/plugin/autodl/jinstall-1_4_2_06-windows-i586.cab#Version=1,4,2,06"
  width="2" height="2" classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93">
  <PARAM NAME="type" VALUE="application/x-java-applet;version=1.4.2">
  <PARAM NAME="name" VALUE="smartj">
  <PARAM NAME="alt" VALUE="PDF - Print and View PDF from browser">
  <PARAM NAME="CODEBASE" VALUE="<?php echo $appletPath; ?>/browser_lib/">
  <PARAM NAME="CODE" VALUE="com.activetree.pdfprint.WebSilentPrintPdf">
  <PARAM NAME="ARCHIVE" VALUE="">
  <PARAM NAME="cache_option" VALUE="Plugin">
  <PARAM NAME="cache_archive" VALUE="bc.jar,jai_codec.jar,jai_core.jar,sjpdf_license.jar,sjpdf_browser.jar">
  <PARAM NAME="LICENSE_KEY" VALUE=<?php echo "\"${licenseKey}\""; ?>>
  <PARAM NAME="DOC_LIST" VALUE=<?php echo "\"${docList}\""; ?>>
  <PARAM NAME="DOC_ID" VALUE=<?php echo "\"${docId}\""; ?>>
  <PARAM NAME="PAGE_SCALING" VALUE=<?php echo "\"${pageScaling}\""; ?>>
  <PARAM NAME="AUTO_ROTATE_AND_CENTER" VALUE=<?php echo "\"${autoRotateAndCenter}\""; ?>>
  <PARAM NAME="AUTO_MATCH_PAPER" VALUE=<?php echo "\"${autoMatchPaper}\""; ?>>
  <PARAM NAME="IS_USE_PRINTER_MARGINS" VALUE=<?php echo "\"${usePrinterMargins}\""; ?>>
  <PARAM NAME="PRINTER_NAME" VALUE=<?php echo "\"${printerName}\""; ?>>
  <PARAM NAME="PRINTER_NAME_SUBSTRING_MATCH" VALUE=<?php echo "\"${printerNameSubstringMatch}\""; ?>>
  <PARAM NAME="PAPER" VALUE=<?php echo "\"${paper}\""; ?>>
  <PARAM NAME="COPIES" VALUE=<?php echo "\"${copies}\""; ?>>
  <PARAM NAME="COLLATE_COPIES" VALUE=<?php echo "\"${collateCopies}\""; ?>>
  <PARAM NAME="JOB_NAME" VALUE=<?php echo "\"${jobName}\""; ?>>
  <PARAM NAME="SHOW_PRINT_DIALOG" VALUE=<?php echo "\"${showPrinterDialog}\""; ?>>
  <PARAM NAME="SINGLE_PRINT_JOB" VALUE=<?php echo "\"${singlePrintJob}\""; ?>>
  <PARAM NAME="SHOW_PRINT_ERROR_DIALOG" VALUE=<?php echo "\"${showErrorDialog}\""; ?>>
  <PARAM NAME="PASSWORD" VALUE=<?php echo "\"${password}\""; ?>>
  <PARAM NAME="PRINT_QUALITY" VALUE=<?php echo "\"${printQuality}\""; ?>>
  <PARAM NAME="SIDE_TO_PRINT" VALUE=<?php echo "\"${side}\""; ?>>
  <PARAM NAME="STATUS_UPDATE_ENABLED" VALUE=<?php echo "\"${statusUpdateEnabled}\""; ?>>
  <PARAM NAME="DEBUG" VALUE=<?php echo "\"${debug}\""; ?>>

  <PARAM NAME="ON_SUCCESS_SHOW_PAGE" VALUE=<?php echo "\"${successPage}\""; ?>>
  <PARAM NAME="ON_SUCCESS_PAGE_TARGET" VALUE=<?php echo "\"${successPageTarget}\""; ?>>
  <PARAM NAME="ON_FAILURE_SHOW_PAGE" VALUE=<?php echo "\"${failurePage}\""; ?>>
  <PARAM NAME="ON_FAILURE_PAGE_TARGET" VALUE=<?php echo "\"${failurePageTarget}\""; ?>>
  <PARAM NAME="SERVER_CALL_BACK_URL" VALUE=<?php echo "\"${serverCallBackUrl}\""; ?>>
  <PARAM NAME="IS_SHOW_PRINT_PREVIEW" VALUE=<?php echo "\"${isShowPrintPreview}\""; ?>>
  <PARAM NAME="VIEWER_PAGE" VALUE=<?php echo "\"${viewerPage}\""; ?>>
  <PARAM NAME="VIEWER_CONTROLS" VALUE=<?php echo "\"${viewerControls}\""; ?>>

  <COMMENT>
    <EMBED
      type="application/x-java-applet;version=1.4.2"
      name="smartj"
      alt="PDF - Print and View PDF from browser"
      pluginspage="http://java.sun.com/j2se/"
      CODEBASE="<?php echo $appletPath; ?>/browser_lib/"
      CODE="com.activetree.pdfprint.WebSilentPrintPdf"
      ARCHIVE=""
      cache_option="Plugin"
      cache_archive="bc.jar,jai_codec.jar,jai_core.jar,sjpdf_license.jar,sjpdf_browser.jar"
      WIDTH="2"
      HEIGHT="2"
      LICENSE_KEY=<?php echo "\"${licenseKey}\"\r\n"; ?>
      DOC_LIST=<?php echo "\"${docList}\"\r\n"; ?>
      DOC_ID=<?php echo "\"${docId}\"\r\n"; ?>
      PAGE_SCALING=<?php echo "\"${pageScaling}\"\r\n"; ?>
      AUTO_ROTATE_AND_CENTER=<?php echo "\"${autoRotateAndCenter}\"\r\n"; ?>
      AUTO_MATCH_PAPER=<?php echo "\"${autoMatchPaper}\"\r\n"; ?>
      IS_USE_PRINTER_MARGINS=<?php echo "\"${usePrinterMargins}\"\r\n"; ?>
      PRINTER_NAME=<?php echo "\"${printerName}\"\r\n"; ?>
      PRINTER_NAME_SUBSTRING_MATCH=<?php echo "\"${printerNameSubstringMatch}\"\r\n"; ?>
      PAPER=<?php echo "\"${paper}\"\r\n"; ?>
      COPIES=<?php echo "\"${copies}\"\r\n"; ?>
      COLLATE_COPIES=<?php echo "\"${collateCopies}\"\r\n"; ?>
      JOB_NAME=<?php echo "\"${jobName}\"\r\n"; ?>
      SHOW_PRINT_DIALOG=<?php echo "\"${showPrinterDialog}\"\r\n"; ?>
      SINGLE_PRINT_JOB=<?php echo "\"${singlePrintJob}\"\r\n"; ?>
      SHOW_PRINT_ERROR_DIALOG=<?php echo "\"${showErrorDialog}\"\r\n"; ?>
      PASSWORD=<?php echo "\"${password}\"\r\n"; ?>
      PRINT_QUALITY=<?php echo "\"${printQuality}\""; ?>
      SIDE_TO_PRINT=<?php echo "\"${side}\""; ?>
      STATUS_UPDATE_ENABLED=<?php echo "\"${statusUpdateEnabled}\""; ?>
      DEBUG=<?php echo "\"${debug}\""; ?>

      ON_SUCCESS_SHOW_PAGE=<?php echo "\"${successPage}\""; ?>
      ON_SUCCESS_PAGE_TARGET=<?php echo "\"${successPageTarget}\""; ?>
      ON_FAILURE_SHOW_PAGE=<?php echo "\"${failurePage}\""; ?>
      ON_FAILURE_PAGE_TARGET=<?php echo "\"${failurePageTarget}\""; ?>
      SERVER_CALL_BACK_URL=<?php echo "\"${serverCallBackUrl}\""; ?>
      IS_SHOW_PRINT_PREVIEW=<?php echo "\"${isShowPrintPreview}\""; ?>
      VIEWER_PAGE=<?php echo "\"${viewerPage}\""; ?>
      VIEWER_CONTROLS=<?php echo "\"${viewerControls}\""; ?>      
      >

      <NOEMBED>
        <p>No java runtime</p>
      </NOEMBED>
    </EMBED>
    </COMMENT>
</OBJECT>

<!--This section is for message display on browser page -->
        <p>Print status</p>
        <textarea id="status" rows="6" cols="80"  readonly="true"></textarea>
<!-- end message display -->

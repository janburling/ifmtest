
<?php
	$this->beginClip('Information');
	$this->renderPartial('/common/show', array('model'=>$model, 'tab'=>'Information'));
	$this->endClip();

	$clips = $this->clips;
	
	$tabs = array (
				'Information'=>array('title'=>'Information', 'content'=>$clips['Information']),
	);
 
	$this->widget('CTabView', array('tabs'=>$tabs, 'viewData'=>array()));
?>

<?php
$clientScript = Yii::app()->getClientScript();
$baseUrl = Yii::app()->baseUrl;

$clientScript->registerCoreScript('jquery');
//$clientScript->registerScriptFile($baseUrl.'/assets/82dbe2db/jquery.js');
$clientScript->registerScriptFile($baseUrl.'/assets/anytime/anytimec.js');
$clientScript->registerCssFile($baseUrl.'/assets/anytime/anytimec.css');
$clientScript->registerCssFile($baseUrl.'/assets/e27f5f8f/css/redmond/jquery-ui-1.7.1.custom.css');
?>
<script type="text/javascript">
$(document).ready(function()
{
	 $("#AuditLogSearchForm_to, #AuditLogSearchForm_from").AnyTime_picker(
	      {
	          format: "%Y-%m-%d %H:%i:%s",
	          askSecond: true,
	          theme: 'redmond'
	      }
	 )
	 .each( function( index, item ){ // add clear buttons to "from" and "to" fields
		 var field = $(item);
		 var fieldId = field.attr('id');
		 var clearId = 'clear' + fieldId;
		 field.after('<a href="#" id="' + clearId + '">Clear</a>' );
		 $("#" + clearId ).click( function(){$("#" + fieldId ).val('');return false; } );
	 });
	 /*
	 $("#AuditLogSearchForm_from").after('<a href="#" id="clearFrom">Clear</a>');
	 $("#clearFrom").click( function(){$("#AuditLogSearchForm_from").val('');return false; } );
	 
	 $("#AuditLogSearchForm_to").after('<a href="#" id="clearTo">Clear</a>');
	 $("#clearTo").click( function(){$("#AuditLogSearchForm_to").val('');return false; } ) ;
	 */

});
</script>
<h2><?php echo $model->modelLabel; ?> Search</h2>

<div class="yiiForm">
<?php
echo $form->render();
?>
</div>

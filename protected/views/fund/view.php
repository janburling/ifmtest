<?php

$modelName = $model->modelName;

$this->widget('TbAlert', array('id'=>'flashMessage'));

$tabs = array();

$tabId = $modelName.'-tab';
$tabs[$tabId] =
    array(
        'id'=>$tabId,
        'label'=>'Detail',
        'content'=>$this->renderPartial('/common/_detail', array('model'=>$model), true),
        'active'=>!$activeTab || $activeTab == $tabId,
    );

$tabId = 'ContextReport-tab';
$tabs[$tabId] =
    array(
        'id'=>$tabId,
        'label'=>'Reports',
        'content'=>$this->renderPartial('/common/contextReports', array('model'=>$model), true),
        'active'=>$activeTab == $tabId,
    );

$this->widget('TbTabs', array('tabs'=>$tabs, 'encodeLabel'=>false));

echo '<div class="form-actions">';
    $this->widget('TbButton', array('buttonType'=>'link', 'type'=>'primary', 'label'=>'Update', 'url'=>array('update', 'id'=>$model->primaryKey)));
    $this->widget('TbButton', array('buttonType'=>'link', 'label'=>'Clone', 'url'=>array('clone', $model->primaryKeyName=>$model->primaryKey)));
echo '</div>';


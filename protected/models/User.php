<?php

/**
 * This is the model class for table "User".
 */
class User extends RemarcModel
{

	public $rememberMe;
	public $loginOrganization;

	/**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('password', 'authenticate', 'on'=>'login'),
			array('loginOrganization', 'required', 'on'=>'login'),
			array('groupId, organizationId, modifiedUser', 'numerical', 'integerOnly'=>true),
			array('username, password, name, email', 'length', 'max'=>45),
			array('createdDate, modifiedDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('username, password, name, email, groupId, rememberMe, createdDate', 'safe', 'on'=>'search'),
			array('username, password, email, groupId, name, departmentIds', 'required' ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            #'Groups'=>array(self::MANY_MANY, 'Group', 'UserGroup(userId, groupId)'),
            'Organization'	=>array(self::BELONGS_TO,	'Organization', 'organizationId'),
            'Group'			=>array(self::BELONGS_TO,	'Group', 'groupId'),
		    'UserAuthItems'	=>array(self::HAS_MANY,		'UserAuthItem',	'userid'),
		);
	}
	
	public function attributeLabels() {
		return array(
			'departmentSummary'=>'Departments',
			'departmentIds'=>'Departments',
			'groupId'=>'Group',
		);
	}
	
	public function search()
	{
		if(isset($_REQUEST[$this->modelName])) $this->attributes=$_REQUEST[$this->modelName];

		$criteria = new CDbCriteria;
		$criteria->compare('lower("username")', strtolower($this->username), true);
		$criteria->compare('lower("name")', strtolower($this->name), true);
		$criteria->compare('lower("email")', strtolower($this->email), true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>20),
			'sort'=>array(
				'defaultOrder'=>array('code'=>false),
				'attributes'=>array(
					'*',
				),
			),
		));
	}
	
	public function getGridColumns() {
		return array(
			$this->gridColumnButtons,
			'username',
			'name',
			'email',
			'Group.name:text:Group',
			'Organization.name:text:Organization',
		);
	}
	
	
	public function getDetailAttributes() {
		return array(
			'username',
			'name',
			'email',
			'departmentSummary',
			'Group.name:text:Group',
			'Organization.name:text:Organization',
		);
	}

	/**
	 * @return array: grouped fields for column or tab display in show view
	 */
	public function getShowFieldsGrouping()
	{
		// add an empty (or text) element to display empty row
		return array(
		    'Information'	=>	array(
						'elements' => array(
		    					'username',
		    					'password',
		    					'name',
		    					'email',
		    					'groupId',
		    					'organizationId',
		    					),
		    			'visibleOnForm' => true,
		    			),
			'UserAuthItems'	=>	array(
						'elements' => array(
								'UserAuthItems',
						),
						'visibleOnForm' => false,
					),		    			
		); 
	}

    // config array for form builder and show view
    public function getFormConfig()
    {
    	if( isset($this->formConfig) ) return $this->formConfig;
    	
		$formConfig = array();
		$formConfig['title'] = ($this->isNewRecord? 'Create' : 'Save') . ' ' .  $this->modelLabel;
		
		$formConfig['elements']['username'] = array(
		            'type'		=> 'text',
		        );
		
		$formConfig['elements']['password'] = array(
		            'type'		=> 'password',
		        );
		
		$formConfig['elements']['name'] = array(
		            'type'		=> 'text',
		        );
		
		$formConfig['elements']['email'] = array(
		            'type'		=> 'text',
		        );

		$formConfig['elements']['groupId'] = $this->relatedDropdownConfig('Group');

		$formConfig['elements']['organizationId'] = $this->relatedDropdownConfig('Organization');

		// related lists
		$relatedClass	= 'UserAuthItem';
		$criteria	= new CDbCriteria;
	    $criteria	->addCondition( "\"assignments\".\"userid\" = {$this->primaryKey}" );
		$formConfig['elements']['UserAuthItems']	= array(
		            'type'				=> 'application.extensions.palmerhill.PHList',
					'relatedModel'		=> $relatedClass,
			    	'visible'			=> !$this->isNewRecord,
					'listViewConfig'	=> array(
							'elements' => self::model($relatedClass)->relatedListViewConfig['elements'],
				    		'criteria'		=> $criteria,
							'hideNavigation'=> true,
			    		),
		);
		
		// buttons		
		$formConfig['buttons'] = array(
		        'save'=>array(
		            'type'		=> 'submit',
		            'label'		=> $this->isNewRecord? 'Create' : 'Save',
		        ),
		        
		        'cancel'=>array(
		            'type'		=> 'button',
		        	'submit'	=> CHttpRequest::getUrlReferrer(),
		            'label'		=> 'Cancel',
		        ),
		        
		    );
		    
		$this->formConfig = $formConfig;
		return $this->formConfig;    	
    }
    
    public function getGroupName()		{ return isset($this->Group->name) ? $this->Group->name : ''; }
	public function getDefaultSort()	{ return array('name'=>'username','order'=>'ASC'); }
	public function getPrimaryKeyName()	{ return 'id'; }

	public function getIsAdmin() {
		$criteria = new CDbCriteria;
		$criteria->addCondition('"itemname"=:itemname');
		$criteria->addCondition('"userid"=:userid');
		$criteria->params = array('itemname'=>'Admin', 'userid'=>$this->id);
		//Yii::trace( 'getIsAdmin, ' . print_r($criteria, true));
		$adminRole = UserAuthItem::model()->find($criteria);
		//$adminRole = $this->UserAuthItems($criteria);		
		return $adminRole ? true : false;
	}
	
	public function getDepartmentSummary() {
		$departments = Department::model()->findAllByPk($this->departmentIds, array('index'=>'name'));
		return implode(', ', array_keys($departments));
	}
	
	public function afterFind() {
		if($this->departmentIds) $this->departmentIds = explode(',', $this->departmentIds);
		else $this->departmentIds = array();
	}
	
	public function beforeSave() {
	    if(parent::beforeSave())
	    {
			$this->organizationId = $this->Group->organizationId;
			Yii::app()->user->setState('organizationId', $this->organizationId);
			
			$this->username = strtolower($this->username);
			
			if($this->isNewRecord)
			{
				$this->password = md5($this->password);
			} 
			elseif(isset($this->password))
			{
				$oldPassword = User::model()->findByPk($this->id)->password;
				if($oldPassword != $this->password) $this->password = md5($this->password);
			}
			
			$this->departmentIds = implode(',', $this->departmentIds);
			return true;
	    }
	    else
	    {
	    	return false;
	    }
	} 

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())  // we only want to authenticate when no input errors
		{
			if($this->loginOrganization) {
				Yii::app()->user->setState('subdomain', $this->loginOrganization);
				Yii::trace($this->loginOrganization);
				$dbConfig = new DatabaseConfig;
				$dbConfig->updateDatabase($this->loginOrganization);
			}
			
			$identity=new UserIdentity($this->username,$this->password);
			$identity->authenticate();
			switch($identity->errorCode)
			{
				case UserIdentity::ERROR_NONE:
					$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
					Yii::app()->user->login($identity,$duration);
					break;
				case UserIdentity::ERROR_USERNAME_INVALID:
					$this->addError('username','Username is incorrect.');
					break;
				default: // UserIdentity::ERROR_PASSWORD_INVALID
					$this->addError('password','Password is incorrect.');
					break;
			}
		}
	}
}

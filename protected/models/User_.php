<?php

class User extends PHModel
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		return array(
			array('groupId, username, password, name, email', 'required'),
			array('username', 'length', 'max'=>45 ),
		);
	}

	public function relations()
	{
		return array(
            #'Groups'=>array(self::MANY_MANY, 'Group', 'UserGroup(userId, groupId)'),
            'Group'			=>array(self::BELONGS_TO,	'Group', 'groupId'),
		    'UserAuthItems'	=>array(self::HAS_MANY,		'UserAuthItem',	'userid'),
		);
	}
	
	public function beforeSave()
	{
	    if(parent::beforeSave())
	    {
			$this->organizationId = $this->Group->organizationId;
			Yii::app()->user->setState('organizationId', $this->organizationId);
			
			$this->username = strtolower($this->username);
			
			if($this->isNewRecord)
			{
				$this->password = md5($this->password);
			} 
			elseif(isset($this->password))
			{
				$oldPassword = User::model()->findByPk($this->id)->password;
				if($oldPassword != $this->password) $this->password = md5($this->password);
			}
			return true;
	    }
	    else
	    {
	    	return false;
	    }
		
	} 

	public function getViewAttributes() {
		return array(
			'group'			=> 	array('label'=>'Group', 		'type'=>'keyList'),
			'username'		=> 	array('label'=>'Username', 		'type'=>'text'),
			'password'		=> 	array('label'=>'Password', 		'type'=>'password'),
			'name'			=>	array('label'=>'Name',		 	'type'=>'text'),
			'email'			=>	array('label'=>'Email', 		'type'=>'text'),
		);
	}
	
	public function getListViewAttributes()
	{
		return array(
			'username'			=> array('label'=>'Userame', 	'width'=>'200'),
			'groupName'			=> array('label'=>'Group',	 	'width'=>'200'),
			'name'				=> array('label'=>'Name', 		'width'=>'200'),
			'email'				=> array('label'=>'Email', 		'width'=>'200'),
		);
	}

	public function getGroupName() { return isset($this->Group->name) ? $this->Group->name : ''; }
	
	public function getDefaultSort() { return array('name'=>'username','order'=>'ASC'); }

	public function getIsAdmin() {
		$criteria = new CDbCriteria;
		$criteria->addCondition('"itemname"=:itemname');
		$criteria->addCondition('"userid"=:userid');
		$criteria->params = array('itemname'=>'Admin', 'userid'=>$this->id);
		Yii::trace(print_r($criteria, true));
		$adminRole = UserAuthItem::model()->find($criteria);
		//$adminRole = $this->UserAuthItems($criteria);		
		return $adminRole ? true : false;
	}
}

<?php

class UserAuthItem extends RemarcModel
{
	public $excludeFromAudit	= true; 
	private $asOfDate;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'assignments';
	}

	public function primaryKey()
	{
	    return array('userid', 'itemname');
	}
	
	public function rules()
	{
		return array(
			//array(implode(', ', array_keys($this->viewAttributes)) . ', userid', 'safe'),
            array('itemname, username, bizrule, data, userid', 'safe' ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
	    return array(
	    		'User'		=> array(self::BELONGS_TO, 'User',		'userid'),
		        'AuthItem'	=> array(self::BELONGS_TO, 'AuthItem',	'itemname'),
	    );
	}
	
	public function search()
	{
		if(isset($_REQUEST[$this->modelName])) $this->attributes=$_REQUEST[$this->modelName];

		$criteria = new CDbCriteria;
		$criteria->compare('"userid"', $this->userid);
		$criteria->compare('lower("itemname")', strtolower($this->itemname), true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array('pageSize'=>20),
			'sort'=>array(
				'defaultOrder'=>array('code'=>false),
				'attributes'=>array(
					'*',
				),
			),
		));
	}
	
	public function getGridColumns() {
		return array(
			$this->gridColumnButtons,
			'User.username',
			'itemname',
		);
	}
	
	
	public function getDetailAttributes() {
		return array(
			//'username',
			//'itemname',
		);
	}
	
	public function getGridColumnButtons() {
		return array(
			'class'=>'TbButtonColumn',
			'buttons'=>array(
				'delete'=>array('url'=>'array("/$data->modelName/delete/", "id"=>$data->primaryKey);'),
			),
			'template'=>'{delete}',
		);
	}


	public function getUserAuthItemName()
	{
		return $this->UserAuthItem->name;
	}
	
	public function getUserId()
	{
		return $this->userid;
	}
		
	public function getUsername()
	{ 
		$userModel = User::model()->findByPk($this->userid);
		return isset($userModel) ? $userModel->username : $this->userid;
	}
	
	public function getRoleOptions()
	{
		$roleAuthItems = AuthItem::model()->findAll('type = :type and name != :admin', array('type'=>2, 'admin'=>'Admin'));
		$roles = array();
		foreach($roleAuthItems as $item)
			$roles[$item->name] = $item->name;
		if( User::model()->findByPk(Yii::app()->user->id)->isAdmin ) $roles['Admin'] = 'Admin';
		return $roles;
	}
	
	public function getModelName()		{ return 'UserAuthItem'; }
	public function getModelsName()		{ return 'UserAuthItems'; }

	public function getModelsLabel()	{ return 'User Roles'; }
	public function getModelLabel()		{ return 'User Role'; }
	
	public function getId()				{ return array('itemname'=>$this->itemname, 'userid'=>$this->userid); }
	public function getDefaultSort()	{ return array('name'=>'itemname','order'=>'ASC'); }

	public function updateDependentFields($key=array())
	{
		// return; // short circuit
		
		if($key)
		{
			//$foreignKey			= $this->getModelPrimaryKeyName($key['name']);
			$foreignKey			= self::model($key['name'])->primaryKeyName;
			//Yii::trace("key: " . print_r($key,1));
			if($key['name'] == 'User')
			{
				$this->userid	= $key['value'];
				//Yii::trace("userid: {$this->userid}");
			}
			
			//$form				= $this->form;
			//$form['username']->showOnly = true;
		}
	}

}

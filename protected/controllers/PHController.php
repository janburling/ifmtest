<?php

class PHController extends CController
{
	const		PAGE_SIZE=10;
	public		$defaultAction='list';
	protected	$_model;
	private 	$breadcrumbs = array();
	public 		$auditSignature;
	
	public function run($actionID)
	{
		$this->auditSignature = $actionID;
		parent::run($actionID);
	}
	
	/**
	 * Gets actual breadcrumbs
	 * @return unknown_type
	 */
	public function getBreadcrumbs(){
	        return $breadcrumbs;
	}
	
	/**
	 * Sets actual breadcrumbs
	 * @param array $value
	 */
	public function setBreadcrubms($value){
	        if (CPropertyValue::ensureArray($value)){
	                $this->breadcrumbs = $value;
	        }else{
	                throw new CException('Breadcrumbs must be an array');
	        }
	}

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				//'actions'=>array('list','show','listXML', 'relatedListXml', 'create','update', 'delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actions() {
        return array(
        	'show'				=> array('class'=>'application.actions.ShowAction'),
        	'create'			=> array('class'=>'application.actions.CreateAction'),
        	'update'			=> array('class'=>'application.actions.UpdateAction'),
        	'delete'			=> array('class'=>'application.actions.DeleteAction'),
        	'clone'				=> array('class'=>'application.actions.CloneAction'),
        	'list'				=> array('class'=>'application.actions.ListAction'),
        	'listXml'			=> array('class'=>'application.actions.ListXmlAction'),    
        	'listPick'			=> array('class'=>'application.actions.ListPickAction'),    
        	'relatedList'		=> array('class'=>'application.actions.RelatedListAction'),
        	'relatedListXml'	=> array('class'=>'application.actions.RelatedListXmlAction'),
        );
   	}
   	
   	public function actionBatchUpdate()
   	{
   		$models	= CActiveRecord::model($this->modelName)->findAllByPk($_REQUEST['ids']);
   		foreach ($models as $model)
   		{
			Yii::trace("{$model->modelName}: {$model->primaryKey}");
   			$model->checkActionAccess( $this );
   		}
   		$modelName	= $models[0]->modelName;
   		
   		// form submitted
   		if( isset($_POST[$modelName]) )
   		{
   			$valid = true;
   			foreach ($models as $model)
   			{
   				$model->attributes	= $_POST[$modelName][$model->primaryKey];
   				$valid	= $model->validate() && $valid;
   			}
   			
   			if($valid)
   			{
   				foreach ($models as $model) $model->save();
			    $passParameters	= array();
			    $route			= 'list';									
				$url			= $this->createUrl( $route , $passParameters );	      
				$this->redirect($url);
   			}
   		}
   		$this->render('/common/batchUpdate', array( 'models' => $models ) );
   	}

	// obsolete functions, instead used an instance of the model in context
	
	//public function findAllModels($criteria=null) 	{ return Address::model()->findAll($criteria); }
	//public function countModels($criteria=null) 	{ return Address::model()->count($criteria); }
	//public function findAllModelsByPk($ids) 		{ return Address::model()->findAllByPk($ids); }	
	
	//public function getListViewAttributes()	{ return Address::model()->listViewAttributes; }
	//public function getModelLabel() 		{ return Address::model()->modelLabel; }
	//public function getModelsLabel() 		{ return Address::model()->modelsLabel; }
	//public function getDefaultSort() 		{ return Address::model()->defaultSort; }

	public function getModel()				{ return $this->_model; }

	public function renderGroupsTabs( $tabsGroup, $form, $update = true )
	{
		$form->isUpdate	= $update;
		$tabs		= array();
		$header		= '';
		foreach( $tabsGroup as $title=>$element )
		{
			if( $update && isset($element['visibleOnForm']) && !$element['visibleOnForm'] ) continue;
			if( isset( $form->elements[$title] ) && !$form->elements[$title]->visible ) continue;
			
			$elements = $element['elements'];
			// header info displayed above all tabs
			if( strtolower($title) == 'header' )
			{
				$header = $update? $form->renderElementsGroup( $elements ) : $form->renderShowElementsGroup( $elements );
				$header .= '<br/><br/>'; 
				continue;
			}
			$this->beginClip($title);
			echo $update? $form->renderElementsGroup( $elements ) : $form->renderShowElementsGroup( $elements );
			$this->endClip();
		}
		
		$clips 		= $this->clips;
		$viewData	= array ('form' => $form);
		
		foreach( $tabsGroup as $title=>$element )
		{
			if( $update && isset($element['visibleOnForm']) && !$element['visibleOnForm'] ) continue;
			if( isset( $form->elements[$title] ) && !$form->elements[$title]->visible ) continue;
			if( strtolower($title) == 'header' ) continue;
			
			$tabs["tab{$element['elements'][0]}"] = array('title'=>CModel::generateAttributeLabel($title) , 'content'=>$clips[$title]);
		}
		
		echo $header;
		$this->widget('CTabView', array('tabs'=>$tabs, 'viewData'=>$viewData));
	}
	
	public function renderGroupsColumns( $tabsGroup, $form, $update = true )
	{
		$form->isUpdate	= $update;
		echo '<table class="columns"><tr>';
		foreach( $tabsGroup as $title=>$element )
		{
			$elements = $element['elements'];
			echo "<td valign=\"top\" id=\"column$title\">";
			if( $update )
			{
				echo $form->renderElementsGroup( $elements );
			}
			else
			{
				echo $form->renderShowElementsGroup( $elements );
			}
			echo '</td>';
		}
		echo '</tr></table>';
	}
	
	public function getModelName() { return ucfirst( $this->id ); }
	 
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			$newModel		= $this->createModel();
			$primaryKeyName	= $newModel->primaryKeyName;
			if(isset($_GET[$primaryKeyName]))
				$this->_model= $newModel->findbyPk($_GET[$primaryKeyName]);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
	public function createModel() {
		return new $this->modelName;
	}
	
	public function displayOperationMessage()
	{
	}
	
	public static function setOperationMessage($message, $returnUrl=null, $isError=false)
	{
		Yii::app()->user->setState('operationMessage', $message);
		Yii::app()->user->setState('isError', $isError);
		if($returnUrl)
			Yii::app()->request->redirect($returnUrl);
	}
	
	public static function getCancelUrl($model=null)
	{
		if(true)
		{
			return CHttpRequest::getUrlReferrer();
		}
	}
	
	public function updateDependentFields($model, $key=array())
	{
		if( isset($_REQUEST['updateDependentFields']))
		{
			$model->attributes	= $_REQUEST[$model->modelName];
			$model->updateDependentFieldsBy	= $_REQUEST['updateDependentFields'];
			$model->updateDependentFields($key);
			return false;
		}
		return true;
	}
}
?>
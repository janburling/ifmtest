<?php

class SiteController extends CController
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image
			// this is used by the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xEBF4FB,
			),
		);
	}

   	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users
				'actions'=>array('login', 'error'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user
				'actions'=>array('*'),
				'users'=>array('*'),
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$contact=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$contact->attributes=$_POST['ContactForm'];
			if($contact->validate())
			{
				$headers="From: {$contact->email}\r\nReply-To: {$contact->email}";
				mail(Yii::app()->params['adminEmail'],$contact->subject,$contact->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('contact'=>$contact));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$loginForm = new LoginForm;
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$loginForm->attributes=$_POST['LoginForm'];
			// validate user input and redirect to previous page if valid
			if($loginForm->validate())
			{
				$returnUrl = Yii::app()->user->returnUrl;
				if( $returnUrl == '/site/login') $returnUrl = '/';
				$this->redirect( $returnUrl );
			}
		}
		// display the login form
		$this->render('login',array('form'=>$loginForm));
	}

	public function actionImport() {
		$form = new ImportForm;
		$output = '';
		if(isset($_POST['ImportForm']))
		{
			$form->attributes = $_POST['ImportForm'];
			if($form->validate()) {
				$output = '';
				$form->import();
				#$this->render('import',array('form'=>$form, 'output'=>$output));
				#CApplication::end();
			}
			else $this->render('import',array('form'=>$form, 'output'=>$output));
		}
		else $this->render('import',array('form'=>$form, 'output'=>$output));
	}

	public function actionToggleGlobaluser() {
		Yii::app()->user->setState('globaluser', !Yii::app()->user->globaluser);
		$this->render('admin',array());
	}
	public function actionImportGetProgress() {
		#Yii::log('Entering getprogress', 'error', '');
		if(isset($_GET['uniqueId'])) {
			#Yii::log('PROGRESS_KEY=' . $_GET['uniqueId'], 'error', '');
			$status = apc_fetch('upload_'.$_GET['uniqueId']);
			$percentageComplete = round($status['current'] / $status['total'] * 100);
			#Yii::trace('PERCENT=' . $percentageComplete);

			echo header('Expires: Tue, 08 Oct 1991 00:00:00 GMT') .
				header('Cache-Control: no-cache, must-revalidate') .
				$percentageComplete;
		}

	}

	public function actionMaintenance() {
		echo '<h1>We are performing system maintenance. Please try again later.</h1>';
	}

	/**
	 * Logout the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionAdmin()
	{
		$this->render('admin');
	}

	public function actionRoles()			{ $this->renderPartial('roles');}
	public function actionAlterTables()		{ $this->renderPartial('alterTables');}
	public function actionRolesAllModels()	{ $this->renderPartial('rolesAllModels');}

	public function actionError()
	{
	    $error = Yii::app()->errorHandler->error;
	    if ($error)
	    {
	    	if( Yii::app()->params['errorLogging']['enabled'] ) // email report
	    	{
				// filter out
				$filterOut = 	strpos($error['message'], 'Unable to resolve the request "sjPDF') !== false ||
								strpos($error['message'], 'The system is unable to find the requested action "com.activetree') !== false ||
								$_SERVER['REQUEST_URI'] == '/payment/show/id/com.activetree.pdfprint.WebSilentPrintPdf' ||
								strpos($error['message'], 'Unable to resolve the request "crystalclear/') !== false;

				if( !$filterOut ) // email report
				{
			    	$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					$headers .= 'From: ' . Yii::app()->params['errorLogging']['sentFrom'] . "\r\n";

					mail(	Yii::app()->params['errorLogging']['to'],
							Yii::app()->params['errorLogging']['subject'],
							$this->renderPartial('/site/errorEmailReport', array('error'=>$error), true ),
							$headers
						);
				}
	    	}

			$this->render('/site/errorEmailReport', array('error'=>$error));
	    }
	    else
		    throw new CHttpException(404, 'Page not found.');
	}
}

<?php

class UserAuthItemController extends RemarcController
{
	public function createModel() {
		return new UserAuthItem;
	}

	public function loadUserAuthItem($id=null)
	{
		//Yii::trace(print_r($_GET['userAuthItemId']['itemname'], true));
		
		if($this->_model===null)
		{
			if(isset($_GET['id'])) {
				if(is_array($_GET['id'])) {
					$id = $_GET['id'];
				}
				else {
					$id = explode('_', $_GET['id']);
					$id = array('itemname'=>$id[0], 'userid'=>$id[1]);
				}
			}
			
			if($id!==null)
				$this->_model=UserAuthItem::model()->findbyPk($id);
			elseif(isset($_GET['userAuthItemId']['userid']) && isset($_GET['userAuthItemId']['itemname']))
				$this->_model=UserAuthItem::model()->findByPk(array(
					'userid'=>$_GET['userAuthItemId']['userid'],
					'itemname'=>$_GET['userAuthItemId']['itemname'],
				));
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
	public function loadModel($id=null) {
		return $this->loadUserAuthItem($id);
	}

	public function getModelName() { return 'UserAuthItem'; }
	public function getFKeyName() { return null; }
		
	public function getChildModel($cModelName) {
		return UserAuthItem::model()->getRelatedModel($cModelName);
	}

}
<?php

class ReportParameterController extends RemarcController
{
    public function actions() {
        return array(
        	'create'			=> array('class'=>'application.actions.CreateAction'),
        	'show'				=> array('class'=>'application.actions.ShowAction'),
        	'update'			=> array('class'=>'application.actions.UpdateAction'),
        	'delete'			=> array('class'=>'application.actions.DeleteAction'),
        	'clone'				=> array('class'=>'application.actions.CloneAction'),
        	'list'				=> array('class'=>'application.actions.ListAction'),
        	'listXml'			=> array('class'=>'application.actions.ListXmlAction'),    
        	'listPick'			=> array('class'=>'application.actions.ListPickAction'),    
        	'relatedList'		=> array('class'=>'application.actions.RelatedListAction'),
        	'relatedListXml'	=> array('class'=>'application.actions.RelatedListXmlAction'),
        );
   	}

	public function getChildModel($cModelName) {
		return ReportParameter::model()->getRelatedModel($cModelName);
	}

}
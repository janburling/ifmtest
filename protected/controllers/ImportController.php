<?php
class ImportController extends RemarcController {
	public function actionCreate(){
		$model = new $this->modelName;
		$model->scenario = 'insert';
		if(isset($_REQUEST['payrollId'])) $model->payrollId = $_REQUEST['payrollId'];
		if(isset($_POST[$model->modelName])) {
			$model->attributes=$_POST[$model->modelName];
			$model->importFile = CUploadedFile::getInstance($model,'importFile');	
			if($model->save()) {
				$model->importFile->saveAs("/var/local/imports/$model->primaryKey.txt");
				$this->redirect(array('view', 'id'=>$model->primaryKey));
            }
		}
		$this->render('/import/create',array(
			'model'=>$model,
		));
	}
	
	public function actionDownloadFile($id) {
		$model = $this->loadModel($id);
		$path = "/var/local/imports/{$model->primaryKey}.txt";
		if(file_exists($path)) {
			return Yii::app()->getRequest()->sendFile("{$model->primaryKey}.txt", @file_get_contents($path));
		}
		else echo "File not found.";
	}
	
	public function actionCreateWorkTimes($id) {
		$model = $this->loadModel($id);
		if($model->TimeImportLineErrors) {
			Yii::app()->user->setFlash('error',  "Correct all errors before creating Work Times.");		
		}
		if(count($model->WorkTimes)) {
			Yii::app()->user->setFlash('error',  "Work Times already exist for this import.");			
		}
		
		else {
			ini_set('memory_limit', '512M');
			set_time_limit(60);
			$lineCount = 0;
			foreach($model->TimeImportLines as $timeImportLine) {
				$timeImportLine->createWorkTime();
				if($timeImportLine->hasErrors()) $model->addErrors($timeImportLine->errors);
				else $lineCount++;
			}
			
			if($lineCount) {
				Yii::app()->user->setFlash('success', "Created $lineCount record(s)." . $model->formattedErrors);
			} else {
				Yii::app()->user->setFlash('error',  "No records created." . $model->formattedErrors);
			}
		}
		$this->redirect(array('/timeImport/view', 'id'=>$model->timeImportId, 'activeTab'=>'WorkTime-tab'));		
	}
	
	public function actionDeleteWorkTimes($id) {
		$model = $this->loadModel($id);
		$count = WorkTime::model()->deleteAllByAttributes(array('timeImportId'=>$model->timeImportId));
		Yii::app()->user->setFlash('info', "Deleted $count record(s).");
		$this->redirect(array('/timeImport/view', 'id'=>$model->timeImportId, 'activeTab'=>'WorkTime-tab'));		
	}
	
	public function actionValidateLines($id) {
		$model = $this->loadModel($id);
		TimeImportLineError::model()->deleteAllByAttributes(array('timeImportId'=>$model->timeImportId));
		
		foreach($model->TimeImportLines as $timeImportLine) {
			$timeImportLine->createLineErrors();
		}
		$this->redirect(array('/timeImport/view', 'id'=>$model->timeImportId, 'activeTab'=>'TimeImportLineError-tab'));		
	}

	public function actionProcessFile($id) {
		
		ini_set('memory_limit', '512M');
		set_time_limit(120);
		
		$importModel = $this->loadModel($id);
		
		$process = isset($_REQUEST['process']);
		
		//$employeeIdentifiers = Employee::model()->findAll(array('index'=>'employeeIdentifier', 'select'=>'"employeeId", "employeeIdentifier", "firstName", "lastName"', 'with'=>'PrimaryPosition'));
		$employeeIdentifiers = Employee::model()->findAll(array('index'=>'employeeIdentifier', 'select'=>'"employeeId", "employeeIdentifier", "firstName", "lastName"'));
		if($importModel->type == 'DeductionTransaction') {
			$codes = Deduction::model()->findAll(array('index'=>'code', 'select'=>'"deductionId", "code", "description"'));
			$codeIndex = 'deductionId';
		}
		elseif($importModel->type == 'GrossAdditionTransaction' || $importModel->type == 'TotalTransaction') {
			$codes = GrossAddition::model()->findAll(array('index'=>'code', 'select'=>'"grossAdditionId", "code", "description"'));
			$codeIndex = 'grossAdditionId';
		}
		elseif($importModel->type == 'TimeAccrualTransaction') {
			$codes = TimeAccrual::model()->findAll(array('index'=>'code', 'select'=>'"timeAccrualId", "code", "description"'));
			$codeIndex = 'timeAccrualId';			
		}
		
		if($importModel->mode == 'C') $employeePayrolls = EmployeePayroll::model()->findAll(array('index'=>'employeeId', 'select'=>'"employeePayrollId", "employeeId"', 'condition'=>'"payrollId"='.$importModel->payrollId));
			
		if($importModel->type != 'TotalTransaction' && $importModel->type != 'TimeAccrualTransaction') {
			$accounts = Account::model()->findAll(array('index'=>'accountNumber', 'select'=>'"accountId", "accountNumber", "accountDescription"'));
			$departments = Department::model()->findAll(array('index'=>'code', 'select'=>'"departmentId", "code", "description"'));
			$titles = Title::model()->findAll(array('index'=>'code', 'select'=>'"titleId", "code", "description"'));
		}
		
		$model = new $importModel->type;
		$models = $model->findAllByAttributes(array('payrollId'=>$importModel->payrollId));
		$modelsArray = array();
		
		foreach($models as $model) {
			if($importModel->type == 'TotalTransaction' || $importModel->type == 'TimeAccrualTransaction') $modelsArray[$model->employeeId][$model->$codeIndex] = $model;
			else $modelsArray[$model->departmentId][$model->employeeId][$model->$codeIndex] = $model;
		}
		
		$lines = file("/var/local/imports/{$importModel->importId}.txt");
		$count = 0;
		
		$content = '<table class="table table-striped">';
		$content .= '
			<th>File EmpId</th>
			<th>File Code</th>
			<th>File Amount</th>';
			
		if($importModel->type != 'TotalTransaction' && $importModel->type != 'TimeAccrualTransaction') {
			$content .= '<th>File Account</th>';
			$content .= '<th>File Dept</th>';
			$content .= '<th>File Title</th>';
		}
		
		if($importModel->type == 'GrossAdditionTransaction') {
			$content .= '
				<th>File Hours</th>
				<th>File Rate</th>
			';
		}
		elseif($importModel->type == 'TimeAccrualTransaction') {
			$content .= '<th>File Date</th>';	
		}

		$content .= '
			<th>Lookup Employee</th>
			<th>Lookup Code</th>
		';
		
		if($importModel->mode == 'U') {
			if($importModel->type == 'GrossAdditionTransaction') {
				$content .= '
					<th>Current Hours</th>
					<th>Current Rate</th>
				';
			}
			$content .= '
				<th>Current Amount</th>
				<th>Difference</th>
			';
		}
		if($importModel->type != 'TotalTransaction' && $importModel->type != 'TimeAccrualTransaction') {
			$content .= '
				<th>Lookup Account</th>
				<th>Lookup Dept</th>
				<th>Lookup Title</th>
			';
		}
		
		Yii::trace(print_r(array_keys($codes), true));

		foreach ($lines as $line_num => $line) {
			$line = trim($line);
			$lineArray = explode(',',$line);
			$employee = null;
			$record = null;;
			$employeePayroll = null;
			$account = null;
			$department = null;
			$title = null;
			$transaction = null;
			$rowClass = "";
			$diff = "";
			
			if(count($lineArray) > 1) {
				$lineArray[1] = trim($lineArray[1], '"'); //trim quotes from code
				if(isset($lineArray[3])) $lineArray[3] = trim($lineArray[3], '"'); //trim quotes from account
				
				if(isset($employeeIdentifiers[$lineArray[0]])) $employee = $employeeIdentifiers[$lineArray[0]];
				else $rowClass = "error";
				
				if($importModel->mode == 'C' && $employee) {
					if(isset($employeePayrolls[$employee->employeeId])) $employeePayroll = $employeePayrolls[$employee->employeeId];
					else $rowClass = "error";
				}
				//else $rowClass = "error";
				
				if(isset($codes[$lineArray[1]])) $record = $codes[$lineArray[1]];
				else $rowClass = "error";

				if($importModel->type != 'TotalTransaction' && $importModel->type != 'TimeAccrualTransaction') {
					if(isset($accounts[$lineArray[3]])) $account = $accounts[$lineArray[3]];
					if(isset($departments[$lineArray[4]])) $department = $departments[$lineArray[4]];
					elseif($importModel->mode == 'U') $rowClass = "error";
					if(isset($titles[$lineArray[5]])) $title = $titles[$lineArray[5]];
				}
						
				if($rowClass != "error") {
					if($importModel->mode == 'C') {						
						$transaction = new $importModel->type;
						$transaction->$codeIndex = $record->$codeIndex;
						$transaction->payrollId = $importModel->payrollId;
						$transaction->employeeId = $employee ? $employee->employeeId : null;
						$transaction->employeePayrollId = $employeePayroll ? $employeePayroll->employeePayrollId : null;
	
						if($importModel->type == 'TimeAccrualTransaction') $transaction->hoursAccrued = $lineArray[2];
						else $transaction->amount = $lineArray[2];
						
						if($importModel->type != 'TotalTransaction' && $importModel->type != 'TimeAccrualTransaction') {
							$transaction->accountId = $account ? $account->primaryKey : null;
							$transaction->departmentId = $department ? $department->primaryKey : null;
							$transaction->titleId = $title ? $title->primaryKey : null;
						}
						
						if($importModel->type == 'GrossAdditionTransaction') {
							$transaction->hours = $lineArray[6];
							$transaction->rate = $lineArray[7];
						}
						elseif($importModel->type == 'TimeAccrualTransaction') {
							$transaction->endDate = $lineArray[3];
							$transaction->startDate = $lineArray[3];
						}						
						if($process) {
							if($transaction->save()) $count++;
							else $importModel->adderrors($transaction->getErrors());
						}
					}
					elseif($importModel->mode == 'U') {
						if($importModel->type == 'TotalTransaction' || $importModel->type == 'TimeAccrualTransaction') 
							$transaction = isset($modelsArray[$employee->primaryKey][$record->primaryKey]) ? $modelsArray[$employee->primaryKey][$record->primaryKey] : null;
						else 
							$transaction = isset($modelsArray[$department->primaryKey][$employee->primaryKey][$record->primaryKey]) ? $modelsArray[$department->primaryKey][$employee->primaryKey][$record->primaryKey] : null;
						if($transaction) {
							if($importModel->type == 'TimeAccrualTransaction') {
								if($transaction->hoursAccrued != $lineArray[2]) 
								$diff = round($transaction->hoursAccrued - $lineArray[2], 2);
							}
							elseif($transaction->amount != $lineArray[2]) 
								$diff = round($transaction->amount - $lineArray[2], 2);
							elseif($importModel->type == 'GrossAdditionTransaction') {
								if($transaction->hours != $lineArray[6])
									$diff = round($transaction->hours - $lineArray[6], 2) . ' hours';
								elseif($transaction->rate != $lineArray[7]) 
									$diff = round($transaction->rate - $lineArray[7], 2) . ' [rate]';
							}
							if($diff) {
								if($process) {
									if($importModel->type == 'TimeAccrualTransaction') $transaction->hoursAccrued = $lineArray[2]; 
									else $transaction->amount = $lineArray[2];
									if($importModel->type == 'GrossAdditionTransaction') {
										$transaction->hours = $lineArray[6];
										$transaction->rate = $lineArray[7];
									}
									if($transaction->save()) $count++;
									else $importModel->adderrors($transaction->getErrors());
								}
								$rowClass = "warning";
							}
						}
						else $rowClass = "error";
					}
				}

				
				$content .= "<tr class='$rowClass'>";

				$content .= "<td>".$lineArray[0]."</td>";
				$content .= "<td>".$lineArray[1]."</td>";
				$content .= "<td>".$lineArray[2]."</td>";
				if($importModel->type != 'TotalTransaction' && $importModel->type != 'TimeAccrualTransaction') {
					$content .= "<td>".$lineArray[3]."</td>";
					$content .= "<td>".$lineArray[4]."</td>";
					$content .= "<td>".$lineArray[5]."</td>";
				}
					
				if($importModel->type == 'GrossAdditionTransaction') {
					$content .= "<td>".$lineArray[6]."</td>";
					$content .= "<td>".$lineArray[7]."</td>";
				}
				elseif($importModel->type == 'TimeAccrualTransaction') {
					$content .= "<td>".$lineArray[3]."</td>";
				}

				if($importModel->mode == 'U') {
					$content .= "<td>" . ($employee ? "$employee->name" : "Employee not found") . "</td>";
					$content .= "<td>" . ($record ? "$record->code" : "Record not found") . "</td>";
					if($importModel->type == 'GrossAdditionTransaction') {
						$content .= "<td>" . ($transaction ? $transaction->hours : "Transaction not found") . "</td>";
						$content .= "<td>" . ($transaction ? $transaction->rate : "Transaction not found") . "</td>";
					}
					if($importModel->type == 'TimeAccrualTransaction') $content .= "<td>" . ($transaction ? $transaction->hoursAccrued : "Transaction not found") . "</td>";
					else $content .= "<td>" . ($transaction ? $transaction->amount : "Transaction not found") . "</td>";
					$content .= "<td>$diff</td>";
				}
				elseif($importModel->mode == 'C') {
					$content .= "<td>" . ($employeePayroll ? "$employeePayroll->name" : "Employee Payroll not found") . "</td>";
					$content .= "<td>" . ($record ? "$record->name" : "Record not found") . "</td>";
				}
				if($importModel->type != 'TotalTransaction' && $importModel->type != 'TimeAccrualTransaction') {
					$content .= "<td>" . ($account ? "$account->accountNumber" : "Account not found") . "</td>";
					$content .= "<td>" . ($department ? "$department->code" : "Department not found") . "</td>";
					$content .= "<td>" . ($title ? "$title->code" : "Title not found") . "</td>";
				}
				$content .= "</tr>";
			}
		}
		$content .= "</table>";		
		
		if($count) {
			Yii::app()->user->setFlash('success', ($importModel->mode == 'C' ? 'Created' : 'Updated') . " $count record(s)." . $importModel->formattedErrors);
		} else {
			Yii::app()->user->setFlash('error',  "No records updated." . $importModel->formattedErrors);
		}
		
		$this->render('preview',array('model'=>$importModel, 'content'=>$content));
	}
}
<?php

class MunicipalityController extends PHController
{
    public function actions() {
        return array(
        	'create'			=> array('class'=>'application.actions.CreateAction'),
        	'show'				=> array('class'=>'application.actions.ShowAction'),
        	'update'			=> array('class'=>'application.actions.UpdateAction'),
        	'delete'			=> array('class'=>'application.actions.DeleteAction'),
        	'clone'				=> array('class'=>'application.actions.CloneAction'),
        	'list'				=> array('class'=>'application.actions.ListAction'),
        	'listXml'			=> array('class'=>'application.actions.ListXmlAction'),    
        	'listPick'			=> array('class'=>'application.actions.ListPickAction'),    
        	'relatedList'		=> array('class'=>'application.actions.RelatedListAction'),
        	'relatedListXml'	=> array('class'=>'application.actions.RelatedListXmlAction'),
        );
   	}

	public function createModel() {
		return new Municipality;
	}

	public function loadMunicipality($id=null)
	{
		if($this->_model===null)
		{
			if($id!==null || isset($_GET['id']))
				$this->_model=Municipality::model()->findbyPk($id!==null ? $id : $_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
	public function loadModel($id=null) {
		return $this->loadMunicipality($id);
	}

	public function getModelName()					{ return 'Municipality'; }
	public function getFKeyName()					{ return 'municipalityId'; }
		
	public function getChildModel($cModelName) {
		return Municipality::model()->getRelatedModel($cModelName);
	}
	
	public function getCustomFKeyName($cModelName)	{ if($cModelName == 'Municipality') return null; }

}
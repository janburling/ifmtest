<?php

class UserController extends PHController
{
    public function actions() {
        return array(
        	'show'				=> array('class'=>'application.actions.ShowAction'),
        	'create'			=> array('class'=>'application.actions.CreateAction'),
        	'update'			=> array('class'=>'application.actions.UpdateAction'),
        	'delete'			=> array('class'=>'application.actions.DeleteAction'),
        	'clone'				=> array('class'=>'application.actions.CloneAction'),
        	'list'				=> array('class'=>'application.actions.ListAction'),
        	'listXml'			=> array('class'=>'application.actions.ListXmlAction'),    
        	'listPick'			=> array('class'=>'application.actions.ListPickAction'),    
        	'relatedList'		=> array('class'=>'application.actions.RelatedListAction'),
        	'relatedListXml'	=> array('class'=>'application.actions.RelatedListXmlAction'),
        );
   	}

	public function loadUser($id=null)
	{
		if($this->_model===null)
		{
			if($id!==null || isset($_GET['id']))
				$this->_model=User::model()->findbyPk($id!==null ? $id : $_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
	public function loadModel($id=null) { return $this->loadUser($id); }
	public function createModel() { return new User; }

	public function getModelName() { return User::model()->modelName; }
	public function getFKeyName() { return User::model()->fKeyName; }
	
	public function getCustomFKeyName($modelName=null) {
		if($modelName == 'UserAuthItem') return 'userid';
		else return $this->fKeyName;
	}
		
	public function getChildModel($cModelName) { return User::model()->getRelatedModel($cModelName); }
}

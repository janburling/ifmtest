<?php
class RemarcFormatter extends CFormatter
{ 
    public function formatAccountType($value) {
    	switch($value) {
    		case 'c': return 'Checking';
    		case 's': return 'Savings';
    		default: return '';
    	}
    }
    
    public function formatRole($value) {
		return array_key_exists($value, Yii::app()->params['roles']) ? Yii::app()->params['roles'][$value] : $value;
    }
    
    public function formatCompanyStructure($value) {
		return array_key_exists($value, 
			Yii::app()->params['companyStructures']) ? Yii::app()->params['companyStructures'][$value] : $value;
    }
    
    public function formatCompanyStructureType($value) {
		return array_key_exists($value, 
			Yii::app()->params['companyStructureTypes']) ? Yii::app()->params['companyStructureTypes'][$value] : $value;
    }
    
    public function formatOwnershipType($value) {
		return array_key_exists($value, 
			Yii::app()->params['ownershipTypes']) ? Yii::app()->params['ownershipTypes'][$value] : $value;
    }
    
    public function formatDepartment($value) {
		return array_key_exists($value, 
			Yii::app()->params['departments']) ? Yii::app()->params['departments'][$value] : $value;
    }
    
    public function formatTaskRequired($value) {
		return implode(' and ', array_intersect_key(Yii::app()->params['taskRequired'], array_flip($value)));
    }
    
    public function formatTaskListDefault($value) {
		return array_key_exists($value, 
			Yii::app()->params['taskListDefault']) ? Yii::app()->params['taskListDefault'][$value] : $value;
    }
    
    public function formatPhone($value) {
    	if(strlen($value) == 7)
	    	return substr($value, 0, 3) . '-' . substr($value, 3, 4);
    	if(strlen($value) == 10)
	    	return '(' . substr($value, 0, 3) . ') ' . substr($value, 3, 3) . '-' . substr($value, 6, 4);
	    else
	    	return $value;
    }
    
    public function formatState($value) {
		return array_key_exists($value, Yii::app()->params['states']) ? Yii::app()->params['states'][$value] : $value;
	}  
	
	public function formatFeeStructure($value) {
		return array_key_exists($value, Yii::app()->params['feeStructures']) ? Yii::app()->params['feeStructures'][$value] : $value;
	}   	
	
	public function formatBillStructure($value) {
		return array_key_exists($value, Yii::app()->params['billStructures']) ? Yii::app()->params['billStructures'][$value] : $value;
	}   	
	
	public function formatPaymentChannel($value) {
		return array_key_exists($value, Yii::app()->params['paymentChannels']) ? Yii::app()->params['paymentChannels'][$value] : $value;
	}  
	 	
	public function formatEnrollmentStatus($value) {
		return array_key_exists($value, Yii::app()->params['enrollmentStatuses']) ? Yii::app()->params['enrollmentStatuses'][$value] : $value;
	}   
	
    public function formatDollars($value) {
		return Yii::app()->locale->numberFormatter->formatCurrency($value, 'USD');
	}
	
    public function formatDollars3decimals($value) {
		return "$" . number_format((float)$value, 3, '.', '');
	}
	
    public function formatDollars12decimals($value) {
		return "$" . number_format((float)$value, 12, '.', '');
	}
	
    public function formatPercentage($value) {
		//return Yii::app()->locale->numberFormatter->formatPercentage($value);
		return round($value,4) . '%';
	}
	
	public function formatDatetimeToDate($value) {
		return Yii::app()->dateFormatter->format('MM/dd/yyyy', $value);
	}
	
	public function formatDatetime($value) {
		return $value ? Yii::app()->dateFormatter->format('MM/dd/yyyy hh:mm', $value) : '';
	}
	
	public function formatPhoneType($value) {
		$phoneType = ListOption::model()->findByAttributes(array('fieldName'=>'phoneType', 'key'=>$value));
		return $phoneType ? $phoneType->value : $value;	
	}

	public function formatProducts($values) {
		$products = array();
		$models = ListOption::model()->findAllByAttributes(array('fieldName'=>'product', 'key'=>$values));
		if($models) {
			foreach($models as $model) {
				$products[] = $model->value;
			}
			return implode(', ', $products);
		}
		return implode(', ', $values);	
	}

	public function formatIndustry($value) {
		$industry = ListOption::model()->findByAttributes(array('fieldName'=>'industry', 'key'=>$value));
		return $industry ? $industry->value : $industry;	
	}
	
	public function formatReferralSource($value) {
		$source = ListOption::model()->findByAttributes(array('fieldName'=>'referralSource', 'key'=>$value));
		return $source ? $source->value : $value;	
	}
	
	public function formatTimezone($value) {
		$source = ListOption::model()->findByAttributes(array('fieldName'=>'timezone', 'key'=>$value));
		return $source ? $source->value : $value;	
	}  
	
	public function formatPayrollStatus($value) {
		return Payroll::model()->statusLabels[$value];
	}    
	
	public function formatPaymentSchedule($value) {
		return PaymentSchedule::model()->findByPk($value)->description;
	} 
	   
	public function formatRateType($value) {
		return $value == 'S' ? 'Salary' : 'Hourly';
	}    
	
	public function formatDate($value) {
		//return(date('m/d/Y', strtotime($value)));
		return Yii::app()->dateFormatter->format('MM/dd/yyyy', $value);
	}
	
	public function formatSsn($value) {
		return substr($value,0,3).'-'.substr($value,3,2).'-'.substr($value,5,4);
	}

}
?>

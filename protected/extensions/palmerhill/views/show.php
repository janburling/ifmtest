
<?php $this->renderFile('assets/jqgrid/include.html')?>
<h2><?php $model[0]->modelLabel ?> List</h2>

<script type="text/javascript"> 
$(document).ready(function() {
	var mygrid = jQuery("#jqgrid1").jqGrid({ 
	    theme: 'redmond',
	    mtype: 'POST',
		datatype: 'xml',
	    useNavBar: true,
	    multiselect: true,
	    viewrecords: true,
	    viewsortcols: true,
	    gridview: true,
		width: '100%',
		height: '100%',
        onSelectRow: function(){document.getElementById('selectedRows').value = jQuery('#jqgrid1').getGridParam('selarrrow');},
		rowNum: <?php echo Yii::app()->params['defaults']['listRowSize']; ?>,
	    rowList: [10,20,30],
		pager: '#jqpager1',
		url:'<?php echo $this->createUrl('listXml', array('ids'=>$ids)); ?>',
		caption:'<?php echo $models[0]->modelsLabel; ?>',
	    sortname: '<?php echo $models[0]->defaultSort['name']; ?>',
		sortorder: '<?php echo $models[0]->defaultSort['order']; ?>',
		colNames:[ <?php foreach($models[0]->listViewAttributes as $columnKey=>$columnName) echo "'" . $columnName . "',";?>],
		colModel:[ <?php foreach($models[0]->listViewAttributes as $columnKey=>$columnName)
							echo "{name:'" . $columnKey . "', index:'" . $columnKey . "'},"; ?>
		], 
	})
	.navGrid('#jqgrid1',{edit:false,add:false,del:false,search:false,refresh:false})
	.navButtonAdd("#jqpager1",{caption:"Toggle",title:"Toggle Search Toolbar", buttonicon :'ui-icon-pin-s',
		onClickButton:function(){
			mygrid[0].toggleToolbar();
		}
	})
	.navButtonAdd("#jqpager1",{caption:"Clear",title:"Clear Search",buttonicon :'ui-icon-refresh',
		onClickButton:function(){
			mygrid[0].clearToolbar();
		}
	});
	mygrid.filterToolbar();	 
});

</script>

<table id="jqgrid1"></table>
<div id="jqpager1"></div>
<div id="filter" style="margin-left:30%;display:none">Search</div>

<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::hiddenField('selectedRows'); ?>
<?php echo CHtml::button('Show', array('submit'=>array('','redirectAction'=>'show'))); ?>
<?php echo CHtml::button('Edit', array('submit'=>array('','redirectAction'=>'update'))); ?>
<?php echo CHtml::button('Create', array('submit' => array('create'))); ?>
<?php echo CHtml::button('Delete', array('submit'=>array('','redirectAction'=>'deleteMulti'))); ?>

<?php echo CHtml::endForm(); ?>
<br />
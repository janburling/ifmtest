<?php

class PHCustomView extends CInputWidget
{
    public $relatedModel;
    public $parentModel;
    public $title 			= '';
    public $partialView;
    public $passParameters	= array();
    
	public function init()
    {
		//$this->passParameters['listModel']	= $listModel;
    	// this method is called by CController::beginWidget()
    }
    
    public function renderShow()
    {
    	return $this->controller->renderPartial( $this->partialView, $this->passParameters, true );
    }
 
    public function run()
    {
        // this method is called by CController::endWidget()
    	echo $this->renderShow();
    }
}
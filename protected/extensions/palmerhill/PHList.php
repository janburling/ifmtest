<?php

class PHList extends CInputWidget
{
    public $relatedModel;
	public $listViewConfig	= array();
    public $title 			= '';
    public $passParameters	= array();
    
	public function init()
    {
		
    	$listModel							= new $this->relatedModel;
		$listModel->listViewConfig			= $this->listViewConfig;
		$this->passParameters['listModel']		= $listModel;
		$this->passParameters['parentModel']	= $this->model;
		//Yii::trace("in PHList, listModel: {$listModel->modelName}, parent: {$this->model->modelName}");
		// this method is called by CController::beginWidget()
    }
    
    public function renderShow()
    {
    	return $this->render('application.views.common.list', $this->passParameters, true );
    }
 
    public function run()
    {
        // this method is called by CController::endWidget()

    	// fields to display
        /*
        Yii::trace("this->fields: " . print_r($this->fields,1));        
        if( isset( $this->fields ) && !empty($this->fields) )
        {
        	$elements = array();
        	
        	foreach( $this->fields as $field )
        	{
        		$elements[$field] = $listViewConfig['elements'][$field];
        	}
        	
        	$listViewConfig['elements'] = $elements;
        }
        */
		
    	echo $this->renderShow();
    }
}
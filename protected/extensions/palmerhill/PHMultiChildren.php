<?php

class PHMultiChildren extends CInputWidget
{
    public $listViewConfig = array();
    public $relatedModel;
	public function init()
    {
        // this method is called by CController::beginWidget()
    }
 
    public function run()
    {
        // this method is called by CController::endWidget()
        
		$passParameters	= array();
		$listModel		= new $this->relatedModel;
		$listModel->listViewConfig		= $this->listViewConfig;
		$passParameters['listModel']	= $listModel;
		
    	echo $this->render('application.views.common.list', $passParameters, true );
    }
}
<?php //added control group for bootstrap and  label to match other inputs 
?>
<div class="control-group ">
<?php echo CHtml::activeLabelEx($this->model, $this->attribute, array('class'=>'control-label')); ?>
<div class="controls">
<input 
	type="text" 
	id="<?php echo $this->id; ?>"
	class="timepicker"
	placeholder="mm/dd/yyyy <?php echo strpos($this->type, 'time') ? "hh:mm " . date('a') : ''; ?>" 
	value="<?php echo $this->value; ?>"
	name="<?php echo get_class($this->model) . $this->tabularLevel . '['.$this->attribute.']'; ?>" />

<?php //Removed registration from clientscript to allow ajax render partial w/o script duplication
echo "<script type='text/javascript'>
	jQuery('#{$this->id}').{$this->type}picker({$this->options});
</script>";
?>
</div></div>
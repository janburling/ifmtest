<?php
class DatetimePicker extends CWidget {

	public $assets = '';
	public $options = array();
	public $skin = 'default';
	
	public $model;
	public $attribute;
	public $value = '';
	
	public $language;
	public $type = 'datetime';
	public $tabularLevel = null;

	/*
	 * @ array The configurable options used if not overridden
	 */
	protected $defaults = array(
		'ampm'=>true,
		//'timeFormat'=>'hh:mm TT',
		//'dateFormat'=>'yy-mm-dd',
		//'showOn'=>'focus', //focus, button, both
		//'showSecond'=>false,
		//'changeMonth'=>false,
		//'changeYear'=>false,
	);

	public function init() {
		$this->registerScripts();
		$this->registerCss();
        
        if($this->model->{$this->attribute}) $this->value = $this->model->{$this->attribute};
		$this->options = array_merge($this->defaults, $this->options);
		$this->options = empty($this->options) ? '' : CJavaScript::encode($this->options);

		parent::init();
	}

	public function run(){
		$this->render($this->skin);		
	}
	
	public function registerScripts() {
		$this->assets = Yii::app()->assetManager->publish(dirname(__FILE__).DIRECTORY_SEPARATOR.'assets');		
		Yii::app()->clientScript
			->registerCoreScript( 'jquery' )
			->registerCoreScript( 'jquery.ui' )      
			->registerScriptFile( $this->assets.'/js/jquery.ui.timepicker.js' );
		
		//language support
		if (empty($this->language))
			$this->language = Yii::app()->language;
 
		if(!empty($this->language)){
			$path = dirname(__FILE__).DIRECTORY_SEPARATOR.'assets';
			$langFile = '/js/jquery.ui.timepicker.'.$this->language.'.js';

			if (is_file($path.DIRECTORY_SEPARATOR.$langFile))
				Yii::app()->clientScript->registerScriptFile($this->assets.$langFile);
		}
	}
	
	public function registerCss() {
		Yii::app()->clientScript
			->registerCssFile( $this->assets.'/css/ui.theme.smoothness/jquery-ui-1.8.22.custom.css' )
			->registerCssFile( $this->assets.'/css/datetimepicker.css' );
	}
}

?>
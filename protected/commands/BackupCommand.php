<?php 
class BackupCommand extends CConsoleCommand
{
    public function run($args)
    {
		$t1 = time();
		$hostname = trim( shell_exec( 'echo $HOSTNAME' ) );
		
		if( $hostname == 'palmer.securesites.net' ) // dev
		{
			ini_set('memory_limit', '300M');
			$baseFolder			= '/home/palmer/backup/db/by-org';
	    	$dailyBackupFolder	= '/home/palmer/backup/db/daily';
		}
		else
		{
			ini_set('memory_limit', '2048M');
			$baseFolder			= '/var/local/backup/db/by-org';
	    	$dailyBackupFolder	= '/var/local/backup/db/daily';
		}
    	
    	// restore db from daily backup
    	$dbRestoreFile		= $this->getRestoreFile($dailyBackupFolder);
    	echo "Restoring db from $dbRestoreFile (last modified: " . date ("Y-m-d H:i:s", filemtime($dbRestoreFile)) . ")\n";
    	//Yii::app()->end();
    	$dbName				= 'org_restore';
    	$this->restoreDb( $dbName, $dbRestoreFile );
    	
    	$dbConnection	= Yii::app()->dbRestore;
		//Yii::app()->db->enableProfiling = false;
		//$excludeModels	= array( 'AuditLog', 'ImportDefinitionLine', 'GroupFilter', 'UserGroup', 'AuthItem', 'UserAuthItem', 'itemchildren', 'items', 'assignments');
		$excludeModels	= explode( ' ', 'AuditLog GroupFilter UserGroup AuthItem UserAuthItem itemchildren items assignments');
		$tableNames		= array_diff( $dbConnection->schema->tableNames, $excludeModels);
		asort( $tableNames );
		//$tableNames		= array_slice( $tableNames, 0, 3 ); // for testing
		
		$organizationIds = array();
		if( $args )
		{
			$organizationIds = $args;
		}
		else
		{
			//Organization::model()->db = $dbConnection;
			$orgModels = Organization::model()->resetScope()->findAll();
			foreach( $orgModels as $orgModel )
			{
				$organizationIds[] = $orgModel->id;
			}
			//$organizationIds = array_slice( $organizationIds, 0, 2 ); // for testing
		}
		
		if( !$organizationIds )
		{
			echo "No orgs to backup... aborting.\n";
		}
    	
		$limit			= 0;
		$limitClause	= $limit? " LIMIT {$limit}" : '';
		
		$dayFolder		= "$baseFolder/" . date("Y-m-d");
		if( !file_exists($dayFolder) ) mkdir( $dayFolder );
		
		echo "Backing up orgs: ";
		
		foreach( $organizationIds as $organizationId )
		{
			//$organizationId = (int) $organizationId;
			echo "$organizationId... ";
			
			$orgFolder	= "$dayFolder/org-$organizationId";
			if( !file_exists($orgFolder) ) mkdir( $orgFolder );
			
			foreach( $tableNames as $tableName )
			{
				if( preg_match('/_backup$/', $tableName) ) continue;
				$buTable	= $tableName . '_backup';
				$commandStr	= "DROP TABLE IF EXISTS \"$buTable\";\n";
				$command	= $dbConnection->createCommand($commandStr);
				$rows		= $command->execute();
				
				$commandStr	= "CREATE TABLE \"$buTable\" AS SELECT * FROM \"$tableName\" WHERE \"organizationId\" = $organizationId $limitClause;";
				$command	= $dbConnection->createCommand($commandStr);
				//$command->bindParam(":organizationId", $organizationId );
				//echo "$commandStr\n";
				$rows		= $command->execute();
				
				// dump table
				$dumpCommand	= "su - postgres -c 'pg_dump -t \\\"$buTable\\\" --data-only --column-inserts $dbName' | fgrep 'INSERT INTO'";
				$sql 			= shell_exec( $dumpCommand );
				$sql 			= str_replace ( "INSERT INTO \"$buTable\" ", "INSERT INTO \"$tableName\" ", $sql );
				//$sql = preg_replace('/(\nSET )/', '/-- $1/', $sql);
				$tableFileName	= "$orgFolder/$tableName-$organizationId.sql";
				file_put_contents( $tableFileName, $sql);
			}
			
		}
    	echo "\nCleaning up...\n";
    	$this->cleanup( $dbName );
		
		$t2 = time();
		echo "\nTime: " . ($t2-$t1) . " seconds\nBackup complete.\n";
    }

    public function getRestoreFile( $path )
    {
    	$files = scandir( $path, 0);
    	return $files? "$path/{$files[4]}" : null; 
    }
    
    public function restoreDb( $db, $file)
    {
    	echo shell_exec( "su - postgres -c 'psql -c \"DROP DATABASE IF EXISTS $db;\"'" );
    	echo shell_exec( "su - postgres -c 'psql -c \"CREATE DATABASE $db;\"'" );
    	echo shell_exec( "su - postgres -c 'pg_restore -1 -d $db $file'" );
    	//echo shell_exec( "su - postgres -c 'pg_restore --schema-only -1 -d $db $file'" );
    }
    
    public function cleanup( $db )
    {
    	echo shell_exec( "su - postgres -c 'psql -c \"DROP DATABASE IF EXISTS $db;\"'" );
    }
}
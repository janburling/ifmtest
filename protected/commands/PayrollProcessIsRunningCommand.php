<?php 
class PayrollProcessIsRunningCommand extends CConsoleCommand
{
    public function run($args)
    {
    	ob_start(); //prevent error related to regenerating session id with multiple logins
    	
    	//echo "Running PayrollProcessIsRunningCommand and outputting to /var/www/setcs/protected/runime/backgroundProcess.log\n";
    	$orgs = Yii::app()->params['organizations'];

    	foreach($orgs as $name=>$label) {    
    		//echo "Running for $name => $label\n";	
			$dbConfig = new DatabaseConfig;
			$dbConfig->updateDatabase($name);

			BackgroundProcess::$db = Yii::app()->db;
			//$backgroundProcess = new BackgroundProcess;
			//echo print_r($backgroundProcess->getDbConnection()->connectionString);
			
			$backgroundProcesses = BackgroundProcess::model()->resetScope()->findAll(array( 'condition'=>' "status" = \'S\' OR "status" = \'Q\' '));
			//echo "found " . count($backgroundProcesses) . " backgroundProcesses\n";		
			foreach ($backgroundProcesses as $backgroundProcess )
			{
				$isRunning = $backgroundProcess->pid ? $this->isRunning($backgroundProcess->pid) : false;
				echo "backgroundProcess: {$backgroundProcess->primaryKey}, pid: {$backgroundProcess->pid}";
				echo " " . ($isRunning?'running':'** dead **') . "\n";
				if( !$isRunning )
				{
					// login user for to be availbale in context of triggers 
					Yii::app()->loginUser($backgroundProcess->userId);
					$userModel			= User::model()->findByPk($backgroundProcess->userId);
					
					$passedPayroll		= unserialize( base64_decode( $backgroundProcess->passedData ) );
					$passedData			= $passedPayroll->dataToPass;
					
					$payroll			= Payroll::model()->resetScope()->findByPk($passedPayroll->primaryKey);
					if($payroll) {
						$payroll->status			= $passedPayroll->status; // old status
						$payroll->auditSignature	= $payroll->auditSignature;
						$payroll->save();
					}
					$userModel					= User::model()->findByPk($backgroundProcess->userId);
					
					$backgroundProcess->status	= 'F';
					$backgroundProcess->endTime	= date( 'Y-m-d G:i:s' );
					$backgroundProcess->save();
					
					$email		= $userModel ? $userModel->email : "USER NOT FOUND";
					$header		= "Process Failed: {$backgroundProcess->description}";
					$payrollStatus = $payroll ? $payroll->statusLabel : "PAYROLL NOT FOUND";
					$emailBody	= "$header\n\nThe payroll's status was reverted back to $payrollStatus";
					mail('errors@remarc.com', $header, $emailBody);
					mail($email, $header, $emailBody);
				}
			}
		}
    }
    
    public function isRunning($pid)
    {
    	$output		= array();
		$isRunning	= false;
		
		exec( "ps -p $pid", $output );
		//print_r($output);
		
		if( isset($output[1]) )
		{
			$isRunning = in_array($pid, explode(' ', $output[1] ));
			//print_r(explode(' ', $output[1] ));
		}
		return $isRunning;
    }
    
}
<?php 
class BillExtractCommand extends CConsoleCommand
{
	public $filePrefix = 'extract';
	public $filePostfix;
	public $dumpPath;
	public function init()
	{
		
	}
	
    public function run($args)
    {
		$t1			= time();
		$hostname	= trim( shell_exec( 'echo $HOSTNAME' ) );
    	//echo 'Yii::app()->user->id: ' . Yii::app()->user->id;
    	//Yii::app()->end();
		ini_set('memory_limit', ( ( $hostname == 'palmer.securesites.net' ) ? '300M' : '4096M' ) );
		
		$currentUser	= Yii::app()->user->name;
		$userModel		= User::model()->findByPk(Yii::app()->user->id);
    	
		if( !$args )
		{
			echo "No ids passed.\n";
			Yii::app()->end();
		}
		
		$backgroundProcess				= BackgroundProcess::model()->findByPk($args[1]);
		$backgroundProcess->pid			= getmypid();
		$backgroundProcess->status		= 'Q';
		$backgroundProcess->save();
		
		// check queue for running processes
		$sleep				= 20;
		$criteria			= new CDbCriteria;
		$criteria->limit	= 1;
		$criteria->select	= 'id';
		$criteria->addCondition(' "status" = \'S\' OR "status" = \'Q\' ');
		$criteria->addCondition("id < {$backgroundProcess->primaryKey}");
		
		//sleep(10);
		
		$parentId			= $backgroundProcess->parentProcessId;
		do
		{
			// abort check
			$parentProcess	= $backgroundProcess->findByPk($parentId);
			if( $parentProcess->status == 'A' )
			{
				$t2				= time();
				$elapsed		= $t2-$t1;
				// set queued processes to Aborted
				$abortCriteria = new CDbCriteria;
				$abortCriteria->addCondition('"parentProcessId" = ' . $parentId );
				$abortCriteria->addCondition('"status" = \'Q\'');
				$c	= $backgroundProcess->updateAll( array('status'=>'A'), $abortCriteria );
				
				// kill running processes
				$killCriteria = new CDbCriteria;
				$killCriteria->addCondition('"parentProcessId" = ' . $parentId );
				$killCriteria->addCondition('"status" = \'S\'');
				$runningProcesses = $backgroundProcess->findAll($killCriteria);
				foreach ($runningProcesses as $runningProcess ) exec("kill -KILL {$runningProcess->pid}");
				
				$updateMessage	= date( 'G:i:s', $t2 ) . " - Extract Aborted";
				$parentProcess->description = $updateMessage;
				$parentProcess->save();
				
				// update extract summary
				$formModel		= unserialize( base64_decode( $backgroundProcess->passedData ) );
				$passedData		= $formModel->dataToPass;
				$extractSummary	= "<b>$updateMessage</b> <br/> {$passedData['extractSummary']}" .
				"<br/>Extract time: $elapsed seconds" .
				"<br/>Records in extract: " . 
				BillExtract::model()->count('"username" = :username', array('username'=>$currentUser));
				$userModel->extractSummary = $extractSummary;
				$userModel->save();
				
				//Yii::trace("aborting backgroundProcess: {$backgroundProcess->primaryKey}");
				Yii::app()->end();
			}
			
			// sleep if there's any process running
			$running	= $backgroundProcess->resetScope()->count($criteria);
			//echo 'count($running): ' . count($running) . "\n";
			if( $running )
			{
				//echo "sleeping for $sleep seconds... zzzzzzz\n";
				sleep($sleep);
			}
		}
		while($running);
		
		// start extract
		$backgroundProcess->status		= 'S';
		$backgroundProcess->save();
		
		echo "extract started...\n";
		$formModel		= unserialize( base64_decode( $backgroundProcess->passedData ) );
		$passedData		= $formModel->dataToPass; 
		$billsCriteria	= $passedData['criteriaToPass'];
		Yii::app()->user->setState('globaluser', $passedData['globaluser'] );
		//Yii::trace("backgroundProcess: {$backgroundProcess->primaryKey}, chunk criteria: " . print_r($billsCriteria, 1) );
		
		if( $formModel->dumpToFile )
		{
			$this->dumpPath				= $formModel->dumpPath;
			$this->filePrefix			= $formModel->filePrefix;
			$this->filePostfix			= $formModel->filePostfix;
			$formModel->dumpFileName	= $this->filePrefix . $backgroundProcess->primaryKey . $this->filePostfix;
		}
		
		$billsChunk		= Bill::model()->filtered()->findAll($billsCriteria);
		$chunkCount		= count($billsChunk);
		Yii::trace("backgroundProcess: {$backgroundProcess->primaryKey}, chunkCount: $chunkCount");
		if($chunkCount) $formModel->runBillExtractChunk($billsChunk);
		
		//sleep(20); // delay
		
		$t2				= time();
		$elapsed		= $t2-$t1;
		$userModel		= User::model()->findByPk(Yii::app()->user->id); // this would retreive the last extratc summary
		
		$backgroundProcess->endTime		= date( 'Y-m-d G:i:s', $t2);
		$backgroundProcess->status		= 'C';
		$backgroundProcess->save();
		
		$isChunk	= !empty($parentId);
		
		// email report
		if( false || !$isChunk ) // only send notification when parent process is complete, not for each chunk
		{
			$this->emailReport($backgroundProcess, $userModel, $elapsed, true);
		}
		
		// update parent process and extract summary
		if($isChunk)
		{
			$updateMessage	= date( 'G:i:s', $t2) . " - Completed {$backgroundProcess->description}";
			$parentProcess	= $backgroundProcess->findByPk($parentId);
			$parentProcess->description = $updateMessage;
			
			// check if all chunks are done
			$chunksCriteria			= new CDbCriteria;
			$chunksCriteria->limit	= 1;
			$chunksCriteria->select	= 'id';
			$chunksCriteria->addCondition(' "status" = \'S\' OR "status" = \'Q\' ');
			$chunksCriteria->addCondition(" \"parentProcessId\" = {$backgroundProcess->parentProcessId}");
			$chunkRunning = $backgroundProcess->resetScope()->count($chunksCriteria);
			
			if(!$chunkRunning) // all chunks completed
			{
				$parentProcess->status	= 'C';
				$parentProcess->endTime	= $backgroundProcess->endTime;
				
				$parentT1	= strtotime($parentProcess->startTime);
				$elapsed	= $t2-$parentT1;
				
				$extractSummary	= $passedData['extractSummary'] .
				"<br/>Extract time: $elapsed seconds" .
				"<br/>Records in extract: " . 
				BillExtract::model()->count('"username" = :username', array('username'=>$currentUser));
				
				if( $formModel->emailNotification )	$this->emailReport($backgroundProcess, $userModel, $elapsed, false);
				if( $formModel->dumpToFile )		$this->mergeDumpFiles($parentProcess);
				//$extractSummary = "{$userModel->extractSummary}<br/>$updateMessage";
			}
			else
			{
				$extractSummary = "{$userModel->extractSummary}<br/>$updateMessage";
			}
			
			$userModel->extractSummary = $extractSummary;
			$userModel->save();
			$parentProcess->save();
		}
    }
    
    public function emailReport($backgroundProcess, $userModel, $elapsed, $debug = false)
    {
		$email		= $userModel->email;
		//$email		= 'khalid.ajlouny@gmail.com'; 
		$header		= 'Bill Extract Complete';
		$emailBody	= "$header\n" . 
		"Start Time: {$backgroundProcess->startTime}\n" .
		"End Time: {$backgroundProcess->endTime}\n" .
		"Total time: $elapsed seconds\n\n"; 
		
		if($debug)
		{
			$emailBody	.= "=====================\n" .
			"Debug Info\n" .
			"Description: {$backgroundProcess->description}\n" . 
			"Memory usage after extract: " . (memory_get_usage(true) / (1024.00*1024.00) ) . " MB\n" .
			"Background Process: {$backgroundProcess->primaryKey}\n";
			/*
			$emailBody	.= preg_replace('/<br\\s*?\/??>/i', "\n", $extractSummary);
			*/
		}
		mail($email, $header, $emailBody);
    }
    
    public function mergeDumpFiles($parentProcess)
    {
		$chunksCriteria	= new CDbCriteria;
		$chunksCriteria->addCondition(" \"parentProcessId\" = {$parentProcess->primaryKey}");
		//Yii::trace( CVarDumper::dumpAsString($chunksCriteria) );
		$chunks			= $parentProcess->findAll($chunksCriteria);
		
		$fullDumpFile	= $this->dumpPath . $this->filePrefix . $this->filePostfix;
		//Yii::trace("fullDumpFile: $fullDumpFile");
		$fp	= fopen($fullDumpFile, 'w');
		foreach ($chunks as $chunk)
		{
			$chunkFileName	= $this->dumpPath . $this->filePrefix . $chunk->primaryKey . $this->filePostfix;
			fwrite($fp, file_get_contents($chunkFileName));
			//Yii::trace("chunkFileName: $chunkFileName, " );
		} 
		fclose($fp);
		
		// delete file chunks
		foreach ($chunks as $chunk)
		{
			$chunkFileName	= $this->dumpPath . $this->filePrefix . $chunk->primaryKey . $this->filePostfix;
			unlink($chunkFileName);
		}
		
		// compress dump file
		$command		= "cd $this->dumpPath; zip -D {$this->filePrefix}.zip {$this->filePrefix}{$this->filePostfix}";
		$commandOutput 	= `$command`;
    }

}
<?php 
class BackgroundProcessIsRunningCommand extends CConsoleCommand
{
    public function run($args)
    {
		/*
    	if( !$args )
		{
			echo "No pid passed.\n";
			Yii::app()->end();
		}
		*/
		
		$backgroundProcesses = BackgroundProcess::model()->resetScope()->findAll(array( 'condition'=>' "status" = \'S\' OR "status" = \'Q\' '));
		//echo "found " . count($backgroundProcesses) . " backgroundProcesses\n";
		foreach ($backgroundProcesses as $backgroundProcess )
		{
			$isRunning	= $this->isRunning($backgroundProcess->pid);
			//echo "backgroundProcess: {$backgroundProcess->primaryKey}, pid: {$backgroundProcess->pid}";
			//echo " " . ($isRunning?'running':'** dead **') . "\n";
			if( !$isRunning )
			{
				// login user for to be availbale in context of triggers 
				Yii::app()->loginUser($backgroundProcess->userId);
				$userModel					= User::model()->findByPk($backgroundProcess->userId);
				
				$backgroundProcess->status	= 'F';
				$backgroundProcess->endTime	= date( 'Y-m-d G:i:s' );
				$backgroundProcess->save();
				
				$header			= "{$backgroundProcess->description} - Failed";
				$updateMessage	= date('G:i:s') . " - $header";
				$extractSummary = "{$userModel->extractSummary}<br/>$updateMessage";
				$userModel->extractSummary = $extractSummary;
				$userModel->save();
				
				$emailBody	= "$header\n";
				$emailBody	.= "BackgroundProcess id: {$backgroundProcess->primaryKey}\n";
				$emailBody	.= "Start Time: {$backgroundProcess->startTime}\n";
				$emailBody	.= "End Time: {$backgroundProcess->endTime}\n";
				
				$email		= $userModel->email;
				//$email		= 'khalid.ajlouny@gmail.com';
				mail($email, $header, $emailBody);
			}
		}
		Yii::app()->end();
    }
    
    public function isRunning($pid)
    {
    	$output		= array();
		$isRunning	= false;
		
		exec( "ps -p $pid", $output );
		//print_r($output);
		
		if( isset($output[1]) )
		{
			$isRunning = in_array($pid, explode(' ', $output[1] ));
			//print_r(explode(' ', $output[1] ));
		}
		return $isRunning;
    }
    
}
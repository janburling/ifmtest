<?php
class PHConsoleApplication extends CConsoleApplication
{
	/*
	public function processRequest()
	{
		echo "child processRequest called\n";
		parent::processRequest();
	}
	*/

	protected function init()
	{
		parent::init();
		// re-persist passed user state 
		if( isset($_SERVER['argv'][2])) $this->loginUser($_SERVER['argv'][2]); // third argument before being passed to command run
	}
	
	protected function registerCoreComponents()
	{
		parent::registerCoreComponents();

		$components=array(
			'session'=>array(
				'class'=>'CHttpSession',
			),
			'user'=>array(
				'class'=>'CWebUser',
			),
		);

		$this->setComponents($components);

		//configure the correct database for PayrollProcessCommand -- other processes don't have this argument, though new processes need to be careful this is not a conflict -nc-
		if(isset($_SERVER['argv'][6])) {
			$dbConfig = new DatabaseConfig;
			$subdomain = $_SERVER['argv'][6] ? $_SERVER['argv'][6] : '';
			if($subdomain) $dbConfig->updateDatabase($subdomain);
		}
	}
	
	/**
	 * @return CHttpSession the session component
	 */
	public function getSession()
	{
		return $this->getComponent('session');
	}

	/**
	 * @return CWebUser the user session information
	 */
	public function getUser()
	{
		return $this->getComponent('user');
	}

	public function loginUser($userId)
	{
		//echo date('c') . " loginUser called for $userId for db" . Yii::app()->db->connectionString . "\n";
		//Yii::trace("loginUser called for $userId for db" . Yii::app()->db->connectionString . "\n");
		$userModel	= User::model()->findByPk($userId);
		
		if($userModel) {
			$identity	= new UserIdentity(strtolower($userModel->username),$userModel->password);
			$identity	->authenticate(true);
			Yii::app()->user->login($identity,0);
		}
		/*
		echo "userModel->password: {$userModel->password}\n";
		echo "identity->errorCode: {$identity->errorCode}\n";
		//Yii::app()->end();
		
		switch($identity->errorCode)
		{
			case UserIdentity::ERROR_NONE:
				//$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
				Yii::app()->user->login($identity,0);
				break;
			default: // UserIdentity::ERROR_PASSWORD_INVALID
				//$this->addError('password','Password is incorrect.');
				break;
		}
		*/
		
		//echo "Yii::app()->user: " . print_r( $_SESSION, 1);
	}
}
?>
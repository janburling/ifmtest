<?php

class CloneAction extends CAction {
	public function run()
	{	
		$model = $this->controller->loadModel();
		$model->checkActionAccess( $this->controller );
		
		if($model->modelName == 'Payroll') {
			ini_set('memory_limit', '512M');
			set_time_limit(120);
		}
			
		$clone = clone $model;
		$clone->isNewRecord = true;
		$clone->primaryKey = null;
		
		if(isset($_POST[$clone->modelName]))
		{
			$clone->attributes=$_POST[$clone->modelName];
			
			if($clone->save()) {
				$key['name']	= $clone->fKeyName;
				$key['value']	= $clone->primaryKey;
				$this->cascadeClone($model, $key);
				if($clone->save()) {
					if(isset($_REQUEST['saveAndClone'])) $returnUrl = array('clone', 'id'=>$clone->primaryKey);
					else $returnUrl = array('view', 'id'=>$clone->primaryKey);
					$this->controller->redirect($returnUrl);
				}
			}
		}
		
		$this->controller->render('/common/create',array(
			'model'=>$clone,
		));
	}

	public function cascadeClone($original, $key) {
		if(isset($original->cloneCascadeModels))
		{
			foreach($original->cloneCascadeModels as $cModel)
			{
				foreach($original->$cModel as $cOriginal)
				{
					$cClone					= clone $cOriginal;
					$cClone->isNewRecord	= true;
					$cClone->primaryKey		= null;
					$cClone->organizationId	= Yii::app()->user->organizationId;
					
					$cClone->$key['name']	= $key['value'];
					
					if($cClone->save())
					{					
						$cloneKey['name']	= $cClone->fKeyName;
						$cloneKey['value']	= $cClone->primaryKey;
						$this->cascadeClone($cOriginal, $cloneKey);						
					}
					else throw new CHttpException('Cascade clone failed on ' . $cModel . ' id ' . $cOriginal->primaryKey);														
				}
			}
		}
	}
}
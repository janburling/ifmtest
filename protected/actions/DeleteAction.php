<?php

class DeleteAction extends CAction {
	public function run()
	{	
		$model					= $this->controller->createModel();
		$model->scenario		= 'delete';
		$operation				= "{$model->modelName} Delete";
		$params					= array('model'=> $model );
		$hasDeletePendingAccess	= 0;
		$hasDeleteAccess		= Yii::app()->user->checkAccess($operation, $params);
		
		if( !$hasDeleteAccess )
		{		
			$deletePendingOperation	= "{$model->modelName} Delete Pending";
			if( Yii::app()->user->checkAccess($deletePendingOperation, $params) )
			{
				$hasDeletePendingAccess = 1; 
			}
		}
		
		if( !$hasDeleteAccess && !$hasDeletePendingAccess )
		{	
			if(isset($_REQUEST['ajax'])) {
				echo "<div class='alert alert-block alert-error fade in'><a class='close' data-dismiss='alert'>×</a>";
				echo "You do not have permission to delete this record.</div>";
				Yii::trace("Unauthorized delete");	
			}
			
			else {
				$this->controller->render('/common/accessViolation',
					array(
						'operation'	=> $operation,
						'returnURL'	=> Yii::app()->user->returnUrl,
					)
				);
			}
			
			CApplication::end();
		}

		ini_set('memory_limit', '512M');
		set_time_limit(60);
		
		$redirectTo	= Yii::app()->request->getUrlReferrer();
		
		if(isset($_REQUEST['ids'])) { // multiple ids passed
			$ids = $_REQUEST['ids'];
			$model		= $this->controller->createModel();
			
			// handle case of composite key
			if($this->controller->modelName == 'UserAuthItem') {
				foreach($ids as $key=>$compositeId) {
					$columnValues = explode('_', $ids[$key]);
					$ids[$key] = array('itemname'=>$columnValues[0], 'userid'=>$columnValues[1]);
				}
			}

			$models		= $model->findAllByPk($ids);
		}
		elseif(isset($_REQUEST['id'])) { // single ids passed
			$id = $_REQUEST['id'];
			$model		= $this->controller->createModel();
			$models		= $model->findAllByPk($id);
		}
		elseif( isset( $_REQUEST['criteria'] ) ) // criteria passed
		{
			$model		= $this->controller->createModel();
			$criteria	= $model->unserializeCriteria( $_REQUEST['criteria'] );
			$models		= $model->filtered()->findAll( $criteria );
			Yii::trace( 'Delete $criteria: ' . print_r($criteria, 1) );
		}
		elseif( Yii::app()->request->isPostRequest ) // single id passed
		{
			$models				= array( $this->controller->loadModel() );
			$redirectTo			= array('list');
		}			
		else
		{
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
		
		if( isset( $_REQUEST['redirectId'] ) )
		{
			$redirectTo	= array('show','id' => $_REQUEST['redirectId'] );
		}
		if($model->modelName == 'PayeesGroup') {
			$redirectTo = array('/payroll/view', 'id'=>$models[0]->payrollId);
		}
		
		// block below commented out because of processing overload
		/*
		foreach($models as $model) {
			$model	->checkActionAccess( $this->controller );
		}
		*/
		//$models[0]	->checkActionAccess( $this->controller );

		$transaction = $model->dbConnection->beginTransaction();				
		try
		{					
			foreach($models as $model)
			{
				if( !$hasDeleteAccess )
				{
					if( ! ($hasDeletePendingAccess && $model->isPending) )
					{
						$this->controller->render('/common/accessViolation',
							array(
								'operation'	=> $deletePendingOperation,
								'returnURL'	=> Yii::app()->user->returnUrl,					
							)
						);
						CApplication::end();
					}
				}
				
				if(($childName = $model->hasChildren()) 
					&& $model->modelName != 'Payroll'
					&& $model->modelName != 'TimeImportLine'
					&& $model->modelName != 'EmployeePayroll'
					&& $model->modelName != 'PayeesGroup'
				) {
					$operationMessage = $childName . ' records exist for this record, not deleted';
				}
				else {
					$this	->cascadeDelete($model);
					$this	->relatedDelete($model);
					$model	->delete();
					$operationMessage = $model->name . ' deleted.';
				}
			}
			$transaction->commit();
			// transaction completed
		}
		catch(Exception $e)
		{
			$transaction->rollBack();				
			$operationMessage = 'Error occurred, rolling back request.';
			// re-throw Exception to be caught as normal
			throw $e;
		}
		
		//$count				= count($models);
		//$labels				= $count == 1 ? $models[0]->modelLabel : "$count {$models[0]->modelsLabel}";   
		//$operationMessage	= "$labels deleted";
				
		if(isset($_REQUEST['ajax'])) {
			echo "<div class='alert alert-block alert-info fade in'><a class='close' data-dismiss='alert'>×</a>";
			echo "{$operationMessage}</div>";
			//Yii::trace($operationMessage);	
		}
		
		else {
			Yii::app()->user->setState('operationMessage', $operationMessage );
			$this->controller->redirect($redirectTo);
		}
	}
	
	public function relatedDelete($model) {
		//hack to remove WorkTimes on EmployeePayroll delete
		if($model->modelName == 'EmployeePayroll') WorkTime::model()->deleteAllByAttributes(array('payrollId'=>$model->payrollId, 'employeeId'=>$model->employeeId));
	}

	public function cascadeDelete($model) {
		if(isset($model->cascadeModels)) {
			foreach($model->cascadeModels as $cModelName) {
				foreach($model->$cModelName as $cModel) {
					$this->cascadeDelete($cModel);
					$cModel->delete();
					//Yii::trace( "Deleted {$cModel->modelName} {$cModel->primaryKey}" );
				}
			}
		}
	}
}
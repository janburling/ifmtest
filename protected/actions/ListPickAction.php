<?php

class ListPickAction extends CAction {
	public function run()
	{	
		$operation	= $this->controller->modelName . ' Pick';
		$params		= array('model'=>new $this->controller->modelName);
		if(Yii::app()->user->checkAccess($operation, $params))
		{
			$criteria			= new CDbCriteria;	
			$model				= $this->controller->createModel();			
			$pages				= new CPagination($model->count($criteria));
			$pages->pageSize	= Yii::app()->params['defaults']['listRowSize'];
			$pages->applyLimit($criteria);	
			$models				= $model->findAll($criteria);
	
			$this->controller->renderPartial('/common/listPick',array(
				'models'=>$models,
				'pages'=>$pages,
			));
		}
		else {
			//handle failed access
			$this->controller->render('/common/accessViolation',
				array(
					'operation'	=> $operation,
					'returnURL'	=> Yii::app()->user->returnUrl,					
				)
			);
			CApplication::end();
		}
		
		
	}
}

<?php

class DeleteAllAction extends CAction {
	public function run()
	{			
		
		$operation = $this->controller->modelName . ' Delete';
		$params = array('model'=>new $this->controller->modelName);
		if(Yii::app()->user->checkAccess($operation, $params))
		{
			$redirectTo	= Yii::app()->request->getUrlReferrer(); 
			echo '<h2>Passed criteria object:</h2>';
			$criteria = unserialize( base64_decode( $_REQUEST['criteria'] ) );
			echo '<pre>';
			print_r($criteria); 
			echo '</pre>';
			echo CHtml::link( '<h2>Back</h2>', $redirectTo ); 
			//$this->controller->redirect($redirectTo);
		}
		else {
			//handle failed access
			$this->controller->render('/common/accessViolation',
				array(
					'operation'	=> $operation,
					'returnURL'	=> Yii::app()->user->returnUrl,					
				)
			);
			exit;
		}		
			
	}

}
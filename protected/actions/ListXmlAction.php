<?php

class ListXmlAction extends CAction {
	public function run()
    {
		if( PHModel::getIsDevServer() ) // dev
		{
			ini_set('memory_limit', '300M');
		}
		else
		{
			ini_set('memory_limit', '1024M');
		}
    	
    	$operation	= $this->controller->modelName . ' List';
		$params		= array('model'=>new $this->controller->modelName);
		if( PHModel::getIsDevServer() || Yii::app()->user->checkAccess($operation, $params))
		{
	    	isset($_REQUEST['pId'])			? $pId			= $_REQUEST['pId']			: $pId = ''; //the id of the model requesting a related list
	    	isset($_REQUEST['cModelName'])	? $cModelName	= $_REQUEST['cModelName']	: $cModelName = ''; //the name of the model for which to create a list
	    	isset($_REQUEST['modelName'])	? $modelName	= $_REQUEST['modelName']	: $modelName = ''; //the name of the model for which to create a list
	    	
	    	isset($_REQUEST['asOfDate'])		? $asOfDate			= $_REQUEST['asOfDate']			: $asOfDate			= ''; //the name of the model for which to create a list
	    	isset($_REQUEST['showNavigation'])	? $showNavigation	= $_REQUEST['showNavigation']	: $showNavigation	= true; //determines whether to show drill in nav
	    	isset($_REQUEST['fromListPick'])	? $fromListPick		= $_REQUEST['fromListPick']		: $fromListPick		= false; //determines whether to the call was from list pick
	    	
			// handle case where parameters are passed by both GET and POST
			//if( isset($_POST['_search']) )	$_REQUEST['_search']	= $_POST['_search'];						
	    	
	    	$modelForSort = $this->controller->createModel(); // created this to use $modelForSort->defaultSort, rather than using the controller alias
	    	isset($_REQUEST['ids'])		? $ids 			= $_REQUEST['ids']			: $ids			= array();
			isset($_REQUEST['page']) 	? $listPage 	= $_REQUEST['page'] 		: $listPage		= 0; //page number to display
			isset($_REQUEST['rows']) 	? $listRows 	= $_REQUEST['rows'] 		: $listRows		= Yii::app()->params['defaults']['listRowSize']; //number of rows to display
			isset($_REQUEST['sidx']) 	? $sortName 	= $_REQUEST['sidx'] 		: $sortName		= $modelForSort->defaultSort['name']; //sort column 
			isset($_REQUEST['sord']) 	? $sortOrder 	= $_REQUEST['sord'] 		: $sortOrder	= $modelForSort->defaultSort['order']; //sort order
			isset($_REQUEST['_search']) ? $isSearch 	= $_REQUEST['_search']		: $isSearch		= 'false';
	
	
			/*Yii::trace(print_r($_REQUEST, true));
			Yii::trace('Parameters: ' . 
				' pId=' 		. $pId 		. ' cModelName='	. $cModelName	. ' ids=' . $ids .
				' listPage=' 	. $listPage	. ' listRows='		. $listRows		.
				' sortname=' 	. $sortName	. ' sortOrder='		. $sortOrder	. ' isSearch =' . $isSearch);
			*/	
	
			$criteria = new CDbCriteria; //used to define sql criteria
			
			if($cModelName) { //treat as a related list
				$model = $this->controller->getChildModel($cModelName);
				//set condition to only retrieve records related to parent
				
				if($model->hasAttribute($this->controller->fKeyName)) $fKey = $this->controller->fKeyName;
				else $fKey = $this->controller->getCustomFKeyName($cModelName);
				if($fKey) $criteria->addCondition('"' . $model->tableName() . '"."' . $fKey . '" = ' . $pId);
			}
			elseif($modelName) { //specific model passed
				$model = $this->controller->getChildModel($modelName);		
			}
	    	else { //treat as a regular list if one of these params is missing
				$model = $this->controller->createModel();
	    	}

	    	if( isset( $_REQUEST['filterCriteria'] ) ) // additional criteria passed as POST parameter
			{
				if( $_REQUEST['filterCriteria'] != '' )
				{
					$criteria->mergeWith( $model->unserializeCriteria( $_REQUEST['filterCriteria'] ) );
				}
			}
	    	
			$startRecord = $listRows * $listPage - $listRows; //first record to display
			if($startRecord < 0) $startRecord = 0; //make sure it's positive
			
			$listType		= Yii::app()->user->getState( 'listType' );
			$listViewConfig	= Yii::app()->user->getState( "{$model->modelName}_{$listType}_listConfig" );
			$listAttributes	= $listViewConfig['elements'];
			
			if($isSearch == 'true')
			{
				$bindParams = array();
				foreach($_REQUEST as $searchKey=>$searchValue) { //loop through all params looking for search keys
					// handle case where parameters are passed by both GET and POST
					if( isset($_POST[$searchKey]) )	$searchValue = $_POST[$searchKey];				
					
					//checks the model to see if this model requires a complex search condition
					$customCriteria = $model->getCustomCriteria($searchKey, $searchValue);
					if($customCriteria) {						
						$criteria->mergeWith($customCriteria);
					}
					
					else { //if there's no custom criteria, do a basic search on any matching DB field
						foreach( $listAttributes as $columnName=>$valueArray )
						{
							if($searchKey == $columnName)
							{
								$parameterName	= $model->tableName() . '_' . $searchKey . '_SearchValue';
								if($model->getFieldType($searchKey) == 'date')
								{
									$criteria->addColumnCondition(array('"'.$searchKey.'"'=>Yii::app()->dateFormatter->format('yyyy-MM-dd', $searchValue)));
								}
								elseif(
									$model->getFieldType($searchKey) == 'number' ||
									$model->getFieldType($searchKey) == 'currency' ||
									$model->getFieldType($searchKey) == 'decimal') {
									
									//$criteria->addCondition('"' . $model->tableName() . '"."' . $searchKey . '" = ' . $searchValue);
									$criteria->addCondition('"' . $model->tableName() . '"."' . $searchKey . '" = :' . $parameterName);
									$bindParams[ ":$parameterName" ] = $searchValue;
								}
								elseif($model->getFieldType($searchKey) == 'dropdownlist')
								{
									$criteria->addCondition('"' . $model->tableName() . '"."' . $searchKey . '" = :' . $parameterName);
									$bindParams[ ":$parameterName" ] = $searchValue;
								}
								else
								{
									$criteria->addCondition('LOWER("' . $model->tableName() . '"."' . $searchKey . '")' . "LIKE :$parameterName"); //add criteria matching the search string for the column
									//$criteria->addCondition('LOWER("' . $model->tableName() . '"."' . $searchKey . '")' . "LIKE '" . strtolower($searchValue) . "%'"); //add criteria matching the search string for the column
									$bindParams[ ":$parameterName" ] = strtolower($searchValue) . '%';
								}
								#$searchKey . ' = ' . $searchValue; // TODO - determine if some fields should be exact matches
							}
						}
						//Yii::trace('from list xml action, bindParams: ' . print_r( $bindParams, 1) );
						if( !empty( $bindParams ) )
						{
							$criteria->params = array_merge( $criteria->params, $bindParams );
						}
					}
				}
				//Yii::trace( 'search $_REQUEST: ' . print_r($_REQUEST, true));
				//Yii::trace( 'search $criteria: ' . print_r($criteria, true));
			}
	
			
			$idsPassed = !empty($ids); 
			if($idsPassed) //checks to see if user passed a selected list of items
			{
				for($i=0; $i<sizeOf($ids); $i++)
				{
					$ids[$i] = (int)$ids[$i];
				}
				$criteria->addInCondition('"' . $model->modelName . '"."id"', $ids);
			//Yii::('search: ' . print_r($ids, true));
			}
			
			/*
			$listType			= isset( $_REQUEST['listType'] )? $_REQUEST['listType'] : 'default';
			$listViewConfig		= $model->getListViewConfig($listType);
			$listAttributes		= $listViewConfig['elements'];
			*/
			
			if( isset( $listViewConfig['criteria'] ) )	$criteria->mergeWith( $listViewConfig['criteria'] );
	
			$recordCount		= $model->filtered()->count($criteria); //get a record count for paging purposes
			
			//set up ordering -- use special criteria to look up related names for ordering
			$customCriteria		= $model->getCustomCriteria($sortName, $sortOrder, 'order');
			if($customCriteria) $criteria->mergeWith($customCriteria);
			else $criteria->order = '"' . $model->tableName() . '"."' . $sortName . '" ' . $sortOrder; //set the order by statement
			
			$criteria->offset	= $startRecord; //set the direction for the order by statement
			$criteria->limit	= $listRows; //set how many rows to return (page length)
					
			//Yii::trace( "from list xml, criteria: " . print_r($criteria,1));
			$models=$model->findAll($criteria); //get all the records based on search criteria
			
			// resort in case selected ids were passed
			if( $idsPassed && $cModelName )
			{
				$sortedModels = array();
				foreach( $models as $index=>$model )
				{
					$positionKey = array_keys($ids, $model->primaryKey );
					$sortedModels[ $positionKey[0] ] = $model;
					//Yii::trace("old position: $index, new position: {$positionKey[0]}, model: {$model->primaryKey}");
				}
				ksort($sortedModels);
				$models = $sortedModels;
			}
			
			$xml				= new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<rows></rows>"); //initial xml output definition for jqgrid
			
			if($recordCount > 0 && $listRows > 0) $totalPages = ceil($recordCount / $listRows); //figure out how many pages are in the list view
			else $totalPages = 0; //make sure it's not negative or undefined
			if($listPage > $totalPages) $listPage = $totalPages; //don't let the page be in an undefined range
			
			$xml->addChild('page'		, $listPage); //add to the xml to be passed
			$xml->addChild('total'		, $totalPages); //add to the xml to be passed
			$xml->addChild('records'	, $recordCount); //add to the xml to be passed
			
			$totals = array();
			
			// delete all records (not only the ones visible on page)
			$wasFormDisplayed	= false;			
			$serializedCriteria	= $model->serializeCriteria($criteria);
						
			$confirmFormPrefix	= 'confirmPending';
			$confirmAction		= 'confirmPending';
			$confirmForm		=	CHtml::beginForm( array( $model->modelName . '/' . $confirmAction ), 'post', array('id'=>$confirmFormPrefix . $model->modelsName ) ) .
									CHtml::hiddenField( 'criteria'		, $serializedCriteria,	array('id' => "$confirmFormPrefix{$model->modelsName}Criteria") ) .
									CHtml::hiddenField( 'recordCount'	, $recordCount,			array('id' => "$confirmFormPrefix{$model->modelsName}Count") ) .
									CHtml::endForm();
									
			$deleteFormPrefix	= 'deleteAll';
			$deleteAction		= $model->standardDeleteAction;
			$deleteForm			=	CHtml::beginForm( array( $model->modelName . '/' . $deleteAction ), 'post', array('id'=>$deleteFormPrefix . $model->modelsName ) ) .
									CHtml::hiddenField( 'criteria'		, $serializedCriteria,	array('id' => "$deleteFormPrefix{$model->modelsName}Criteria") ) .
									CHtml::hiddenField( 'recordCount'	, $recordCount,			array('id' => "$deleteFormPrefix{$model->modelsName}Count") ) .
									CHtml::endForm();

			$hardDeleteFormPrefix	= 'hardDeleteAll';
			$hardDeleteAction		= 'delete';
			$hardDeleteForm			=	CHtml::beginForm( array( $model->modelName . '/' . $hardDeleteAction ), 'post', array('id'=>$hardDeleteFormPrefix . $model->modelsName ) ) .
									CHtml::hiddenField( 'criteria'		, $serializedCriteria,	array('id' => "$hardDeleteFormPrefix{$model->modelsName}Criteria") ) .
									CHtml::hiddenField( 'recordCount'	, $recordCount,			array('id' => "$hardDeleteFormPrefix{$model->modelsName}Count") ) .
									CHtml::endForm();

			foreach($models as $model)
			{
				/*
				// custom list view
				if( isset( $_REQUEST['listType'] ) )
				{
					$model->listType = $_REQUEST['listType'];
				}
				
			    if( $fromListPick ){
			    	$model->listType = 'pick';
			    }
			    */
			    
			    // set $listViewConfig to skip not displayed fields
			    $model->listViewConfig	= $listViewConfig;
			    $model->isListRow		= true;
				
			    if($asOfDate) { $model->asOfDate = $asOfDate; }
	
				$row		= $xml->addChild('row'); //start a new row
				$modelId	= is_array($model->primaryKey) ? implode('_', $model->primaryKey) : $model->primaryKey;
			    $row->addAttribute('id', $modelId); //define the row id
	
			    if($showNavigation && !$fromListPick ) {
				    $cellContent = CHtml::link(	'<img src="/images/eye-icon.gif" alt="Show" title="Show" border="0"/>',
				    							array( "{$model->modelName}/show",$model->primaryKeyName=>$model->primaryKey)); 
				    if( $model->modelName == 'Bill')
				    {
				    	$cellContent .= CHtml::link(	'<img src="/images/dollar-sign.gif" alt="Pay" title="Pay" border="0"/>',
				    									array( '/Payment/create', 'BillSelectedRows' => $model->primaryKey, 'asOfDate'=>$model->asOfDate) );
				    }
				    $row->addChild('cell', $cellContent ); // show column
			    }
		    
			    foreach($listAttributes as $columnKey=>$valueArray) { //loop through all the columns to display
			    	
			    	$fieldXml	= $model->$columnKey;
			    	$fieldType	= $model->getFieldType($columnKey);
			    	
			    	switch($fieldType)
			    	{
			    		case 'currency':
			    			$fieldXml = Yii::app()->numberFormatter->formatCurrency($fieldXml, 'USD');
			    			break;
			    		
			    		case 'dropdownlist':
			    			$fieldXml = $model->getForeignValue($columnKey);
			    			break;
			    			
			    		case 'checkbox':
			    			$fieldXml = $fieldXml ? 'Yes' : 'No';
			    			break;
			    		
			    		case 'application.extensions.jui.EDatePicker':
			    			$fieldXml ? $fieldXml = Yii::app()->dateFormatter->format('MM/dd/yyyy', $fieldXml) : $fieldXml = '';
			    			break;
			    		
			    		default:
			    			$fieldXml = CHtml::encode($fieldXml);
			    	}
			    	
					/*
			    	// was breaking list views, likely because these methods no longer exist in payroll
 			    	if($fieldType == 'currency') //format currency
			    		$fieldXml = Yii::app()->numberFormatter->formatCurrency($fieldXml, 'USD');
			    	
			    	elseif($fieldType == 'application.extensions.jui.EDatePicker') //format dates
			    		$fieldXml ? $fieldXml = Yii::app()->dateFormatter->format('MM/dd/yyyy', $fieldXml) : $fieldXml = '';
			    	
			    	elseif($fieldType == 'checkbox') //format booleans
			    		$fieldXml = $fieldXml ? 'Yes' : 'No';
			    	
			    	elseif($fieldType == 'list') { //format lists
			    		if(isset($model->viewAttributes[$columnKey]['options'][$fieldXml]))
			    			$fieldXml = $model->viewAttributes[$columnKey]['options'][$fieldXml];
			    		elseif(!$fieldXml && isset($model->viewAttributes[$columnKey]['options'][0]))
			    			$fieldXml = $model->viewAttributes[$columnKey]['options'][0];
			    	}
			    	
			    	else //format text and other
						$fieldXml = CHtml::encode($fieldXml);
						
					*/
			    	
			    	//Yii::trace("listXML, delete form: $deleteForm$confirmForm$hardDeleteForm");
			    	//Yii::trace("wasFormDisplayed: $wasFormDisplayed, fromListPick: $fromListPick");
					// temp case to display delete all form
			    	if( !$wasFormDisplayed && !$fromListPick && !isset( $_REQUEST['hideNavigation'] ) )
			    	{
			    		//Yii::trace("listXML, delete form: $deleteForm$confirmForm$hardDeleteForm");
			    		$fieldXml .= "$deleteForm$confirmForm$hardDeleteForm";
			    		$wasFormDisplayed = 1;
			    	}
			    	//Yii::trace("ListXmlAction, fieldXml: $fieldXml");
					$row->addChild('cell', $fieldXml); //add the column cell
			    	
	
			    	if(isset($valueArray['footer'])) {
			    		if($valueArray['footer'] == 'total') {
			    			if(isset($totals[$columnKey])) $totals[$columnKey] += $model->$columnKey;
			    			else $totals[$columnKey] = $model->$columnKey;
			    		}
			    	}
			    }
			    
			    foreach($totals as $fieldName=>$totalAmount) {
	    	    	if($model->getFieldType($fieldName) == 'currency') //format currency
		    			$totalAmount = Yii::app()->numberFormatter->formatCurrency($totalAmount, 'USD');
			    	$userdata = $xml->addChild('userdata', $totalAmount);
			    	$userdata->addAttribute('name', $fieldName);	
			    }
			    
			}		
			
			$this->controller->renderPartial( '/common/listXml', array('xml'=>$xml,) ); //pass the xml to the lixtXML view

		}
		else {
			//handle failed access
			$this->controller->render('/common/accessViolation',
				array(
					'operation'	=> $operation,
					'returnURL'	=> Yii::app()->user->returnUrl,					
				)
			);
			CApplication::end();
		}
	}
}

<?php

class ShowAction extends CAction {
	public function run()
	{
		$model = $this->controller->loadModel();
		$model	->checkActionAccess( $this->controller );
		
		$this->controller->render('/common/show',array( 'model'=>$model ));
	}
}
<?php
class UpdateAction extends CAction {
	public function run()
	{
		$model	= $this->controller->loadModel();
		$model	->checkActionAccess( $this->controller );
		
		$loadData	= $this->controller->updateDependentFields($model);
		$form		= $model->form;
		
		if( $form->submitted('save', $loadData) && $form->validate() )
		{			
			if($model->save())
			{
			    $passParameters[$model->primaryKeyName]	= $model->primaryKey;
			    $route			= 'show';									
				$url			= $this->controller->createUrl( $route, $passParameters );	      
				$this->controller->redirect($url);
			}
		}
		$this->controller->render('/common/update', array( 'model' => $model ) );
	}
}
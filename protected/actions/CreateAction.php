<?php

class CreateAction extends CAction {
	public function run()
	{	
		$model	= $this->controller->createModel();
		$model	->checkActionAccess( $this->controller );
		
		//Yii::trace( 'from create action: ' . print_r($_REQUEST, 1 ));
		$key 		= isset($_REQUEST['key'])? $_REQUEST['key'] : array(); 
		$loadData	= $this->controller->updateDependentFields($model, $key);
		$success	= false;
		
		if( $loadData && $key )
		{
			$parentModel	= CActiveRecord::model($key['name'])->findByPk($key['value']);
			$matchFields	= array_diff( array_keys($parentModel->attributes), explode( ' ',  'id status organizationId createdDate modifiedDate modifiedUser') );
			//Yii::trace('$matchFields: ' . print_r($matchFields,1)); 
			foreach( $matchFields as $field )
			{
				if( $model->hasAttribute($field) && $model->$field == '' )
				{
					$model->$field = $parentModel->$field;
					//Yii::trace("matching fields, {$field}: {$model->$field}");
				}
			}
			$model->updateDependentFields($key);
		}
		
		$form	= $model->form;
		
 		if( $form->submitted('save', $loadData) && $form->validate() )
		{
			$transaction	= $model->dbConnection->beginTransaction();
			try{
				if($model->save())
				{
					//$model->customPostCreate();
					
					// if created from a realted list, go back to parent show view
					if($key)
					{
						$passParameters[ PHModel::model($key['name'])->primaryKeyName ]	= $key['value'];
						$passParameters['#']	= "tab{$model->modelsName}"; // anchor pointing to related tab
						$route	= "{$key['name']}/view";
					}
					else
					{
						$passParameters[$model->primaryKeyName]	= $model->primaryKey;
						$route	= "view";
					}	      
					$url					= $this->controller->createUrl( $route , $passParameters );
					$transaction->commit();
					$this->controller->redirect($url);
					
					/*
					if($model->modelName == 'Organization')
					{
						$newOrganizationId = $model->id;
						//system update to the new org
						$query = 'Update "Organization" set "organizationId" = ' . $newOrganizationId . 'where "id" = ' . $newOrganizationId; 
						$command	= Yii::app()->db->createCommand($query);
						$rowCount	= $command->execute();
						//system update to the current user
						$query = 'Update "User" set "organizationId" = ' . $newOrganizationId . 'where "id" = ' . Yii::app()->user->id; 
						$command	= Yii::app()->db->createCommand($query);
						$rowCount	= $command->execute();
						Yii::app()->user->setState('organizationId', $newOrganizationId);
						//create a new group for the new organization
						$group = new Group;
						$group->name = $model->name . ' Admin';
						if($group->save()) {
							//put the user in the new group
							$user = User::model()->findByPk(Yii::app()->user->id);
							$user->groupId = $group->id;
							if($user->save()) {}
						}							
					}
					*/
				}
			}
			catch(Exception $e){
			    $transaction->rollBack();
			    // re-throw Exception to be caught as normal
			    throw $e;
			    // further handling comes here
			}
		}
		$this->controller->render('/common/update', array( 'model' => $model ) );
	}
}
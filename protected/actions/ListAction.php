<?php

class ListAction extends CAction {
	public function run()
	{	
		$model	= $this->controller->createModel();
		$model	->checkActionAccess( $this->controller );

		//TEST
		$operation	= "{$model->modelName} " . $model->generateAttributeLabel( $this->id );
		//Yii::trace($operation);
		$params		= array('model'=>$model);
		if( !Yii::app()->user->checkAccess($operation))
		{
			$this->controller->render('/common/accessViolation',
				array(
					'operation'	=> $operation,
					'returnURL'	=> Yii::app()->user->returnUrl,					
				)
			);
			CApplication::end();
		}		
		if(isset($_REQUEST['selectedRows'])) {
			$selectedRows = $_REQUEST['selectedRows'];		
		}
		elseif(isset($_REQUEST[$this->controller->modelName . 'SelectedRows'])) { //allows a uniquely named related list to send in selected rows
			$selectedRows = $_REQUEST[$this->controller->modelName . 'SelectedRows'];
		}
		else {
			$selectedRows = '';
		}

		isset($_REQUEST['redirectAction'])	? $redirectAction	= $_REQUEST['redirectAction']	: $redirectAction = '';
		isset($_REQUEST['asOfDate'])		? $asOfDate			= $_REQUEST['asOfDate']			: $asOfDate = '';
		isset($_REQUEST['ids'])				? $ids 				= $_REQUEST['ids']				: $ids = array();
		
		//if(isset($redirectAction)) {
		
		if($selectedRows) $ids = explode(',', $selectedRows);
		if( count($ids) > 1 )
		{
			$ids = array_values( array_unique($ids) ); // restart the index
		}
		//Yii::trace( print_r( $ids, 1) );
		
		// pass through prameters
		$passParameters					= array();
		$autoPrintURL					= '';
		
		if( isset( $_REQUEST['DOC_LIST'] ) ){
			$autoPrintURL .= 'DOC_LIST=' . $_REQUEST['DOC_LIST'];
			if( isset( $_REQUEST['COPIES'] ) )
			{
				$autoPrintURL .= '&COPIES=' . $_REQUEST['COPIES'];
			}
		}
		
		if($ids) {	
			if($redirectAction == 'update')
			{
				if(sizeof($ids) == 1)
				{
					$passParameters[$model->primaryKeyName]	= $ids[0];
					$route					= $redirectAction;
				}
				else
				{
					$passParameters['ids']	= $ids;
					$route					= 'batchUpdate';
					
				}
				$url = $this->controller->createUrl( $route , $passParameters );
				$this->controller->redirect( $url );	
			}
			
			elseif($redirectAction == 'show') {
				if(sizeof($ids) == 1)
				{
					$route = $redirectAction;
					if( !empty( $autoPrintURL ) )
					{
						$url	= $this->controller->createUrl( $route , $passParameters );
						$url	.= "?{$model->primaryKeyName}={$ids[0]}&" . $autoPrintURL;
					}
					else
					{
						$passParameters[$model->primaryKeyName]	= $ids[0];;
						$url					= $this->controller->createUrl( $route , $passParameters );
					}
					
					$this->controller->redirect( $url );
					//$this->controller->redirect(array($redirectAction, 'id'=>$ids[0]));
				}	
			}
			
			elseif($redirectAction == 'delete' || $redirectAction == $this->controller->createModel()->standardDeleteAction )
			{
				$passParameters['ids']	= $ids;
				$route					= $redirectAction;
				$url					= $this->controller->createUrl( $route , $passParameters );
				$this->controller->redirect( $url );
				//$this->controller->redirect(array($redirectAction, 'ids'=>$ids));
			}
			
		}

		$passParameters['ids']			= $ids;
		$passParameters['listModel']	= $model;
		$this->controller->render('/common/list', $passParameters );
	}
}
<?php

// uncomment the following to define a path alias
 Yii::setPathOfAlias('local', 'localhost/ifmtest');  //'path/to/local-folder');
 Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Systems East IFM - Integrated Financial Management',
    'onBeginRequest'=>array('DatabaseConfig', 'setUserDatabase'),
   	'catchAllRequest'=>file_exists(dirname(__FILE__).'/.maintenance')
        ? array('site/maintenance') : null,
	// preloading 'log' component
	 'preload'=>array('log'),

	// autoloading model and component classes
	'import'=> array(
		'application.models.*',
        'application.models.forms.*',
		'application.controllers.*',
        'application.components.*',
        'application.modules.srbac.controllers.SBaseController',
		'ext.bootstrap.widgets.*',
		'ext.datetimepicker.*',
	),

/*	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>false,
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),
 */

	// application components
	'components'=> array(
	    'urlManager'	=> array(
            'urlFormat'			=> 'path',
			'showScriptName'	=> false,
			/*'rules'=>array(
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
    */
			),
			    'errorHandler'	=> array(
            'errorAction'		=> 'site/error',
        ),



        'log'=> array(
        	'class'		=> 'CLogRouter',
			'routes'	=> array(
				array(
					'class'			=> 'CFileLogRoute',
					'levels'		=> 'error, warning',
				),
				array(
					'class'			=> 'CFileLogRoute',
					'levels'		=> 'error, warning, trace',
					//'categories'	=> 'application.*, system.db.ar.*',
					'categories'	=> 'application.*',
				),
				/*array(
					'class'			=> 'CWebLogRoute',
					'levels'		=> 'error, warning, trace',
					'categories'	=> 'application.*',
				),*/
				array(
                    'class'			=> 'PHEmailLogRoute',
                    'levels'		=> 'error',
                    'emails'		=> 'errors@remarc.com',
					'subject' 		=> "IFM Error - {$_SERVER['SERVER_NAME']}",
					'enabled' 		=> true,
					'sentFrom'		=> 'errors@remarc.com',
					//'filter'		=> 'CLogFilter',
                ),
				//array(
				//	'class'	=> 'CProfileLogRoute',
				//	'levels'=> 'profile',
				//),
			),
		),
		'user'=> array(
			// enable cookie-based authentication
			'allowAutoLogin'=> true,
		),
		'db'=>array(
	//		'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
			'connectionString'=>'pgsql:host=localhost;port=5433;dbname=ifm_ogs',
                      'username'=>'postgres',
                       'password'=>'c@rm0nd',
		),



		// uncomment the following to use a MySQL database
		/*
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=testdrive',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		*/
				'cache'=>array(
			'class' => 'CFileCache',
		),
//			   	'authManager'=> array(
			// The type of Manager (Database)
//			'class'				=> 'CDbAuthManager',
			// The database component used
//			'connectionID'		=> 'db',
			// The itemTable name (default:authitem)
//			'itemTable'			=> 'items',
			// The assignmentTable name (default:authassignment)
//			'assignmentTable'	=> 'assignments',
			// The itemChildTable name (default:authitemchild)
//			'itemChildTable'	=> 'itemchildren',
//		),
		'bootstrap'=>array(
			'class'=>'ext.bootstrap.components.Bootstrap',
			'coreCss'=>true,
			'responsiveCss'=>true,
			'plugins'=>array(),
		),
		'format'=>array(
			'class'=>'application.extensions.RemarcFormatter'
		),
	),
   	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=> array(
		// this is used in contact page
		'phpPath'	=> 'c:/wamp/bin/php',
		'webRoot'	=> 'c:/wamp/www/ifmtest',
		'reportingDataSource'	 => 'postgres-payroll',
		'adminEmail'	=> 'jan@systemseast.com',
		'defaults'		=> array(
			'listRowSize'	=> 20,
			'datePicker'	=> array(
				'mode'			=> 'imagebutton',
				'theme'			=> 'redmond',
	       		'dateFormat'	=> 'mm/dd/yy',
			   	'fontSize'		=> '10px',
				'htmlOptions'	=> array('size'=>10),
			),
		),
		'organizations' => array(
			'ogdensburg'=>'City of Ogdensburg',
			//'wash'=>'Washington',
			'staging'=>'Staging',
			'test'=>'Test',
		),
/*		'errorLogging' => array(
			'enabled' 	=> true,
			'subject'	=> "IFM Error - //{$_SERVER['SERVER_NAME']}",
			'to'		=> 'jan@systemseast.com',
			'sentFrom'	=> 'jan@systemseast.com',
		),
/*		// payroll defaults
		'baseGrossAdditions' => array(
			'shift1'			=> 4,
			'shift2'			=> 5,
			'shift3'			=> 6,
			'shift1OT'			=> 7,
			'shift2OT'			=> 8,
			'shift3OT'			=> 9,
			'additionalWages'	=> 67,
		),
		'totalAndNetGrossAdditions' =>	array(
			'Gross Wages'		=> 11,
			'Soc Sec Wages'		=> 12,
			'Medicare Wages'	=> 13,
			'Federal Taxable'	=> 14,
			'State Taxable'		=> 15,
			'ERS Wages'			=> 16,
*/			'Net Wages'			=> 17,

		'year'	=> date('Y'),
  	),

	'modules'=> array(

        'srbac' => array(
			'userclass'				=> 'User', //optional defaults to User
			'userid'				=> 'id', //optional defaults to userid
			'username'				=> 'username', //optional defaults to username
			'debug'					=> false, //optional defaults to false
			'pagesize'				=> 20, //optional defaults to 15
			'superUser'				=> 'Admin', //optional defaults to Authorizer
			'css'					=> 'srbac.css', //optional defaults to srbac.css
			'layout'				=>
			'application.views.layouts.main', //optional defaults to empty string
			// must be an existing alias
			'notAuthorizedView'		=>
			'srbac.views.authitem.unauthorized', // optional defaults to
			//srbac.views.authitem.unauthorized, must be an existing alias
			'alwaysAllowed'			=> array( //optional defaults to gui
			'SiteLogin','SiteLogout','SiteIndex','SiteAdmin',
			'SiteError', 'SiteContact'),
			'userActions'			=> array(//optional defaults to empty array
			'Show','View','List'),
   			'imagesPack'			=> 'tango',
   			'iconText'				=> false,
			'listBoxNumberOfLines'	=> 20, //optional defaults to 10 'imagesPath' => 'srbac.images', //optional defaults to srbac.images 'imagesPack'=> 'noia', //optional defaults to noia 'iconText'=>true, //optional defaults to false 'header'=> 'srbac.views.authitem.header', //optional defaults to
			// srbac.views.authitem.header, must be an existing alias 'footer'=> 'srbac.views.authItem.footer', //optional defaults to
			// srbac.views.authitem.footer, must be an existing alias 'showHeader'=>true, //optional defaults to false 'showFooter'=>true, //optional defaults to false
			'alwaysAllowedPath'		=> 'srbac.components', //optional defaults to srbac.components
			// must be an existing alias
		),
      		'gii'=> array(
            'class'		=> 'system.gii.GiiModule',
            'password'	=> false,
   			'ipFilters'	=> false,
            //'ipFilters'=>array(...a list of IPs...),
            // 'newFileMode'=>0666,
            // 'newDirMode'=>0777,

        ),
	)

);

<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'	=> dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'		=> 'Payroll',
	'onBeginRequest'=>array('DatabaseConfig', 'setUserDatabase'),

	'catchAllRequest'=>file_exists(dirname(__FILE__).'/.maintenance')
        ? array('site/maintenance') : null,

    // preloading 'log' component
	'preload'=> array('log'),

	// autoloading model and component classes
	'import'=> array(
		'application.models.*',
        'application.models.forms.*',
		'application.controllers.*',
        'application.components.*',
        'application.modules.srbac.controllers.SBaseController',
	),

	// application components
	'components'=> array(
	    'urlManager'	=> array(
            'urlFormat'			=> 'path',
			'showScriptName'	=> false,
			/*
            'rules'=>array(
		            'gii'=> 'gii',
		            'gii/<controller:\w+>'=> 'gii/<controller>',
		            'gii/<controller:\w+>/<action:\w+>'=> 'gii/<controller>/<action>',
		            // existing rules...
		        ),
		        */
		  ),
        
        'errorHandler'	=> array(
            'errorAction'		=> 'site/error',
        ),
        
        'log'=> array(
        	'class'		=> 'CLogRouter',
			'routes'	=> array(
				array(
					'class'			=> 'CFileLogRoute',
					'levels'		=> 'error, warning',
				),
				array(
					'class'			=> 'CFileLogRoute',
					'levels'		=> 'error, warning, trace',
					//'categories'	=> 'application.*, system.db.ar.*',
					'categories'	=> 'application.*',
				),
				array(
                    'class'			=> 'PHEmailLogRoute',
                    'levels'		=> 'error',
                    'emails'		=> 'kajlouny@horizonlinetech.com', //, ncramer@horizonlinetech.com',
					'subject' 		=> "Payroll Error - {$_SERVER['SERVER_NAME']}",
					'enabled' 		=> true || ( $_SERVER['SERVER_NAME'] != 'payroll.palmerhill.com' ),
					'sentFrom'		=> 'tcs@palmerhill.com',
					//'filter'		=> 'CLogFilter',
                ),
				array(
					'class'	=> 'CProfileLogRoute',
					'levels'=> 'profile',				
				),
			),
		),
		'user'=> array(
			// enable cookie-based authentication
			'allowAutoLogin'=> true,
		),
		/*'db'=> array(
			'class'=> 'CDbConnection',
			'connectionString'	=> 'pgsql:host=localhost;port=5432;dbname=payroll',
			'username'			=> 'payroll',
			'password'			=> 'seuserpass2',
			'schemaCachingDuration'=>0,
			'enableProfiling'	=> 0 && ($_SERVER['SERVER_NAME'] == 'payroll.palmerhill.com'),
		),*/
		
		'cache'=>array(
			'class' => 'CFileCache',
		),
				
	   	'authManager'=> array(
			// The type of Manager (Database)
			'class'				=> 'CDbAuthManager',
			// The database component used
			'connectionID'		=> 'db',
			// The itemTable name (default:authitem)
			'itemTable'			=> 'items',
			// The assignmentTable name (default:authassignment)
			'assignmentTable'	=> 'assignments',
			// The itemChildTable name (default:authitemchild)
			'itemChildTable'	=> 'itemchildren',
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=> array(
		// this is used in contact page
		'phpPath'	=> '/usr/bin/php',
		'webRoot'	=> '/var/www/html',
		'reportingDataSource'	 => 'postgres-payroll',
		'adminEmail'	=> 'kajlouny@horizonlinetech.com',
		'defaults'		=> array(
			'listRowSize'	=> 20,
			'datePicker'	=> array(
				'mode'			=> 'imagebutton',
				'theme'			=> 'redmond',
	       		'dateFormat'	=> 'mm/dd/yy',
			   	'fontSize'		=> '10px',                
				'htmlOptions'	=> array('size'=>10),
			),
		),
		'organizations' => array(
			'wash'=>'Washington',
			'staging'=>'Staging',
			'test'=>'Test',
			'dtest'=>'D-Test',
		),
		'errorLogging' => array(
			'enabled' 	=> true || ( $_SERVER['SERVER_NAME'] != 'payroll.palmerhill.com' ),
			'subject'	=> "Payroll Error - {$_SERVER['SERVER_NAME']}",
			'to'		=> 'kajlouny@horizonlinetech.com', // , ncramer@horizonlinetech.com',
			'sentFrom'	=> 'tcs@palmerhill.com',
		),
		// payroll defaults
		'baseGrossAdditions' => array(
			'shift1'			=> 4,
			'shift2'			=> 5,
			'shift3'			=> 6,
			'shift1OT'			=> 7,
			'shift2OT'			=> 8,
			'shift3OT'			=> 9,
			'additionalWages'	=> 67,
		),
		'totalAndNetGrossAdditions' =>	array(
			'Gross Wages'		=> 11,
			'Soc Sec Wages'		=> 12,
			'Medicare Wages'	=> 13,
			'Federal Taxable'	=> 14,
			'State Taxable'		=> 15,
			'ERS Wages'			=> 16,
			'Net Wages'			=> 17,		
		),
		'year'	=> date('Y'),
   	),

	'modules'=> array(
   	
		'gii'=> array(
            'class'		=> 'system.gii.GiiModule',
            'password'	=> 'PHuserpass1',
   			'ipFilters'	=> false,
            //'ipFilters'=>array(...a list of IPs...),
            // 'newFileMode'=>0666,
            // 'newDirMode'=>0777,
        ),		
   	
        'srbac' => array(
			'userclass'				=> 'User', //optional defaults to User
			'userid'				=> 'id', //optional defaults to userid
			'username'				=> 'username', //optional defaults to username
			'debug'					=> false, //optional defaults to false
			'pagesize'				=> 20, //optional defaults to 15
			'superUser'				=> 'Admin', //optional defaults to Authorizer
			'css'					=> 'srbac.css', //optional defaults to srbac.css
			'layout'				=>
			'application.views.layouts.main', //optional defaults to empty string
			// must be an existing alias
			'notAuthorizedView'		=>
			'srbac.views.authitem.unauthorized', // optional defaults to
			//srbac.views.authitem.unauthorized, must be an existing alias
			'alwaysAllowed'			=> array( //optional defaults to gui
			'SiteLogin','SiteLogout','SiteIndex','SiteAdmin',
			'SiteError', 'SiteContact'),
			'userActions'			=> array(//optional defaults to empty array
			'Show','View','List'),
   			'imagesPack'			=> 'tango',
   			'iconText'				=> false,
			'listBoxNumberOfLines'	=> 20, //optional defaults to 10 'imagesPath' => 'srbac.images', //optional defaults to srbac.images 'imagesPack'=> 'noia', //optional defaults to noia 'iconText'=>true, //optional defaults to false 'header'=> 'srbac.views.authitem.header', //optional defaults to
			// srbac.views.authitem.header, must be an existing alias 'footer'=> 'srbac.views.authItem.footer', //optional defaults to
			// srbac.views.authitem.footer, must be an existing alias 'showHeader'=>true, //optional defaults to false 'showFooter'=>true, //optional defaults to false
			'alwaysAllowedPath'		=> 'srbac.components', //optional defaults to srbac.components
			// must be an existing alias
		),
		
	)
);

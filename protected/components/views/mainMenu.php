<ul>
<?php foreach($items as $item): ?>
<li><?php
//Yii::trace( CVarDumper::dumpAsString($item) );
// KA: add support to target attribute
$options = array();
if($item['active']) $options['class'] = 'active';
if( isset($item['target'])) $options['target'] = $item['target'];

echo CHtml::link($item['label'], $item['url'], $options ); ?></li>
<?php endforeach; ?>
</ul>

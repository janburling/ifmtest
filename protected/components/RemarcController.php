<?php

class RemarcController extends CController
{
	const		PAGE_SIZE=10;
	public		$defaultAction='index';
	protected	$_model;
	public	 	$breadcrumbs = array();
	public		$menu;
	public 		$auditSignature;
	
	public function run($actionID)
	{
		$this->auditSignature = $actionID;
		parent::run($actionID);
	}

    public function actions() {
        return array(
        	'delete'			=> array('class'=>'application.actions.DeleteAction'),
        	'clone'				=> array('class'=>'application.actions.CloneAction'),
        );
   	}
   	
	public function actionGetDivisionList()
	{
 		echo "<option value=''>-</option>";
		if(isset($_REQUEST['departmentId']) && $_REQUEST['departmentId']) $data= CHtml::listData(Division::model()->sort()->findAllByAttributes(array('departmentId'=>$_REQUEST['departmentId'])), 'id', 'name');
		else $data = CHtml::listData(Division::model()->sort()->findAll(), 'divisionId', 'name');
		foreach($data as $id=>$name) echo CHtml::tag('option', array('value'=>$id), CHtml::encode($name), true);
	}

	public function actionEditInline() 
	{
		$models = array();	
		$modelName = ucfirst($this->id);
		$summaryModel = new $modelName;
		$summaryModel->pageOffset = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
		
		Yii::trace(print_r($_REQUEST,true));
		
		if(isset($_REQUEST['payrollId'])) $summaryModel->payrollId = $_REQUEST['payrollId'];
		if(isset($_REQUEST['departmentId']) && isset($summaryModel->departmentId)) $summaryModel->departmentId = $_REQUEST['departmentId'];
		if(isset($_REQUEST['divisionId']) && isset($summaryModel->divisionId)) $summaryModel->divisionId = $_REQUEST['divisionId'];
		if(isset($_REQUEST['grossAdditionId'])) $summaryModel->grossAdditionId = $_REQUEST['grossAdditionId'];
		if(isset($_REQUEST['deductionId'])) $summaryModel->deductionId = $_REQUEST['deductionId'];
		if(isset($_REQUEST['timeAccrualId'])) $summaryModel->timeAccrualId = $_REQUEST['timeAccrualId'];


		if(isset($_POST[$modelName]))
		{
			$modelsForm = $_POST[$modelName];
			$summaryModel->attributes = $modelsForm;
			$recordCount = 0;
				
			if(isset($_REQUEST['save']) || isset($_REQUEST['create'])) {
				foreach($modelsForm as $key=>$formModel) {
					if($key === 'payrollId' || $key === 'departmentId' || $key === 'grossAdditionId' || $key === 'deductionId' || $key === 'divisionId' || $key === 'timeAccrualId') continue;
					$model = $summaryModel->findByPk($formModel[$summaryModel->primaryKeyName]);
					$model->attributes = $formModel;
					if($model->save()) $recordCount++;
					else $summaryModel->addErrors($model->errors);
				}
				Yii::app()->user->setFlash('success', $recordCount . " $summaryModel->modelLabel record(s) updated." . $summaryModel->formattedErrors);		
			}
			
			if(isset($_REQUEST['create'])) {
				$this->redirect(array('create', 'payrollId'=>$summaryModel->payrollId, 'departmentId'=>$summaryModel->departmentId, 'divisionId'=>$summaryModel->divisionId));	
			}
		}
		
		$this->breadcrumbs = array($summaryModel->modelsLabel=>array('index'), "Edit $summaryModel->modelsLabel Inline");
		$filters = array();
		
		if($summaryModel->hasAttribute('payrollId')) {
			if($summaryModel->payrollId) {
				$filters['payrollId'] = $summaryModel->payrollId;	
				$this->breadcrumbs = array(
					'Payrolls'=>array('payroll/index'),
					$summaryModel->Payroll->name=>array('/payroll/view', 'id'=>$summaryModel->payrollId),
					"Edit $summaryModel->modelsLabel Inline"
				);
			}
		}
		
		if($summaryModel->modelName == 'EmployeeDeduction') {
			$criteria = new CDbCriteria;
			$criteria->with = array('Employee.PrimaryPosition'=>array('select'=>'departmentId'));
			$criteria->compare('"PrimaryPosition"."departmentId"', $summaryModel->departmentId);
			$criteria->compare('"deductionId"', $summaryModel->deductionId);
			
			if($summaryModel->departmentId || $summaryModel->deductionId) {
				$models = $summaryModel->sort()->limit()->findAll($criteria);
				if(count($models) >= $summaryModel->editInlineLimit) Yii::app()->user->setFlash('warning', 'Results limited to ' . count($models) . ' records. Select more specific filters to see all records.');					
			}
		}
		else {
			if(isset($summaryModel->departmentId) && $summaryModel->departmentId) $filters['departmentId'] = $summaryModel->departmentId;
			if(isset($summaryModel->divisionId) && $summaryModel->divisionId) $filters['divisionId'] = $summaryModel->divisionId;
			if(isset($summaryModel->grossAdditionId) && $summaryModel->grossAdditionId) $filters['grossAdditionId'] = $summaryModel->grossAdditionId;
			if(isset($summaryModel->deductionId) && $summaryModel->deductionId) $filters['deductionId'] = $summaryModel->deductionId;
			if(isset($summaryModel->timeAccrualId) && $summaryModel->timeAccrualId) $filters['timeAccrualId'] = $summaryModel->timeAccrualId;
	
			if(isset($filters['departmentId']) || isset($filters['divisionId']) || isset($filters['grossAdditionId']) || isset($filters['deductionId']) || isset($filters['timeAccrualId'])) {
				$models = $summaryModel->sort()->limit()->findAllByAttributes($filters);
				if(count($models) >= $summaryModel->editInlineLimit && $summaryModel->modelName == 'EmployeePosition') {
					Yii::app()->user->setFlash('warning', 'Results limited to ' . count($models) . ' records. Use Save and Next to edit additional records.');
					$summaryModel->isMoreRecords = true;
				}
				elseif(count($models) >= $summaryModel->editInlineLimit) Yii::app()->user->setFlash('warning', 'Results limited to ' . count($models) . ' records. Select more specific filters to see all records.');					
			
			}
		}
		
		$this->render('editInline',array(
			'models'=>$models, 'model'=>$summaryModel
		));
	}
     	
   	public function actionUpdateDeductionDefaults()
	{
		$deduction = Deduction::model()->findByPk($_REQUEST['EmployeeDeduction']['deductionId']);
		if($deduction)
			header("Content-Type: application/json", true);
            echo CJSON::encode(array(
              'accountId'=>$deduction->defaultAccountId,
              'amount'=>$deduction->defaultAmount
            ));
	}
	 	
   	public function actionUpdateEmployeePositions()
	{
		$json = $this->getWorkTimeFieldUpdates();
		$json['employeePositionOptions'] = CHtml::tag('option', array('value'=>''),'-',true);
		$employeePayroll = EmployeePayroll::model()->findByPk($_REQUEST['WorkTime']['employeePayrollId']);
		if($employeePayroll) {
			$employeePositions = EmployeePosition::model()->findAllByAttributes(array(
				'employeeId'=>$employeePayroll->employeeId
			));
			if($employeePositions) {
				$options = '';
				foreach($employeePositions as $employeePosition)
				{
					if($employeePosition->employeePositionId == $employeePayroll->Employee->primaryPositionId) {
						$options .= CHtml::tag('option', array('value'=>$employeePayroll->Employee->primaryPositionId, 'selected'=>true),CHtml::encode($employeePosition->name),true);
					}
					$options .= CHtml::tag('option', array('value'=>$employeePosition->employeePositionId),CHtml::encode($employeePosition->name),true);
				}
				
				$json = $this->getWorkTimeFieldUpdates($employeePayroll->Employee->PrimaryPosition);
				$json['employeePositionOptions'] = $options;
			}	
		}	
		header("Content-Type: application/json", true);	
		echo CJSON::encode($json);			
	}
	
   	public function actionUpdateWorkTime()
	{
		header("Content-Type: application/json", true);	
		echo CJSON::encode($this->getWorkTimeFieldUpdates(
      		EmployeePosition::model()->findByPk($_REQUEST['WorkTime']['employeePositionId'])
		));
	}
	 	
   	public function getWorkTimeFieldUpdates($employeePosition = null)
	{
		if($employeePosition) {
			if($employeePosition->regularAccountId) $accountId = $employeePosition->regularAccountId;
			else $accountId = $employeePosition->rateType == 'H'?
				$employeePosition->Division->hourlyAccountId :
				$employeePosition->Division->salaryAccountId;
            return array(
              'rate'=>$employeePosition->hourlyRate,
              'shift'=>$employeePosition->normalShift,
              'shiftDifferential2'=>$employeePosition->shiftDifferential2,
              'shiftDifferential3'=>$employeePosition->shiftDifferential3,
              'hours'=>$employeePosition->normalHours,
              'days'=>$employeePosition->normalDays,
              'overtimeRate'=>$employeePosition->overtimeRate,
              'accountId'=>$accountId,
            );
        }
		return array(
		  'rate'=>0,
		  'shift'=>1,
		  'shiftDifferential2'=>0,
		  'shiftDifferential3'=>0,
		  'hours'=>0,
		  'days'=>0,
		  'overtimeRate'=>0,
		  'accountId'=>null,
		);
	}

   	public function actionUpdateDepartment()
	{
		$modelName = ucfirst($this->id);
		//Yii::trace($modelName);
		Yii::trace(print_r($_REQUEST, true));

		$json = array(
			$json['departmentId'] = '',
			$json['divisionId'] = '',
			$json['titleId'] = '',		
		);

		if(isset($_REQUEST[$modelName]['employeePayrollId'])) {
			$employeePayroll = EmployeePayroll::model()->findByPk($_REQUEST[$modelName]['employeePayrollId']);
			if($employeePayroll) $employeePosition = $employeePayroll->Employee->PrimaryPosition;
			else $employeePosition = null;
		}
		elseif(isset($_REQUEST[$modelName]['employeeId'])) {
			$employee = Employee::model()->findByPk($_REQUEST[$modelName]['employeeId']);
			if($employee) $employeePosition = $employee->PrimaryPosition;
			else $employeePosition = null;
		}
		else $employeePosition = null;
		
		if($employeePosition) {
			$json['departmentId'] = $employeePosition->departmentId;
			$json['divisionId'] = $employeePosition->divisionId;
			$json['titleId'] = $employeePosition->titleId;			
		}
		
/*		$json['employeePositionOptions'] = CHtml::tag('option', array('value'=>''),'-',true);
		$employeePayroll = EmployeePayroll::model()->findByPk($_REQUEST['WorkTime']['employeePayrollId']);
		if($employeePayroll) {
			$employeePositions = EmployeePosition::model()->findAllByAttributes(array(
				'employeeId'=>$employeePayroll->employeeId
			));
			if($employeePositions) {
				$options = '';
				foreach($employeePositions as $employeePosition)
				{
					if($employeePosition->employeePositionId == $employeePayroll->Employee->primaryPositionId) {
						$options .= CHtml::tag('option', array('value'=>$employeePayroll->Employee->primaryPositionId, 'selected'=>true),CHtml::encode($employeePosition->name),true);
					}
					$options .= CHtml::tag('option', array('value'=>$employeePosition->employeePositionId),CHtml::encode($employeePosition->name),true);
				}
				
				$json = $this->getWorkTimeFieldUpdates($employeePayroll->Employee->PrimaryPosition);
				$json['employeePositionOptions'] = $options;
			}	
		}	
*/
		header("Content-Type: application/json", true);	
		echo CJSON::encode($json);			

	}

   	public function actionIndex()
	{
		Yii::app()->user->returnUrl = '';
		$modelName = ucfirst($this->id);
		$model = new $modelName;
		$model->scenario = 'search';
		$model->unsetAttributes();  // clear any default values
		if(isset($_REQUEST[$modelName])) $model->attributes=$_REQUEST[$modelName];
		$activeTab = isset($_REQUEST['activeTab']) ? $_REQUEST['activeTab'] : null;
		
		$this->breadcrumbs = array($model->modelsLabel);

		$this->render('/common/index',array(
			'model'=>$model, 'activeTab'=>$activeTab
		));
	}
	
	public function actionView($id) {
		Yii::app()->user->returnUrl = Yii::app()->getRequest()->getUrl();
		$model = $this->loadModel($id);
		
		$this->breadcrumbs = array(
			$model->modelsLabel=>array('index'),
			$model->name,
		);
		
		if(strtolower($this->id) == 'payroll') {
			if($model->status == 'L') Yii::app()->user->setFlash('success', "Payroll processing started... you'll receive an email once it's complete. " . '<a href="javascript:location.reload(true)">Refresh Page</a>' . $model->formattedErrors);
		}
		
		$this->render('view',array(
			'model'=>$model,
			'activeTab'=>isset($_REQUEST['activeTab']) ? $_REQUEST['activeTab'] : '',
		));
	}	
	
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->scenario = 'update';
		
		if($model->modelName == 'Payroll') $payroll = $model;
		elseif($model->hasAttribute('payrollId')) $payroll = $model->Payroll;
		else $payroll = null;
		
		if($payroll) {
			if($payroll->status == 'L') {
				Yii::app()->user->setFlash('error', "$model->modelName can not be updated while payroll is locked for processing.");
				$this->redirect(array('/payroll/view', 'id'=>$payroll->payrollId));	
			}
		}
		if($model->modelName == 'WorkTime') {
			if(!isset(Yii::app()->user->departmentIds))
				Yii::app()->user->setState('departmentIds', User::model()->findByPk(Yii::app()->user->id)->departmentIds);			
			if($model->approvedUserId && Yii::app()->user->departmentIds) {
				Yii::app()->user->setFlash('error', "This work time can not be updated since it has already been approved.");
				$this->redirect(array('view', 'id'=>$model->primaryKey));
			}
		}
		
		if(isset($_POST[$model->modelName]))
		{
			$model->attributes=$_POST[$model->modelName];
		
			if($model->modelName == "WorkTime" || $model->modelName == "PayrollTimeAccrual")
				$activeTab = 'Time-tab';
			elseif($model->modelName == "PayrollGrossAddition" || $model->modelName == "GrossAdditionTransaction") 
				$activeTab = 'GrossAddition-tab';
			elseif($model->modelName == "PayrollDeduction" || $model->modelName == "DeductionTransaction")
				$activeTab = 'Deduction-tab';
			else $activeTab = null;
			
			if(Yii::app()->user->returnUrl) {
				if(strpos(Yii::app()->user->returnUrl, 'activeTab')) $return = Yii::app()->user->returnUrl;
				else $return = Yii::app()->user->returnUrl . '?activeTab=' . $activeTab;
			}
			else $return = array('view', 'id'=>$model->primaryKey, 'activeTab'=>$activeTab);
			
			if($model->save()) $this->redirect($return);
		}
		else { if(isset($_SERVER['HTTP_REFERER'])) Yii::app()->user->setReturnUrl($_SERVER['HTTP_REFERER']); }
		
		$this->breadcrumbs = array(
			$model->modelsLabel=>array('index'),
			$model->name=>array('view', 'id'=>$model->primaryKey),
		);

		$this->render('/common/update',array(
			'model'=>$model,
		));
	}

	public function actionCreate()
	{
		$model = new $this->modelName;
		if(isset($_REQUEST['payrollId']) 			&& $model->hasAttribute('payrollId')) 			$model->payrollId 				= $_REQUEST['payrollId'];
		if(isset($_REQUEST['employeeId']) 			&& $model->hasAttribute('employeeId')) 			$model->employeeId 				= $_REQUEST['employeeId'];
		if(isset($_REQUEST['employeePositionId']) 	&& $model->hasAttribute('employeePositionId')) 	$model->employeePositionId 		= $_REQUEST['employeePositionId'];
		if(isset($_REQUEST['employeePayrollId']) 	&& $model->hasAttribute('employeePayrollId')) 	$model->employeePayrollId 		= $_REQUEST['employeePayrollId'];
		if(isset($_REQUEST['grossAdditionCategoryId']) 	&& $model->hasAttribute('grossAdditionCategoryId')) $model->grossAdditionCategoryId = $_REQUEST['grossAdditionCategoryId'];
		if(isset($_REQUEST['deductionCategoryId']) 	&& $model->hasAttribute('deductionCategoryId')) $model->deductionCategoryId 	= $_REQUEST['deductionCategoryId'];
		if(isset($_REQUEST['departmentId'])			&& $model->hasAttribute('departmentId')) 		$model->departmentId 			= $_REQUEST['departmentId'];
		if(isset($_REQUEST['divisionId'])			&& $model->hasAttribute('divisionId')) 			$model->divisionId 				= $_REQUEST['divisionId'];
		if(isset($_REQUEST['projectId'])			&& $model->hasAttribute('projectId')) 			$model->projectId 				= $_REQUEST['projectId'];
		if(isset($_REQUEST['reportFolderId'])		&& $model->hasAttribute('reportFolderId')) 		$model->reportFolderId 			= $_REQUEST['reportFolderId'];
		if(isset($_REQUEST['reportDefinitionId'])	&& $model->hasAttribute('reportDefinitionId')) 	$model->reportDefinitionId 		= $_REQUEST['reportDefinitionId'];
		if(isset($_REQUEST['bargainingUnitId'])		&& $model->hasAttribute('bargainingUnitId')) 	$model->bargainingUnitId 		= $_REQUEST['bargainingUnitId'];
		if(isset($_REQUEST['gradeId'])				&& $model->hasAttribute('gradeId')) 			$model->gradeId			 		= $_REQUEST['gradeId'];
		if(isset($_REQUEST['stepId'])				&& $model->hasAttribute('stepId')) 				$model->stepId			 		= $_REQUEST['stepId'];
		if(isset($_REQUEST['payeesGroupId'])		&& $model->hasAttribute('payeesGroupId')) 		$model->payeesGroupId			= $_REQUEST['payeesGroupId'];
		if(isset($_REQUEST['userId'])				&& $model->hasAttribute('userId')) 				$model->userId					= $_REQUEST['userId'];
		if(isset($_REQUEST['userId'])				&& $model->hasAttribute('userid')) 				$model->userid					= $_REQUEST['userId'];
		if(isset($_REQUEST['groupId'])				&& $model->hasAttribute('groupId')) 			$model->groupId					= $_REQUEST['groupId'];
		if(isset($_REQUEST['timeImportId'])			&& $model->hasAttribute('timeImportId')) 		$model->timeImportId			= $_REQUEST['timeImportId'];
		if(isset($_REQUEST['timeImportLineId'])		&& $model->hasAttribute('timeImportLineId')) 	$model->timeImportLineId		= $_REQUEST['timeImportLineId'];
		if(isset($_REQUEST['bankId'])				&& $model->hasAttribute('bankId'))			 	$model->bankId					= $_REQUEST['bankId'];
		
		if(isset($model->payrollId)) {
			$payroll = Payroll::model()->findByPk($model->payrollId);
			if($payroll) {
				if($payroll->status == 'L') {
					Yii::app()->user->setFlash('error', "{$model->modelName} can not be created while payroll is locked for processing.");
					$this->redirect(array('/payroll/view', 'id'=>$payroll->payrollId));	
				}
			}
		}
		if($model->modelName == 'WorkTime') {
			if(!isset(Yii::app()->user->departmentIds))
				Yii::app()->user->setState('departmentIds', User::model()->findByPk(Yii::app()->user->id)->departmentIds);			
			if($model->approvedUserId && Yii::app()->user->departmentIds) {
				Yii::app()->user->setFlash('error', "This work time can not be updated since it has already been approved.");
				$this->redirect(array('view', 'id'=>$model->primaryKey));
			}
		}

		if($model->modelName == "WorkTime" || $model->modelName == "PayrollTimeAccrual")
			$activeTab = 'Time-tab';
		elseif($model->modelName == "PayrollGrossAddition" || $model->modelName == "GrossAdditionTransaction") 
			$activeTab = 'GrossAddition-tab';
		elseif($model->modelName == "PayrollDeduction" || $model->modelName == "DeductionTransaction")
			$activeTab = 'Deduction-tab';
		else $activeTab = null;
		
		//set the return url for save/clone redirect and cancel button
		if(Yii::app()->user->returnUrl) {
			if(isset($_REQUEST['departmentId']) && !strpos(Yii::app()->user->returnUrl, 'department_search')) $returnUrl = Yii::app()->user->returnUrl . '/department_search/' . $_REQUEST['departmentId'];
			elseif(strpos(Yii::app()->user->returnUrl, 'activeTab')) $returnUrl = Yii::app()->user->returnUrl;
			else $returnUrl = Yii::app()->user->returnUrl . '?activeTab=' . $activeTab;
		}
		else $returnUrl = array($model->viewUrl, 'activeTab'=>$activeTab);

		if(isset($_POST[$model->modelName])) {
			$model->attributes=$_POST[$model->modelName];
			if($model->save()) {
				if(isset($_REQUEST['saveAndClone'])) $returnUrl = array('clone', 'id'=>$model->primaryKey);
				//hacks to redirect to appropriate view on special cases
				elseif($model->modelName == 'UserAuthItem') $returnUrl = array('user/view', 'id'=>$model->primaryKey);
				elseif($model->modelName == 'PayeesGroup') $returnUrl = array('payeesGroup/view', 'id'=>$model->primaryKey);			
				$this->redirect($returnUrl);
			}
		}
		else { if(isset($_SERVER['HTTP_REFERER'])) Yii::app()->user->setReturnUrl($_SERVER['HTTP_REFERER']); }
		
		$this->render('/common/create',array(
			'model'=>$model,
			'returnUrl'=>$returnUrl
		));
	}

   	public function actionBatchUpdate()
   	{
   		$models	= CActiveRecord::model($this->modelName)->findAllByPk($_REQUEST['ids']);
   		foreach ($models as $model)
   		{
			Yii::trace("{$model->modelName}: {$model->primaryKey}");
   			$model->checkActionAccess( $this );
   		}
   		$modelName	= $models[0]->modelName;
   		
   		// form submitted
   		if( isset($_POST[$modelName]) )
   		{
   			$valid = true;
   			foreach ($models as $model)
   			{
   				$model->attributes	= $_POST[$modelName][$model->primaryKey];
   				$valid	= $model->validate() && $valid;
   			}
   			
   			if($valid)
   			{
   				foreach ($models as $model) $model->save();
			    $passParameters	= array();
			    $route			= 'list';									
				$url			= $this->createUrl( $route , $passParameters );	      
				$this->redirect($url);
   			}
   		}
   		$this->render('/common/batchUpdate', array( 'models' => $models ) );
   	}

	// obsolete functions, instead used an instance of the model in context
	
	//public function findAllModels($criteria=null) 	{ return Address::model()->findAll($criteria); }
	//public function countModels($criteria=null) 	{ return Address::model()->count($criteria); }
	//public function findAllModelsByPk($ids) 		{ return Address::model()->findAllByPk($ids); }	
	
	//public function getListViewAttributes()	{ return Address::model()->listViewAttributes; }
	//public function getModelLabel() 		{ return Address::model()->modelLabel; }
	//public function getModelsLabel() 		{ return Address::model()->modelsLabel; }
	//public function getDefaultSort() 		{ return Address::model()->defaultSort; }

	public function getModel()				{ return $this->_model; }

	public function renderGroupsTabs( $tabsGroup, $form, $update = true )
	{
		$form->isUpdate	= $update;
		$tabs		= array();
		$header		= '';
		foreach( $tabsGroup as $title=>$element )
		{
			if( $update && isset($element['visibleOnForm']) && !$element['visibleOnForm'] ) continue;
			if( isset( $form->elements[$title] ) && !$form->elements[$title]->visible ) continue;
			
			$elements = $element['elements'];
			// header info displayed above all tabs
			if( strtolower($title) == 'header' )
			{
				$header = $update? $form->renderElementsGroup( $elements ) : $form->renderShowElementsGroup( $elements );
				$header .= '<br/><br/>'; 
				continue;
			}
			$this->beginClip($title);
			echo $update? $form->renderElementsGroup( $elements ) : $form->renderShowElementsGroup( $elements );
			$this->endClip();
		}
		
		$clips 		= $this->clips;
		$viewData	= array ('form' => $form);
		
		foreach( $tabsGroup as $title=>$element )
		{
			if( $update && isset($element['visibleOnForm']) && !$element['visibleOnForm'] ) continue;
			if( isset( $form->elements[$title] ) && !$form->elements[$title]->visible ) continue;
			if( strtolower($title) == 'header' ) continue;
			
			//$tabs["tab{$element['elements'][0]}"] = array('title'=>CModel::generateAttributeLabel($title) , 'content'=>$clips[$title]);
		}
		
		echo $header;
		$this->widget('CTabView', array('tabs'=>$tabs, 'viewData'=>$viewData));
	}
	
	public function renderGroupsColumns( $tabsGroup, $form, $update = true )
	{
		$form->isUpdate	= $update;
		echo '<table class="columns"><tr>';
		foreach( $tabsGroup as $title=>$element )
		{
			$elements = $element['elements'];
			echo "<td valign=\"top\" id=\"column$title\">";
			if( $update )
			{
				echo $form->renderElementsGroup( $elements );
			}
			else
			{
				echo $form->renderShowElementsGroup( $elements );
			}
			echo '</td>';
		}
		echo '</tr></table>';
	}
	
	public function getModelName() { return ucfirst( $this->id ); }
	 
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			$newModel		= $this->createModel();
			$primaryKeyName	= $newModel->primaryKeyName;
			if(isset($_GET[$primaryKeyName])) $this->_model= $newModel->resetScope()->findbyPk($_GET[$primaryKeyName]);
			elseif(isset($_GET['id'])) $this->_model= $newModel->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
	public function createModel() {
		return new $this->modelName;
	}
	
	public function displayOperationMessage()
	{
	}
	
	public static function setOperationMessage($message, $returnUrl=null, $isError=false)
	{
		Yii::app()->user->setState('operationMessage', $message);
		Yii::app()->user->setState('isError', $isError);
		if($returnUrl)
			Yii::app()->request->redirect($returnUrl);
	}
	
	public static function getCancelUrl($model=null)
	{
		if(true)
		{
			return CHttpRequest::getUrlReferrer();
		}
	}
	
	public function updateDependentFields($model, $key=array())
	{
		if( isset($_REQUEST['updateDependentFields']))
		{
			$model->attributes	= $_REQUEST[$model->modelName];
			$model->updateDependentFieldsBy	= $_REQUEST['updateDependentFields'];
			$model->updateDependentFields($key);
			return false;
		}
		return true;
	}
}
?>
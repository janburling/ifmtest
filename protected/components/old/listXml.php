<?php

class ListXml extends CWidget {

	public $xml;
	public $ids;
	public $model;
	
	public function run()
    {
		$criteria = new CDbCriteria; //used to define sql criteria

		isset($_REQUEST['ids'])		? $ids 			= $_REQUEST['ids']			: $ids = array();
		isset($_REQUEST['page']) 	? $listPage 	= $_REQUEST['page'] 		: $listPage = 0; //page number to display
		isset($_REQUEST['rows']) 	? $listRows 	= $_REQUEST['rows'] 		: $listRows = Yii::app()->params['defaults']['listRowSize']; //number of rows to display
		isset($_REQUEST['sidx']) 	? $sortName 	= $_REQUEST['sidx'] 		: $sortName = $this->controller->defaultSort['name']; //sort column 
		isset($_REQUEST['sord']) 	? $sortOrder 	= $_REQUEST['sord'] 		: $sortOrder = $this->controller->defaultSort['order']; //sort order
		isset($_REQUEST['_search']) ? $isSearch 	= $_REQUEST['_search']		: $isSearch = 'false';

		$startRecord = $listRows * $listPage - $listRows; //first record to display
		if($startRecord < 0) $startRecord = 0; //make sure it's positive

		if($isSearch == 'true') {
			foreach($_REQUEST as $searchKey=>$searchValue) { //loop through all params looking for search keys
				foreach($this->model->listViewAttributes as $columnName=>$columnLabel) { //loop through all available columns
					if($searchKey == $columnName) {
						$criteria->addCondition($searchKey . " LIKE '%" . $searchValue . "%'"); //add criteria matching the search string for the column
						#$searchKey . ' = ' . $searchValue; // TODO - determine if some fields should be exact matches
					}
				}
			}
		}
			
		if(!empty($ids)) { //checks to see if user passed a selected list of items
			$recordCount = sizeOf($ids); //get a record count for paging purposes
			$models = $this->controller->findAllModelsByPk($ids); //get all the records for passed ids
			
/*			Can't currently do a count an in statement or pass an array for in statement
			$criteria->condition='id=:ids';
			$criteria->params=array(':ids'=>$ids);
			$criteria->addInCondition('id', $ids);
			$models=WorkLog::model()->findAll('id=:ids', array(':ids'=>$ids));
			$recordCount = WorkLog::model()->count('id in :ids', array('ids'=>$ids));
*/			
		} else {
			$recordCount = $this->controller->countModels($criteria); //get a record count for paging purposes
			$criteria->order = $sortName . ' ' . $sortOrder; //set the order by statement
			$criteria->offset = $startRecord; //set the direction for the order by statement
			$criteria->limit = $listRows; //set how many rows to return (page length)
			$models=$this->controller->findAllModels($criteria); //get all the records based on search criteria
		}	
			
		if($recordCount > 0 && $listRows > 0) $totalPages = ceil($recordCount / $listRows); //figure out how many pages are in the list view
		else $totalPages = 0; //make sure it's not negative or undefined
		if($listPage > $totalPages) $listPage = $totalPages; //don't let the page be in an undefined range

		$xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<rows></rows>"); //initial xml output definition for jqgrid	
		
		$xml->addChild('page', $listPage); //add to the xml to be passed
		$xml->addChild('total', $totalPages); //add to the xml to be passed
		$xml->addChild('records', $recordCount); //add to the xml to be passed
		
		
		foreach($models as $model) { //loop through the returned records and create xml
		    $row = $xml->addChild('row'); //start a new row
		    $row->addAttribute('id', $model->id); //define the row id
		    foreach($this->controller->listViewAttributes as $columnKey=>$columnName) { //loop through all the columns to display
		    	$row->addChild('cell', $model->$columnKey); //add the column cell
		    }
		}		
		
		$this->render('listXml', array('xml'=>$xml)); //pass the xml to the lixtXML view
	}
}
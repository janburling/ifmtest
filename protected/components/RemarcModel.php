<?php

class RemarcModel extends CActiveRecord
{	

	public $listType				= null;
	
	public $excludeFromAudit		= false; 
	public $auditSignature			= null;
	public $modelBeforeSave			= null;
	public $skipAuditFields			= array('modifiedDate');
	public $updateDependentFieldsBy	= null;
	public $isListRow				= false;
	public $hideActions;
	public $hideFilters;
	public $pageSize 				= 20;
	public $editInlineLimit			= 100;
	public $pageOffset				= 1;
	public $isMoreRecords			= false;
	
	public function defaultScope() {
		if($this->hasAttribute('departmentId')) {

			if(!isset(Yii::app()->user->departmentIds))
				Yii::app()->user->setState('departmentIds', User::model()->findByPk(Yii::app()->user->id)->departmentIds);			
			if(implode(',', Yii::app()->user->departmentIds)) {
				return array(
					'condition'=>'"'. $this->getTableAlias(false, false) .'"."departmentId" in ('. implode(',', Yii::app()->user->departmentIds) . ')'
				);
			}
		}
		
		elseif($this->hasAttribute('employeePositionId')) {
			if(!isset(Yii::app()->user->departmentIds))
				Yii::app()->user->setState('departmentIds', User::model()->findByPk(Yii::app()->user->id)->departmentIds);			
			if(implode(',', Yii::app()->user->departmentIds)) {
				return array(
					'with'=>'EmployeePosition',
					'condition'=>'"EmployeePosition"."departmentId" in ('. implode(',', Yii::app()->user->departmentIds) . ')',
				);
			}		
		}
		return array();
	}
		
	public function rules() {
		return array(
			array(implode(',',array_keys($this->attributes)), 'default', 'setOnEmpty'=>true, 'value'=>null)
		);
	}
    public function scopes()
    {
    	$scopes = array();
    	$scopes['sort'] = array();
    	$scopes['limit'] = array(
    		'limit'=>$this->editInlineLimit,
    		'offset'=>$this->pageOffset > 1 ? (($this->pageOffset-1) * $this->editInlineLimit) : 0,
    		//'offset'=>100,
    	);

		if($this->hasAttribute('code')) $scopes['sort'] = array('order'=>'"'.$this->tableAlias.'"."code" asc');
		elseif($this->hasAttribute('name')) $scopes['sort'] = array('order'=>'"'.$this->tableAlias.'"."name" asc');
		elseif($this->hasAttribute('accountNumber')) $scopes['sort'] = array('order'=>'"'.$this->tableAlias.'"."accountNumber" asc');
		elseif($this->hasAttribute('lastName') && $this->hasAttribute('firstName')) $scopes['sort'] = array('order'=>'"' . $this->tableAlias . '"."lastName" asc, "' . $this->tableAlias . '"."firstName" asc');
		elseif($this->hasAttribute('employeeId')) $scopes['sort'] = array('with'=>'Employee', 'order'=>'"Employee"."lastName" asc, "Employee"."firstName" asc');
		elseif($this->hasAttribute('deductionId')) $scopes['sort'] = array('with'=>'Deduction', 'order'=>'"Deduction"."code" asc, "Deduction"."code" asc');
		
        return $scopes;   	
    }    
    
	public function getDbConnection()
	{
		if(self::$db!==null) {
			if(Yii::app()->db->connectionString == self::$db->connectionString) return self::$db;
		}
		try {
			self::$db=Yii::app()->getDb();
			if(self::$db instanceof CDbConnection)
				return self::$db;
			else
				throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
		} catch(Exception $e) { Yii::app()->user->loginRequired(); Yii::log($e, 'error'); }
	}    
	
	public static function refreshDbConnection()
	{
		self::$db=Yii::app()->getDb();
		if(self::$db instanceof CDbConnection)
			return self::$db;
		else
			throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
	}
	

	public function getGridColumnButtons() {
		return array(
			'class'=>'TbButtonColumn',
			'buttons'=>array(
				'view'=>array('url'=>'array("/$data->modelName/view/", "id"=>$data->primaryKey);'),
				'update'=>array('url'=>'array("/$data->modelName/update/", "id"=>$data->primaryKey);'),
				'delete'=>array('url'=>'array("/$data->modelName/delete/", "id"=>$data->primaryKey);'),
			),
		    'afterDelete'=>'function(link,success,data){ if(success) $("#flashMessage").html(data) }',

		);
	}
	
	public function getGridActions() {
		if($this->hideActions) return null;
		$urlArray = array($this->modelName.'/create');
		if(isset($this->payrollId)) $urlArray['payrollId'] = $this->payrollId;
		if(isset($this->employeeId)) $urlArray['employeeId'] = $this->employeeId;
		if(isset($this->grossAdditionCategoryId)) $urlArray['grossAdditionCategoryId'] = $this->grossAdditionCategoryId;
		if(isset($this->deductionCategoryId)) $urlArray['deductionCategoryId'] = $this->deductionCategoryId;
		if(isset($this->employeePositionId)) $urlArray['employeePositionId'] = $this->employeePositionId;
		if(isset($this->employeePayrollId)) $urlArray['employeePayrollId'] = $this->employeePayrollId;
		if(isset($this->departmentId)) $urlArray['departmentId'] = $this->departmentId;
		if(isset($this->divisionId)) $urlArray['divisionId'] = $this->divisionId;
		if(isset($this->projectId)) $urlArray['projectId'] = $this->projectId;
		if(isset($this->reportFolderId)) $urlArray['reportFolderId'] = $this->reportFolderId;
		if(isset($this->reportDefinitionId)) $urlArray['reportDefinitionId'] = $this->reportDefinitionId;
		if(isset($this->bargainingUnitId)) $urlArray['bargainingUnitId'] = $this->bargainingUnitId;
		if(isset($this->gradeId)) $urlArray['gradeId'] = $this->gradeId;
		if(isset($this->stepId)) $urlArray['stepId'] = $this->stepId;
		if(isset($this->payeesGroupId)) $urlArray['payeesGroupId'] = $this->payeesGroupId;
		if(isset($this->userId)) $urlArray['userId'] = $this->userId;
		if(isset($this->groupId)) $urlArray['groupId'] = $this->groupId;
		if(isset($this->timeImportId)) $urlArray['timeImportId'] = $this->timeImportId;
		if(isset($this->timeImportLineId)) $urlArray['timeImportLineId'] = $this->timeImportLineId;
		if(isset($this->bankId)) $urlArray['bankId'] = $this->bankId;

		return Yii::app()->controller->widget('TbButton', array('buttonType'=>'link', 'label'=>'Create ' . $this->modelLabel, 'url'=>$urlArray), true);
	}
    
	public function getRelatedModel($modelName)
	{
		$allModels	= self::getModelNames();
		$modelUC	= strtoupper( $modelName );
		foreach( $allModels as $model ){
			if( strtoupper( $model ) == $modelUC ){
				return new $model;
			}
		}
		
		if( in_array( $modelUC , array('RECEIPTREPORT', 'STATUSREPORT') ))	return new ReportDefinition;
		if( in_array( $modelUC , array('GAINACCOUNT', 'LOSSACCOUNT', 'UNDERPAYMENTACCOUNT') ))	return new Account;
				
		throw new CHttpException('Could not find a related model by the name of \'' . $modelName . '\'. Add an appropriate case to the PHModel.');
	}
	
	public static function getModelNames() {
		$excludeModels	= array( 'itemchildren', 'items', 'assignments');
		$tableNames		= array_diff( self::$db->schema->tableNames, $excludeModels);
		asort( $tableNames );
		return array_combine($tableNames, $tableNames);
	}

	public function updateDependentFields($key=array())
	{
		if($key)
		{
			//$foreignKey			= $this->getModelPrimaryKeyName($key['name']);
			$foreignKey			= self::model($key['name'])->primaryKeyName;
			$form				= $this->form;
			$form[$foreignKey]->showOnly = true;
			//$this->form			= $form;
		}
	}
	
	public function handleSaveException()
	{
		throw new CHttpException('200', "Failed to save {$this->modelName}. Error: " . print_r($this->getErrors(), true ) );
	}
	
	/*
	public function validate($attributes=null, $clearErrors=true)
	{
		if( isset($_REQUEST['updateDependentFields']))
		{
			if( !$this->updateDependentFields() ) return false;
		}
		return parent::validate($attributes, $clearErrors);
	}
	*/
	
	protected function beforeValidate()
	{
		if($this->isNewRecord) {
			if($this->hasAttribute('createdDate'))		$this->createdDate		= date("Y-m-d H:i:s", time());
			if($this->hasAttribute('organizationId'))	$this->organizationId	= Yii::app()->user->organizationId;
		}
		if($this->hasAttribute('modifiedDate'))			$this->modifiedDate		= date("Y-m-d H:i:s", time());
		if($this->hasAttribute('modifiedUser'))			$this->modifiedUser		= Yii::app()->user->id;
/*		
		foreach( $this->formConfig['elements'] as $fieldName=>$element )
		{
			// display only fields
			if( !( $this->hasAttribute($fieldName) || property_exists($this, $fieldName) ) ) continue;
			
			$type = isset( $element['appType'] )? $element['appType'] : $element['type'];
			if( $type == 'application.extensions.palmerhill.PHList' ) continue;
			switch( $type )
			{
				case 'application.extensions.jui.EDatePicker':
					//Yii::trace("$fieldName: {$this->$fieldName} (before)");
					$trimmed = trim($this->$fieldName);
					if( empty( $trimmed ) || $this->$fieldName == null )
					{
						$this->$fieldName = null;
					}
					else
					{
						$this->$fieldName = Yii::app()->dateFormatter->format('yyyy-MM-dd', $this->$fieldName);
					}
					//Yii::trace("$fieldName: {$this->$fieldName} (after)");
					break;
					
				case 'currency':
					if( !$this->$fieldName ) $this->$fieldName = 0;
					$this->$fieldName	= str_replace(array('$',','), '', $this->$fieldName);
					$this->$fieldName	= round($this->$fieldName, 2);
					break;
					
				default:
					if( $this->$fieldName == '' )
					{
						$this->$fieldName = null;
					}
			}
			
		}
*/		
		/*
		foreach($this->viewAttributes as $name=>$attributes) {
			if(isset($attributes['type']) && !isset($attributes['readOnly'])) {
				if($attributes['type'] == 'currency' || $attributes['type'] == 'number' || $attributes['type'] == 'decimal') {
					if(!$this->$name) $this->$name = 0;
					$this->$name = str_replace(array('$',','), '', $this->$name);
				}
				elseif($attributes['type'] == 'date') {					
					if(trim($this->$name) == '' || $this->$name == null) {
						$this->$name = null;
					}
					else {
						$this->$name = Yii::app()->dateFormatter->format('yyyy-MM-dd', $this->$name);
					}
				}
			}
			#if(!$this->name) $this->$name = null;
		}
		*/
		
		return true;
	}

	protected function beforeSave()
	{
	    if(parent::beforeSave())
	    {
	        if( !$this->excludeFromAudit )
	        {
		    	if( !$this->isNewRecord ) // update
		        {
		        	$this->modelBeforeSave = $this->findByPk($this->primaryKey);
		        }
	        }
	        return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	
	protected function afterSave()
	{
	    parent::afterSave();
	    $modelBeforeSave = $this->modelBeforeSave;
	    if( !$this->excludeFromAudit )
	    {
			$insertRows			= array();
			$user				= Yii::app()->user->name;
		   	$organizationId		= Yii::app()->user->organizationId;
			$changeTime			= date( 'Y-m-d G:i:s');					
	    	
	    	if( $modelBeforeSave ) //this is an update
	    	{
	    		if( $this->auditSignature === null ) $this->auditSignature = 'update';
	    		$modelAfterSave		= $this->findByPk( $this->primaryKey );
		    	$attributes			= $modelAfterSave->attributes;
		    	
		    	foreach( $attributes as $key=>$value )
		    	{		    		
		    		if( $modelBeforeSave->$key == $value || in_array( $key, $this->skipAuditFields ) ) continue;
		    		// escape special chars
		    		$modelBeforeSave->$key = str_replace("'", "''", $modelBeforeSave->$key);    		
		    		$value = str_replace("'", "''", $value);
		    		
		    		$modelBeforeSave->$key = str_replace("\\", "\\\\", $modelBeforeSave->$key);    		
		    		$value = str_replace("\\", "\\\\", $value);
		    		
		    		$insertRows[] = "( '" . $this->auditSignature . "', '" . $this->modelName . "', " . $modelAfterSave->primaryKey . ", '$key', '" . $modelBeforeSave->$key . "', '$value', '$user', '$changeTime', '$organizationId')";
		    	}
	    	}
	    	else // this is a new record
	    	{ 
	    		if( $this->auditSignature === null ) $this->auditSignature = 'create';
	    		$insertRows[]	= "( '{$this->auditSignature}', '{$this->modelName}', {$this->primaryKey}, '', '', '', '$user', '$changeTime', '$organizationId')";
	    	}
	    	$this->addToAuditLog($insertRows);		    	
	    }
	}
	
	protected function afterDelete()
	{
	    parent::afterDelete();
	    if( $this->excludeFromAudit ) return;
	    
		// add to audit log
		$insertRows		= array();
		$user			= Yii::app()->user->name;
	   	$organizationId = Yii::app()->user->organizationId;	
		$changeTime		= date( 'Y-m-d G:i:s');					
		$insertRows[]	= "( 'delete', '{$this->modelName}', {$this->primaryKey}, '', '', '', '$user', '$changeTime', $organizationId)";
		$this			->addToAuditLog($insertRows);
    }
	
	public function checkActionAccess( $controller )
	{
		$operation	= "{$this->modelName} " . $this->generateAttributeLabel( $controller->action->id );
		//Yii::trace($operation);
		$params		= array('model'=>$this);
		if(!Yii::app()->user->checkAccess($operation, $params)) {		
			$controller->render('/common/accessViolation',
				array(
					'operation'	=> $operation,
					'returnURL'	=> Yii::app()->user->returnUrl,					
				)
			);
			CApplication::end();
		}
	}		
    
    public function addToAuditLog($insertRows)
	{
    	if( !empty($insertRows) )
    	{
    		$query			=	'INSERT INTO "AuditLog" ("operation", "object", "objectId", "field", "previousValue", "newValue", "userName", "time", "organizationId")' .
    							"\n VALUES \n" .
    							implode( ",\n", $insertRows );
			//Yii::trace( "query: $query");			
    		//$transaction	= $this->dbConnection->beginTransaction();				
			try
			{
				Yii::app()->db->createCommand($query)->execute();
				//$transaction->commit();
				// transaction completed
			} // end try
			catch(Exception $e)
			{
				//$transaction->rollBack();				
				// re-throw Exception to be caught as normal
				throw $e;
			} // end catch
		}
	}
	
	public function externalLookup($model) {
		
		if($model->primaryKey) { //if an id was specified, look for it
			$this->getDbCriteria()->mergeWith(array(
        		'condition'=>'"id" = :id',
        		'params'=>array('id'=>$model->primaryKey)
        	));
		}
		
		else { //otherwise
			$this->getDbCriteria()->mergeWith(array(
        		'condition'=>'"id" = :id',
        		'params'=>array('id'=>null)
        	));		}
		
    	return $this;
		
	}
	
	public function attributeLabels() {		
		$labels = array();
		return $labels; // short circuit
		foreach($this->viewAttributes as $name=>$attributes)
			$labels[$name] = $attributes['label'];
		return $labels;
	}
	
	public function hasChildren() {
		foreach($this->relations() as $relationName=>$relation) {
			if($relation[0] == 'CHasManyRelation') {
				if($relation[1] != 'DeferredTransactionalUpdate') {
					if(count($this->$relationName)) {
						return $relation[1];
					}
				}
			}
		}
		return false;
	}
	
	public function getFieldType($field)
	{
		if( isset( $this->formConfig['elements'][$field] ) )
		{
			$formElement = $this->formConfig['elements'][$field];
			return ( isset( $formElement['appType'] )? $formElement['appType'] : $formElement['type'] );
		}
		else // attribute in list but not in form
		{
			//Yii::trace("list type: {$this->listViewConfig['elements'][$field]['type']}");
			return ( isset( $this->listViewConfig['elements'][$field]['type'] )? $this->listViewConfig['elements'][$field]['type'] : 'text' );
		}
	}
	
	public function relatedListConfig( $relationName , $visible=null, $elements=array() )
	{
		$relation		= $this->getMetaData()->relations[$relationName];
		$relatedClass	= $relation->className;
		$criteria	= new CDbCriteria;
	    $criteria	->addCondition( "\"$relatedClass\".\"{$relation->foreignKey}\" = {$this->primaryKey}" );
		
		$rlConfig	= array(
		            'type'				=> 'application.extensions.palmerhill.PHList',
					'relatedModel'		=> $relatedClass,
			    	'visible'			=> $visible === null ? !$this->isNewRecord : $visible,
					'listViewConfig'	=> array(
							'elements' => $elements? $elements : self::model($relatedClass)->relatedListViewConfig['elements'],
				    		'criteria'		=> $criteria,
			    		),
		);
		
		//Yii::trace( '$relation: ' . print_r($relation,1));
		//Yii::trace( '$rlConfig: ' . print_r($rlConfig,1));
		return $rlConfig;
	}
	
	public function relatedDropdownConfig( $relationName, $config=array() )
	{
		$md	= $this->getMetaData();
		if( $md->hasRelation($relationName) )
		{
			$relation		= $md->relations[$relationName];
			$relatedClass	= $relation->className;
			//$foreignKey		= $relation->foreignKey;
			$foreignKey		= self::model($relatedClass)->primaryKeyName;
			$criteria		= array(); // used to build the dropdown
			//Yii::trace( '$relation: ' . print_r($relation,1));
		}
		else // two step relation (parent of parent)
		{
			$relatedClass	= $relationName;
			$foreignKey		= self::model($relatedClass)->primaryKeyName;
			//Yii::trace( "relatedClass: $relatedClass, foreignKey: $foreignKey");
			$criteria		= empty( $this->$foreignKey ) ?
				array() : array( 'condition' => "\"$foreignKey\" = " . $this->$foreignKey); // used to build the dropdown
		}
		
		// set drop down to empty if not included in list view fields, to avoid repetitive queries
		if( $this->isListRow && !array_key_exists($foreignKey, $this->listViewConfig['elements']) )
		{
			//Yii::trace("listConfig: " . print_r($this->listViewConfig, 1));
			//Yii::trace("not in listConfig, {$this->modelName}: {$this->primaryKey}, foreignKey: $foreignKey");
			$listModels	= array();
		}
		else
		{
			// called from a list view, set items to the relation model only and skip query
			if($this->isListRow)
				$listModels	= array($this->$relationName);
			else {
				$listModels = self::model($relatedClass)->findAll($criteria);
			}
		}
		
		//Yii::trace("{$this->isListRow} $relationName " . count($listModels));
		//if($relationName == "EmployeePayroll") Yii::trace(print_r($listModels[0],1));
		$items = CHtml::listData($listModels, $foreignKey, 'name');

		$items = $this->jsEscape($items);
		$configArray	= array(
		            'type'		=> 'dropdownlist',
		            'label'		=> $this->generateAttributeLabel($relationName),
		        	'empty'		=> '[Select]',
		        	'items'		=> $items,
		);
		$configArray = array_merge($configArray, $config); // merge passed config
		
		//Yii::trace( '$configArray: ' . print_r($configArray,1));
		return $configArray;
	}
	
	public function setRelatedListViewConfig($value)	{ $this->relatedListViewConfig = $value; }
	public function getRelatedListViewConfig()			{ return $this->listViewConfig;}
	
	public function getCustomCriteria($name, $value, $type='condition') {
    	return null;		
	}
	
	public function isInFilters($readOnly=false) {
		//shortcircuit
		//return false;
		
	    isset(Yii::app()->user->globaluser) ? $globaluser = Yii::app()->user->globaluser : $globaluser = false;
        if($globaluser) { return true; }
        
        if(!$this->primaryKey) return true;

		//Grab the list of available filter types
		$filterTypes = array_keys(Filter::model()->filterTypes);
		
		//Get this user's filters
		$filters = array();
		$filterModels	= User::model()->findByPk(Yii::app()->user->id)->Group->Filters;
		foreach($filterModels as $filter)
		{
			//Yii::trace("filterModels loop, filter: {$filter->primaryKey}");
			if(!$filter->readOnly || $readOnly) { //Skip the record if filter is readonly and function requires write  
				$filters[$filter->type][] = $filter->value;
			}
			else { //make sure the system knows to still place an empty filter for cases where only readonly is present
				$filters[$filter->type][] = null;
			}
		}
		
		$filters['organizationId'][] = Yii::app()->user->organizationId;
		
//		/Yii::trace(print_r($filters, true));
				
		foreach($filterTypes as $filterType) {
			if($filters[$filterType]) {
				if($this->hasAttribute($filterType)) {
					if(!in_array($this->$filterType, $filters[$filterType])) return false;
				}
			}
		}
		
		if($this->hasAttribute('entityId')) {
			$entity = Entity::model()->findByPk($this->entityId);
			if($entity) {
				if(!$entity->isInFilters($readOnly)) return false;
			}
		}		

		return true;
	}
	
	public function isInReadFilters()			{ return $this->isInFilters(true); }
	
	public function getCascadeModels()			{ return null; }	
	
	public function getCloneCascadeModels()		{ return $this->cascadeModels; }	
	
	public function cloneChildren($originalId)	{}

	public function serializeCriteria( $criteria, $noLimit=1 ) {
			$serializedCriteria = new CDbCriteria;
			$serializedCriteria->mergeWith($criteria);
			
			// re-index bind parameters to avoid name collision
			$prefix = ':ycp';
			$newPrefix = "{$prefix}_serialized_";
			foreach( $serializedCriteria->params as $key=>$val )
			{
				$newKey = str_replace($prefix, $newPrefix, $key);
				unset( $serializedCriteria->params[$key] );
				$serializedCriteria->params[$newKey] = $val;
			}
			$serializedCriteria->condition = str_replace($prefix, $newPrefix, $serializedCriteria->condition);
			
			if( $noLimit )
			{
				$serializedCriteria->limit	= -1;
				$serializedCriteria->offset	= -1;				
			}			
			$serializedCriteria = base64_encode( serialize($serializedCriteria) );
			return $serializedCriteria;
	}			
	public function unserializeCriteria( $serializedCriteria ) {
			return unserialize( base64_decode( $serializedCriteria ) );
	}
	
	public function getModel()					{ return $this->_model; }

	public function getModelName()				{ return $this->tableName(); }
	public function getModelsName()				{ return self::pluralize( $this->tableName() ); }

	public function getModelLabel()				{ return $this->generateAttributeLabel($this->modelName); }
	public function getModelsLabel()			{ return $this->generateAttributeLabel($this->modelsName); }
	
	public static function pluralize($name)
	{
	    $rules=array(
	        '/(x|ch|ss|sh|us|as|is|os)$/i' => '\1es',
	        '/(?:([^f])fe|([lr])f)$/i' => '\1\2ves',
	        '/(m)an$/i' => '\1en',
	        '/(child)$/i' => '\1ren',
	        '/(r)y$/i' => '\1ies',
	        '/s$/' => 's',
	    );
	    foreach($rules as $rule=>$replacement)
	    {
	        if(preg_match($rule,$name))
	            return preg_replace($rule,$replacement,$name);
	    }
	    return $name.'s';
	}	
	
	public function getFKeyName()
	{
		return strtolower(substr($this->tableName(),0,1)) . substr($this->tableName(),1) . 'Id';
	} // can use lcfirst() in php 5.3+

	public function getViewUrl() {
		return Yii::app()->createUrl("/$this->modelName/view", array('id'=>$this->primaryKey) );
	}
	public function getShowUrl() { return $this->viewUrl; }
	
	public function getStandardDeleteAction()	{ return 'delete'; }
	
	public function getName()					{ return $this->primaryKey; }
	public function getDefaultSort()			{ return array('name'=> $this->primaryKeyName, 'order'=>'ASC'); }
	public function getDisplayListFooter()		{ return false; }
	
	public function getPrimaryKeyName()			{ return $this->getModelPrimaryKeyName($this->modelName); }
	public function getModelPrimaryKeyName($modelName)
	{
		$key	= "{$modelName}Id";
		$key{0}	= strtolower($key{0});
		return $key; 
	}
	
	public static function getForeignModelName( $foreignKey )
	{
		// dirty hack to hanlde UserAuthItems
		if($foreignKey == 'userid') return 'User';
		
		$labelWords = explode( ' ', CModel::generateAttributeLabel( $foreignKey ) );
		foreach( $labelWords as $i=>$word )
		{
			if( $word == 'Id')
			{
				unset( $labelWords[$i] );
				break;
			}
		}
		return implode( '', $labelWords );
	}
	
	public function getForeignValue( $element )
	{
		$foreignId			= isset( $this->$element )? $this->$element : null;
		$foreignValue		= $foreignId !== null ?
								( isset( $this->form[$element]->items[$foreignId] )?
									$this->form[$element]->items[$foreignId] : null ) : null;
									
		// item is dropdown but not a model, like a list of statuses								
		if( !$this->form[$element]->isModel )
		{
			$fieldValue	= $foreignValue;
		}
		else
		{
			$relationName		= $this->getForeignModelName( $element );
			$md	= $this->getMetaData();
			if( $md->hasRelation($relationName) )
			{
				$relation			= $md->relations[$relationName];
				$foreignModelName	= $relation->className;
				/*
				if( $relation instanceof CHasOneRelation)
					$foreignKey	= $this->getModelPrimaryKeyName($foreignModelName); 
				else
					$foreignKey	= $relation->foreignKey;
					*/
			}
			else
			{
				$foreignModelName	= $relationName;
				//$foreignKey			= $this->getModelPrimaryKeyName($foreignModelName); 
			}
			$foreignKey	= self::model($foreignModelName)->primaryKeyName;
			$fieldValue	= $foreignValue ? CHtml::link( htmlspecialchars($foreignValue), array("$foreignModelName/show"	, $foreignKey => $foreignId ) ) : '';
		}
		return $fieldValue;
	}
	
	public static function getColumnType( $column )
	{
		$labelWords = explode( ' ', $this->generateAttributeLabel( $column->name ) );
		
		if( $column->type == 'boolean' ) $type = 'checkbox';
		elseif( in_array( 'Date', $labelWords) ); // date field
		elseif( in_array( 'Id', $labelWords) ); // foreign key
		else $type = 'text';
		return $type;
	}
	
	// common getters and setters
	public function setListViewConfig($value)		{ $this->listViewConfig		= $value; }
	public function setShowFieldsGrouping($value)	{ $this->showFieldsGrouping	= $value; }
	
	public function setFormConfig($value) 			{ $this->formConfig			= $value; } // getter is defined in each model
	public function setForm( $value )				{ $this->form				= $value; }
	public function getForm( $forceFresh = false )
	{
		if( isset($this->form) && !$forceFresh ) return $this->form;
		
		$this->form = new PHForm( $this->formConfig, $this );
		return $this->form;
	}
	
	public function getActionBarConfig()
	{
			return array(
			'buttons' => array(
		        'create'=>array(
		            'type'		=> 'button',
		        	'submit'	=> array('create'),
		            'label'		=> 'Create',
		        ),
		        
		        'update'=>array(
		            'type'		=> 'button',
		        	'submit'	=> array('update', $this->primaryKeyName => $this->primaryKey),
		            'label'		=> 'Update',
		        ),
		        
		        'clone'=>array(
		            'type'		=> 'button',
		        	'submit'	=> array('clone', $this->primaryKeyName => $this->primaryKey),
		            'label'		=> 'Clone',
		        ),
		        
		        'list'=>array(
		            'type'		=> 'button',
		        	'submit'	=> array('list'),
		            'label'		=> 'List',
		        ),
		        
		        'delete'=>array(
		            'type'		=> 'button',
		        	'submit'	=> array('delete', $this->primaryKeyName => $this->primaryKey),
		        	'confirm'	=> "Are you sure you want to delete the selected {$this->modelsLabel}?",
		            'label'		=> 'Delete',
		        ),
		        
		       ),   
		    );
	}
	
 	// helper functions
	public static function getFilingStatuses()
 	{
 		return array(
 			'M' => 'Married',
 			'S' => 'Single',
 			//'O' => '',
 			//'B' => '',
 		);
 	}
	
	public static function getMaritalStatuses()
 	{
 		return array(
 			'M' => 'Married',
 			'S' => 'Single',
 		);
 	}
	
	public static function getSexes()
 	{
 		return array(
 			'M' => 'Male',
 			'F' => 'Female',
 		);
 	}
	
 	public static function getIsDevServer()
	{
		return ( $_SERVER['SERVER_NAME'] == 'payroll.palmerhill.com' );
	}

	public function getIsPending()
	{
		return false;
	}
	
	public static function getModelsIds( $models )
	{
		$ids	= array();
		foreach($models as $model) $ids[] = $model->primaryKey;
		return $ids;
	}
	
	/*
	public static function getRules()
	{
		if( !isset($this->rules) )
		{
			$rules = self::model( 'Rule' )->findAll( '"baseContext" = :baseContext',
	   			array('baseContext' => $employeePositionPayroll->modelName )
	   		);
		}
		return $this->rules;
	}
	*/
	
	public function runRules()
	{
/*		// stored as static to be shared accross all objects of the same type, and calculated once
		if( self::$rules === null )
		{
			self::$rules = Rule::model()->findAll( '"baseContext" = :baseContext',
	   			array('baseContext' => $this->modelName )
	   		);
		}
		
		$rules	= self::$rules;
		foreach ($rules as $rule)
   		{
   			if( $rule->baseContextId && ($this->primaryKey != $rule->baseContextId) ) continue;
   			if( $rule->executeRule($this) ) continue; // rule pass
   			
   			// rule fail, flag model
   			$ruleError	= new RuleError;
   			$ruleError->baseContext		= $this->modelName;
   			$ruleError->baseContextId	= $this->primaryKey;
   			$ruleError->ruleId			= $rule->primaryKey;
   			// link back to payroll
   			// makes it easier to get errors for all objects under a payroll
   			if( isset($this->payrollId) ) $ruleError->payrollId	= $this->payrollId; 
   			
   			$ruleError->save();
   			//Yii::trace("rule: {$rule->primaryKey}, {$this->modelName}: {$this->primaryKey}");
   		}*/
	}
	
	public function jsEscape($value) {
		if(is_array($value)) {
			foreach($value as $aKey=>$aValue) {
				$value[$aKey] = str_replace(array("'",'"'), '', $aValue);
			}
		}
		return $value;
	}

	public function getFormattedErrors() {
		//dump user back to the home page on error
		$error = '<ul>';
		foreach($this->errors as $attribute=>$errorArray) {
			$error .= '<li>' . $errorArray[0] . '</li>';
		}
		$error .= '</ul>';
	
		return $error;
	}
}
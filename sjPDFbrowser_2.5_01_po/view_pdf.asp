<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">
<title>
  Java PDF Viewer | Silent Print PDF | View PDF from browser | Print PDF from browser | print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>

</head>

<body bgcolor="#FFFFFF" text="#666699"
topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">

<%
   licenseKey = "A8EC-73AF-64A9-0375"
   successPage = Request.QueryString("ON_SUCCESS_SHOW_PAGE")
   successPageTarget = Request.QueryString("ON_SUCCESS_PAGE_TARGET")
   failurePage = Request.QueryString("ON_FAILURE_SHOW_PAGE")
   failurePageTarget = Request.QueryString("ON_FAILURE_PAGE_TARGET")
   serverCallBackUrl = Request.QueryString("SERVER_CALL_BACK_URL")
   isShowPrintPreview = Request.QueryString("IS_SHOW_PRINT_PREVIEW")
   viewerPage = Request.QueryString("VIEWER_PAGE")
   viewerControls = Request.QueryString("VIEWER_CONTROLS")
   docList = Request.QueryString
   docId = Request.QueryString("DOC_ID")
   printerName = Request.QueryString("PRINTER_NAME")
   printer_name_substring_match = Request.QueryString("PRINTER_NAME_SUBSTRING_MATCH")
   paper = Request.QueryString("PAPER")
   copies = Request.QueryString("COPIES")
   jobName = Request.QueryString("JOB_NAME")
   showPrinterDialog = Request.QueryString("SHOW_PRINT_DIALOG")
   autoMatchPaper = Request.QueryString("AUTO_MATCH_PAPER")
   pageScaling = Request.QueryString("PAGE_SCALING")
   autoRotateAndCenter = Request.QueryString("AUTO_ROTATE_AND_CENTER")
   usePrinterMargins = Request.QueryString("IS_USE_PRINTER_MARGINS")
   singlePrintJob = Request.QueryString("SINGLE_PRINT_JOB")
   collateCopies = Request.QueryString("COLLATE_COPIES")
   showErrorDialog = Request.QueryString("SHOW_PRINT_ERROR_DIALOG")
   password= Request.QueryString("PASSWORD")
   urlAuthId = Request.QueryString("URL_AUTH_ID")
   urlAuthPassword = Request.QueryString("URL_AUTH_PASSWORD")
   printQuality = Request.QueryString("PRINT_QUALITY")
   side = Request.QueryString("SIDE_TO_PRINT")
   enablePrintStatusUpdate = Request.QueryString("STATUS_UPDATE_ENABLED")
   debug = Request.QueryString("DEBUG")
%>

<OBJECT
  codeBase="http://java.sun.com/products/plugin/autodl/jinstall-1_4_2_06-windows-i586.cab#Version=1,4,2,06"
  width="100%" height="100%" classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93">
  <PARAM NAME="type" VALUE="application/x-java-applet;version=1.4.2">
  <PARAM NAME="name" VALUE="smartj">
  <PARAM NAME="alt" VALUE="PDF - Print and View from browser">
  <PARAM NAME="CODEBASE" VALUE="browser_lib/">
  <PARAM NAME="CODE" VALUE="com.activetree.pdfprint.WebPdfViewer">
  <PARAM NAME="ARCHIVE" VALUE="">
  <PARAM NAME="cache_option" VALUE="Plugin">
  <PARAM NAME="cache_archive" VALUE="bc.jar,jai_codec.jar,jai_core.jar,jai_imageio.jar,sjpdf_license.jar,sjpdf_browser.jar">
  <PARAM NAME="LICENSE_KEY" VALUE="<%=licenseKey%>">
  <PARAM NAME="DOC_LIST" VALUE="<%=docList%>">
  <PARAM NAME="DOC_ID" VALUE="<%=docId%>">
  <PARAM NAME="PAGE_SCALING" VALUE="<%=pageScaling%>">
  <PARAM NAME="AUTO_ROTATE_AND_CENTER" VALUE="<%=autoRotateAndCenter%>">
  <PARAM NAME="AUTO_MATCH_PAPER" VALUE="<%=autoMatchPaper%>">
  <PARAM NAME="IS_USE_PRINTER_MARGINS" VALUE="<%=usePrinterMargins%>">
  <PARAM NAME="PRINTER_NAME" VALUE="<%=printerName%>">
  <PARAM NAME="PRINTER_NAME_SUBSTRING_MATCH" VALUE="<%=printer_name_substring_match%>">
  <PARAM NAME="PAPER" VALUE="<%=paper%>">
  <PARAM NAME="COPIES" VALUE="<%=copies%>">
  <PARAM NAME="COLLATE_COPIES" VALUE="<%=collateCopies%>">
  <PARAM NAME="JOB_NAME" VALUE="<%=jobName%>">
  <PARAM NAME="SHOW_PRINT_DIALOG" VALUE="<%=showPrinterDialog%>">
  <PARAM NAME="SINGLE_PRINT_JOB" VALUE="<%=singlePrintJob%>">
  <PARAM NAME="SHOW_PRINT_ERROR_DIALOG" VALUE="<%=showErrorDialog%>">
  <PARAM NAME="PASSWORD" VALUE="<%=password%>">
  <PARAM NAME="URL_AUTH_ID" VALUE="<%=urlAuthId%>">
  <PARAM NAME="URL_AUTH_PASSWORD" VALUE="<%=urlAuthPassword%>">
  <PARAM NAME="PRINT_QUALITY" VALUE="<%=printQuality%>">
  <PARAM NAME="SIDE_TO_PRINT" VALUE="<%=side%>">
  <PARAM NAME="STATUS_UPDATE_ENABLED" VALUE="<%=enablePrintStatusUpdate%>">
  <PARAM NAME="ON_SUCCESS_SHOW_PAGE" VALUE="<%=successPage%>">
  <PARAM NAME="ON_SUCCESS_PAGE_TARGET" VALUE="<%=successPageTarget%>">
  <PARAM NAME="ON_FAILURE_SHOW_PAGE" VALUE="<%=failurePage%>">
  <PARAM NAME="ON_FAILURE_PAGE_TARGET" VALUE="<%=failurePageTarget%>">
  <PARAM NAME="SERVER_CALL_BACK_URL" VALUE="<%=serverCallBackUrl%>">
  <PARAM NAME="IS_SHOW_PRINT_PREVIEW" VALUE="<%=isShowPrintPreview%>">
  <PARAM NAME="VIEWER_PAGE" VALUE="<%=viewerPage%>">
  <PARAM NAME="VIEWER_CONTROLS" VALUE="<%=viewerControls%>">
  <PARAM NAME="DEBUG" VALUE="<%=debug%>">

  <COMMENT>
    <EMBED
      type="application/x-java-applet;version=1.4.2"
      name="Java PDF Print"
      alt="PDF - Print and View PDF from browser"
      pluginspage="http://java.sun.com/j2se/"
      CODEBASE="browser_lib/"
      CODE="com.activetree.pdfprint.WebPdfViewer"
      ARCHIVE=""
      cache_option="Plugin"
      cache_archive="bc.jar,jai_codec.jar,jai_core.jar,jai_imageio.jar,sjpdf_license.jar,sjpdf_browser.jar"
      WIDTH="100%"
      HEIGHT="100%"
      LICENSE_KEY="<%=licenseKey%>"
      DOC_LIST="<%=docList%>"
      DOC_ID="<%=docId%>"
      PAGE_SCALING="<%=pageScaling%>"
      AUTO_ROTATE_AND_CENTER="<%=autoRotateAndCenter%>"
      AUTO_MATCH_PAPER="<%=autoMatchPaper%>"
      IS_USE_PRINTER_MARGINS="<%=usePrinterMargins%>"
      PRINTER_NAME="<%=printerName%>"
      PRINTER_NAME_SUBSTRING_MATCH="<%=printer_name_substring_match%>"
      PAPER="<%=paper%>"
      COPIES="<%=copies%>"
      COLLATE_COPIES="<%=collateCopies%>"
      JOB_NAME="<%=jobName%>"
      SHOW_PRINT_DIALOG="<%=showPrinterDialog%>"
      SINGLE_PRINT_JOB="<%=singlePrintJob%>"
      SHOW_PRINT_ERROR_DIALOG="<%=showErrorDialog%>"
      PASSWORD="<%=password%>"
      URL_AUTH_ID="<%=urlAuthId%>"
      URL_AUTH_PASSWORD="<%=urlAuthPassword%>"
      PRINT_QUALITY="<%=printQuality%>"
      SIDE_TO_PRINT="<%=side%>"
      STATUS_UPDATE_ENABLED="<%= enablePrintStatusUpdate %>"
      ON_SUCCESS_SHOW_PAGE="<%= successPage %>"
      ON_SUCCESS_PAGE_TARGET="<%= successPageTarget %>"
      ON_FAILURE_SHOW_PAGE="<%= failurePage %>"
      ON_FAILURE_PAGE_TARGET="<%= failurePageTarget %>"
      SERVER_CALL_BACK_URL="<%= serverCallBackUrl %>"
      IS_SHOW_PRINT_PREVIEW="<%= isShowPrintPreview %>"
      VIEWER_PAGE="<%= viewerPage %>"
      VIEWER_CONTROLS="<%= viewerControls %>"
      DEBUG="<%=debug%>"
      >

      <NOEMBED>
        <p>Not java runtime.</p>
      </NOEMBED>
    </EMBED>
  </COMMENT>
</OBJECT>

</body>
</html>

<%@ page import="java.io.IOException" %>
<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">

<style type="text/css">
  body {
    margin-top: 5px;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 5px;
  }
</style>
<title>
  Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>


<script type="text/javascript" language="javascript">

 function showMessage(msg) {
   if (msg=='') {
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
     return;
   }
   if (document.createTextNode){
     var mytext=document.createTextNode(msg+"\r");
     var dest = document.getElementById("status");
     if (dest) {
       dest.appendChild(mytext);
     }
   } else{
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
   }
}

function printWithParams(params) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    print_pane.innerHTML=output;
  }
}
  
function printMultipleWithParams(params, count) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    var newdiv = document.createElement('div');
    var newdiv_id = 'div_'+count;
    newdiv.innerHTML = output;
    print_pane.appendChild(newdiv);
  }
}

</script>


</head>
<BODY link="#009900" vlink="#009900" TEXT="#4b73af" >

<%
  String codeBase = getCodeBaseURLString(request);
%>

<%!
  String getCodeBaseURLString(HttpServletRequest req) throws IOException {
    String contextPath = req.getContextPath();  //e.g. "/sample_app"
    String protocol = req.getProtocol();
    String scheme = req.getScheme();
    if (scheme != null && scheme.length() > 0) {
      protocol = scheme;
    }
    if (protocol != null && protocol.equalsIgnoreCase("https")) {
      protocol = "https";
    } else if (protocol != null && protocol.equalsIgnoreCase("http")) {
      protocol = "http";
    } else {
      protocol = "http";
    }
    String host = req.getServerName();
    String port = ":" + req.getServerPort();
    if (port.equalsIgnoreCase(":80")) {
      port = ""; //need not show port 80
    }
    String baseUrlStr = protocol + "://" + host + port + contextPath; // + "/";
    return baseUrlStr;
  }
%>

<table  border="0" width="800" cellpadding="0" cellspacing="0">
<tr> <td>

<table border="0" width="100%" cellpadding="0" cellspacing="0">
  <tr bgcolor="white">
    <td >
      <div class=square>
      <h2><font color=red>Secure Printing of your PDF!</font></h2>
      <p>
      Silent print and view existing PDF documents from web browser, standalone
         and Java/J2EE applications with complete security.
      </p>
 <p>
    <li>Secure silent print your PDF documents.</li>
   <li>While printing and viewing from a browser page it is normal practice to
    have a visible URL pointing to a PDF or an URL that dynamically generates it. </li>
   <li>If you have to silent print
    your PDF you can avoid
    provide the actual document URL and instead can type a secret document ID. </li>
   <li>You may not provide the document
    password as part of the URL. If your document URL requires user authentication you may not provide the auth ID and password
    as part of the URL. </li>
   <li>In addition, what if you want to control the documents your user is printing, such as
    number of copies, number of times, date and time when printed, printer printed, and more. </li>
   <li>Do you want to track the users usage of the documents? </li>

   <p>
    It can not only secure your document's actual identity and security information in advance, you can completely control
    the document printing and viewing. Your controlling program in a server location will be
    called with all information about the document user is attempting to print or view.
     If may decide at run time whether or not you will
    allow such access based on the history of users usage of such documents. If you do allow more access
     then provide all the document access security information e.g. password, url auth id/password
     enabling further access or simply deny it by setting status to cancel.  This example is using a
     sample JSP server program called <strong>server.jsp</strong> as part of this archive. Your server be
     of any program such as a C/C++, PHP, JSP/Servlet, ASP and other scripts or programs.
   </p>
</p>
    </div>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <tr>
    <td><fieldset>
         <legend>Secure Printing</legend>

    <form name="secure" action="javascript:" method="GET" target="">
      <!--
      <li>Secret Document 1
          <input name="f1" type="text"
                 value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample2.pdf"
                 size="85">
        (<a class="bottomlinks1" onMouseDown="javascript:document.secure.currdoc.value=document.secure.f1.value;"
        href="javascript:printDoc('<%=codeBase%>/silent_print_pdf.jsp?','&STATUS_UPDATE_ENABLED=true&ON_SUCCESS_SHOW_PAGE=<%=codeBase%>/demo/success.jsp&ON_SUCCESS_PAGE_TARGET=_blank&ON_FAILURE_SHOW_PAGE=<%=codeBase%>/demo/failure.jsp&ON_FAILURE_PAGE_TARGET=_blank&SERVER_CALL_BACK_URL=<%=codeBase%>/demo/server.jsp');">
        Silent Print</a>)
      </li>
      -->

      <li>Secret Document  (
        <a class="bottomlinks1"
            href="javascript:"
            onClick="printWithParams('<%=codeBase%>/silent_print_pdf.jsp?DOC_ID=demoId&STATUS_UPDATE_ENABLED=true&ON_FAILURE_SHOW_PAGE=<%=codeBase%>/demo/failure.jsp&ON_FAILURE_PAGE_TARGET=_blank&SERVER_CALL_BACK_URL=<%=codeBase%>/demo/server.jsp'); return false;">
        Silent Print</a>)
      </li>

      <!--
      <li>Secret Document 2 (
        <a class="bottomlinks1"
            href="javascript:"
            onClick="window.open('<%=codeBase%>/view_pdf.jsp?DOC_ID=demoId&ON_FAILURE_SHOW_PAGE=<%=codeBase%>/demo/failure.jsp&ON_FAILURE_PAGE_TARGET=_blank&SERVER_CALL_BACK_URL=<%=codeBase%>/demo/server.jsp&VIEWER_CONTROLS=SILENT_PRINT_BUTTON, SILENT_PRINT_BUTTON, FIRST_PAGE_BUTTON, NEXT_PAGE_BUTTON, PREVIOUS_PAGE_BUTTON, LAST_PAGE_BUTTON, GO_TO_A_PAGE_FIELD, TOTAL_PAGE_LABEL','','width=880,height=650,resizable=yes,scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no'); return false;">
        Preview & Silent Print</a>)
      </li>
      -->

      <input name="currdoc" type="hidden" value="">
    </form>

</fieldset>
</td>

  </tr>


  <tr>
    <td >
      <fieldset>
        <legend>Print status </legend>
        <textarea id="status" rows="6" cols="96"  readonly="true"></textarea>
      </fieldset>
    </td>
  </tr>

<tr width="100%">
  <td width="100%" >
    <label id="print_pane"></label>
  </td>
</tr>

</table>

</td>
</tr>

<tr>
  <td>
    <hr size="0.5">
      Copyright &#169; 2008 Activetree, Inc. All rights reserved. <br>
      Web: <a href="http://www.activetree.com" class="bottomlinks1">http://www.activetree.com</a><br>
      Email: sales@activetree.com<br>
      Tel: +1 408 716 8414 Fax: +1 408 716 8450<br>
  </td>
</tr>

</table>

</body>
</html>

<%@ page import="java.io.IOException" %>
<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">

<style type="text/css">
  body {
    margin-top: 5px;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 5px;
  }
</style>
<title>
  Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>

<script type="text/javascript" language="javascript">

 function showMessage(msg) {
   if (msg=='') {
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
     return;
   }
   if (document.createTextNode){
     var mytext=document.createTextNode(msg+"\r");
     var dest = document.getElementById("status");
     if (dest) {
       dest.appendChild(mytext);
     }
   } else{
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
   }
}

function printWithParams(params) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    print_pane.innerHTML=output;
  }
}

function printMultipleWithParams(params, count) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    var newdiv = document.createElement('div');
    var newdiv_id = 'div_'+count;
    newdiv.innerHTML = output;
    print_pane.appendChild(newdiv);
  }
}


function printFromForm() {
  var code_base=document.PRINT_PARAMS_FORM.code_base.value;
  var successpage='null'; //document.PRINT_PARAMS_FORM.successpage.value;
  var show_printer_dialog=document.PRINT_PARAMS_FORM.SHOW_PRINT_DIALOG.checked;
  //Doc1
  var doc1=document.PRINT_PARAMS_FORM.doc1.value;
  var url1 = code_base+'silent_print_pdf.jsp' + '?DOC_LIST=[' + doc1 + ']&SHOW_PRINT_DIALOG=' + show_printer_dialog + '&PRINT_DIALOG_TITLE='+ dialogTitle(doc1) +'&ON_SUCCESS_SHOW_PAGE='+ successpage;
  printMultipleWithParams(url1, 1);
  //Doc-2
  var doc2=document.PRINT_PARAMS_FORM.doc2.value;
  var url2 = code_base+'silent_print_pdf.jsp' + '?DOC_LIST=[' + doc2 + ']&SHOW_PRINT_DIALOG=' + show_printer_dialog + '&PRINT_DIALOG_TITLE='+ dialogTitle(doc2) +'&ON_SUCCESS_SHOW_PAGE='+ successpage;
  printMultipleWithParams(url2, 2);
  //Doc-3
  var doc3=document.PRINT_PARAMS_FORM.doc3.value;
  var url3 = code_base+'silent_print_pdf.jsp' + '?DOC_LIST=[' + doc3 + ']&SHOW_PRINT_DIALOG=' + show_printer_dialog + '&PRINT_DIALOG_TITLE='+ dialogTitle(doc3) +'&ON_SUCCESS_SHOW_PAGE='+ successpage;
  printMultipleWithParams(url3, 3);
  //Doc-4
  var doc4=document.PRINT_PARAMS_FORM.doc4.value;
  var url4 = code_base+'silent_print_pdf.jsp' + '?DOC_LIST=[' + doc4 + ']&SHOW_PRINT_DIALOG=' + show_printer_dialog + '&PRINT_DIALOG_TITLE='+ dialogTitle(doc4) +'&ON_SUCCESS_SHOW_PAGE='+ successpage;
  //alert('params='+params);
  printMultipleWithParams(url4, 4);
}

function dialogTitle(doc) {
  var lastIdx = doc.lastIndexOf('/');
  if (lastIdx >= 0) {
    return doc.substring(lastIdx+1);
  }
  return doc;
}

function viewDocuments() {
  var title = '';
  var window_params = 'width=880,height=650,resizable=yes,scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no';
  var code_base=document.PRINT_PARAMS_FORM.code_base.value;

  var doc1=document.PRINT_PARAMS_FORM.doc1.value;
  var url1 = code_base+'view_pdf.jsp?DOCUMENT=[' + doc1 +']';
  window.open(url1, title,window_params);

  var doc2=document.PRINT_PARAMS_FORM.doc2.value;
  var url2 = code_base+'view_pdf.jsp?DOCUMENT=[' + doc2 + ']';
  window.open(url2, title,window_params);

  var doc3=document.PRINT_PARAMS_FORM.doc3.value;
  var url3 = code_base+'view_pdf.jsp?DOCUMENT=[' + doc3 +']';
  window.open(url3, title,window_params);

  var doc4=document.PRINT_PARAMS_FORM.doc4.value;
  var url4 = code_base+'view_pdf.jsp?DOCUMENT=[' + doc4 +']';
  window.open(url4, title,window_params);
}
</script>

</head>
<BODY link="#009900" vlink="#009900" TEXT="#4b73af" >

<%
  String codeBase = getCodeBaseURLString(request);
%>

<%!
  String getCodeBaseURLString(HttpServletRequest req) throws IOException {
    String contextPath = req.getContextPath();  //e.g. "/sample_app"
    String protocol = req.getProtocol();
    String scheme = req.getScheme();
    if (scheme != null && scheme.length() > 0) {
      protocol = scheme;
    }
    if (protocol != null && protocol.equalsIgnoreCase("https")) {
      protocol = "https";
    } else if (protocol != null && protocol.equalsIgnoreCase("http")) {
      protocol = "http";
    } else {
      protocol = "http";
    }
    String host = req.getServerName();
    String port = ":" + req.getServerPort();
    if (port.equalsIgnoreCase(":80")) {
      port = ""; //need not show port 80
    }
    String baseUrlStr = protocol + "://" + host + port + contextPath + "/";
    return baseUrlStr;
  }
%>


<table  border="0" width="800" cellpadding="0" cellspacing="0">
<tr> <td>

<table border="0" width="100%" cellpadding="0" cellspacing="0">
  <tr bgcolor="white">
    <td >
      <h2>Java PDF Silent Print and PDF Viewer</h2>
      Silent print and view existing PDF documents from web browser, standalone
         and Java/J2EE applications.
         <p>
This demo shows how to print multiple PDFs at a time silently (or with print dialog). 
Multple PDFs may be viewed each in a new browser window.
</p>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <tr>
    <td><fieldset>
         <legend>Example: Printing multiple PDFs with (and without print dialog)</legend>
<p>Here you can enter an URL of your choice (local or remote URL or a dynamic PDF generator URL) </p>
<form name="PRINT_PARAMS_FORM" action="<%=codeBase%>silent_print_pdf.jsp" method="GET" target="print_message_frame">
<li>
  <input type="text" name="doc1" value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample1.pdf" size="85">
</li>
<li>
  <input type="text" name="doc2" value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample2.pdf" size="85">
</li>
<li>
  <input type="text" name="doc3" value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample3.pdf" size="85">
</li>
<li>
  <input type="text" name="doc4" value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample4.pdf" size="85">
</li>
  <p>
  <input type="hidden" name="code_base" value="<%=codeBase%>">
  <input type="hidden" name="successpage" value="<%=codeBase%>/demo/success.jsp">
  <input type="checkbox" name="SHOW_PRINT_DIALOG" checked> Show printer dialog?
  </p>
</form>
</fieldset>
</td>
  </tr>

<tr>
  <td colspan="2" height="30">
    <input type="button" name="printButton" value="Silent Print PDF(s)" onClick="printFromForm();">
    <input type="button" name="printButton" value="View PDF(s)" onClick="viewDocuments(); return false;">
  </td>
</tr>


  <tr>
    <td >
      <fieldset>
        <legend>Print status </legend>
        <textarea id="status" rows="6" cols="96"  readonly="true"></textarea>
      </fieldset>
    </td>
  </tr>


  <tr width="100%">
    <td width="100%" >
      <label id="print_pane"></label>
    </td>
  </tr>

</table>

</td>
</tr>

<tr>
  <td>
    <hr size="0.5">
      Copyright &#169; 2008 Activetree, Inc. All rights reserved. <br>
      Web: <a href="http://www.activetree.com" class="bottomlinks1">http://www.activetree.com</a><br>
      Email: sales@activetree.com<br>
      Tel: +1 408 716 8414 Fax: +1 408 716 8450<br>
  </td>
</tr>

</table>

</body>
</html>

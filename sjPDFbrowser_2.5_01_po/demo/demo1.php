<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">

<style type="text/css">
  body {
    margin-top: 5px;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 5px;
  }
</style>
<title>
  Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>

<script language="javascript">

 function showMessage(msg) {
   if (msg=='') {
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
     return;
   }
   if (document.createTextNode){
     var mytext=document.createTextNode(msg+"\r");
     var dest = document.getElementById("status");
     if (dest) {
       dest.appendChild(mytext);
     }
   } else{
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
   }
}

function printWithParams(params) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    print_pane.innerHTML=output;
  }
}

function printMultipleWithParams(params, count) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    var newdiv = document.createElement('div');
    var newdiv_id = 'div_'+count;
    newdiv.innerHTML = output;
    print_pane.appendChild(newdiv);
  }
}

function printFromForm() {
  var code_base=document.PRINT_PARAMS_FORM.code_base.value;
  //alert('code_base='+code_base);

  //collect all the user choices
  var doc_url=document.PRINT_PARAMS_FORM.DOC_URL.value;
  //alert('doc_url='+doc_url);

  var printer_name=document.PRINT_PARAMS_FORM.PRINTER_NAME.value;
  //alert('printer_name='+printer_name);

  var paper=document.PRINT_PARAMS_FORM.PAPER.value;
  //alert('papers='+papers);

  var auto_match_paper=document.PRINT_PARAMS_FORM.AUTO_MATCH_PAPER.value;
  //alert('auto_match_paper='+auto_match_paper);

  var printer_name_substring_match=document.PRINT_PARAMS_FORM.PRINTER_NAME_SUBSTRING_MATCH.value;
  //alert('printer_name_substring_match='+printer_name_substring_match);

  var copies=document.PRINT_PARAMS_FORM.COPIES.value;
  //alert('copies='+copies);

  var show_printer_dialog=document.PRINT_PARAMS_FORM.SHOW_PRINT_DIALOG.value;
  //alert('show_printer_dialog='+show_printer_dialog);

  var job_name=document.PRINT_PARAMS_FORM.JOB_NAME.value;
  //alert('job_name='+job_name);

  var pageScaling=document.PRINT_PARAMS_FORM.PAGE_SCALING.value;
  //alert('pageScaling='+pageScaling);

  var autoRotateAndCenter=document.PRINT_PARAMS_FORM.AUTO_ROTATE_AND_CENTER.value;
  //alert('autoRotateAndCenter='+autoRotateAndCenter);

  var is_use_printer_margins=document.PRINT_PARAMS_FORM.IS_USE_PRINTER_MARGINS.value;
  //alert('is_use_printer_margins='+is_use_printer_margins);

  var single_print_job=document.PRINT_PARAMS_FORM.SINGLE_PRINT_JOB.value;
  //alert('single_print_job='+single_print_job);

  var collate_copies=document.PRINT_PARAMS_FORM.COLLATE_COPIES.value;
  //alert('collate_copies='+collate_copies);

  var show_print_error_message=document.PRINT_PARAMS_FORM.SHOW_PRINT_ERROR_DIALOG.value;
  //alert('show_print_error_message='+show_print_error_message);

  var is_debug=document.PRINT_PARAMS_FORM.DEBUG.value;
  //alert('is_debug='+is_debug);

  var password=document.PRINT_PARAMS_FORM.PASSWORD.value;
  //alert('password='+password);

  var printQuality=document.PRINT_PARAMS_FORM.PRINT_QUALITY.value;
  //alert('printQuality='+printQuality);

  var side=document.PRINT_PARAMS_FORM.SIDE_TO_PRINT.value;
  //alert('sideToPrint='+side);

  var statusUpdateEnabled=document.PRINT_PARAMS_FORM.STATUS_UPDATE_ENABLED.value;
  //alert('statusUpdateEnabled='+statusUpdateEnabled);

  //Collect all
  var params = code_base+'silent_print_pdf.php' +
               '?DOC_LIST=' + doc_url +
               '&PRINTER_NAME=' + printer_name +
               '&PAPER=' + paper +
               '&AUTO_MATCH_PAPER=' + auto_match_paper +
               '&PRINTER_NAME_SUBSTRING_MATCH=' + printer_name_substring_match +
               '&COPIES=' + copies +
               '&SHOW_PRINT_DIALOG=' + show_printer_dialog +
               '&JOB_NAME=' + job_name +
               '&PAGE_SCALING=' + pageScaling +
               '&AUTO_ROTATE_AND_CENTER=' + autoRotateAndCenter +
               '&IS_USE_PRINTER_MARGINS=' + is_use_printer_margins +
               '&SINGLE_PRINT_JOB=' + single_print_job +
               '&COLLATE_COPIES=' + collate_copies +
               '&SHOW_PRINT_ERROR_DIALOG=' + show_print_error_message +
               '&DEBUG=' + is_debug +
               '&PASSWORD=' + password +
               '&PRINT_QUALITY=' + printQuality +
               '&SIDE_TO_PRINT=' + side +
               "&STATUS_UPDATE_ENABLED=" + statusUpdateEnabled;
  //alert('params='+params);

  printWithParams(params);
}

var NS4 = document.layerX;
function checkEnter(event) {
  var code = 0;
  if (NS4)
    code = event.which;
  else
    code = event.keyCode;
  if (code==13)
    printFromForm();
}

function viewDocument() {
  var code_base=document.PRINT_PARAMS_FORM.code_base.value;
  var doc_url=document.PRINT_PARAMS_FORM.DOC_URL.value;
  var viewer_url = code_base+'view_pdf.jsp?DOCUMENT=[' + doc_url + ']';
  var title = '';
  var window_params = 'width=880,height=650,resizable=yes,scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no';
  window.open(viewer_url, title,window_params);
}
</script>

</head>
<BODY link="#009900" vlink="#009900" TEXT="#4b73af" >

<?php
$params="&JOB_NAME=PDFPrintJob";
$params.="&PAGE_SCALING=FIT_TO_PRINTABLE_AREA&AUTO_ROTATE_AND_CENTER=true";
$params.="&SHOW_PRINT_ERROR_DIALOG=false&AUTO_MATCH_PAPER=true&DEBUG=true";
$params.="&PRINTER_NAME=&PRINTER_NAME_SUBSTRING_MATCH=false&PAPER=&COPIES=1&SHOW_PRINT_DIALOG=false";
$params.="&IS_USE_PRINTER_MARGINS=true&SINGLE_PRINT_JOB=true&PASSWORD=&COLLATE_COPIES=false";
$params.="&PRINT_QUALITY=HIGH&SIDE_TO_PRINT=ONE_SIDED&STATUS_UPDATE_ENABLED=true";
?>



<table border="0" width="800" cellpadding="0" cellspacing="0">
  <tr bgcolor="white">
    <td >
      <font face="" size="4"><b>Java PDF Print and Viewer</b></font>
       - Silent print and view existing PDF documents from web browser, standalone
         and Java/J2EE applications.
         <p>
This web demo shows how your web application can print and view a PDF document from an URL
(existing PDF or dynamically generated). URL protocol can be any one of the known protocols
such as FILE, HTTP and HTTPS.
</p>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <tr>
    <td><fieldset>
         <legend>Enter/Click PDF Urls</legend>

<p><b>Example-1</b></p>
<li>
<a href="javascript:printWithParams('?DOC_LIST=[http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample1.pdf]<?php echo "${params}"; ?>');"
   class="bottomlinks1">
  Print - US IRS 1040 (Tax form)</a> or
  (<a class="bottomlinks1" href="javascript:"
      onClick="window.open('view_pdf.php?DOCUMENT=http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample1.pdf','','width=880,height=650,resizable=yes,scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no'); return false;">View it</a>)
</li>

<p><b>Example-2</b></p>
Here you can enter an URL of your choice (e.g. URL pointing to an existing PDF or a server program that generates a PDF dynamically)
(Examples: Web URL: http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample3.pdf or a local file URL (Windows): file:/c:/pdf/sample.pdf)
<form name="DOC_PROPERTIES_FORM" action="silent_print_pdf.php" method="GET" target="print_message_frame">
<li>
  <a href="javascript:printFromForm();" class="bottomlinks1">Silent Print</a>
  <input type="text" name="DOC_URL" value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample2.pdf" size="70"
                 onKeyPress="checkEnter(event)">
  (<a class="bottomlinks1" href="javascript:" onClick="viewDocument(); return false;">View it</a>)
   <input type="hidden" name="JOB_NAME" value="PDFPrintJob">
   <input type="hidden" name="PAGE_SCALING" value="FIT_TO_PRINTABLE_AREA">
   <input type="hidden" name="AUTO_ROTATE_AND_CENTER" value="true">
   <input type="hidden" name="SHOW_PRINT_ERROR_DIALOG" value="false">
   <input type="hidden" name="AUTO_MATCH_PAPER" value="true">
   <input type="hidden" name="PRINTER_NAME" value="">
   <input type="hidden" name="PRINTER_NAME_SUBSTRING_MATCH" value="false">
   <input type="hidden" name="PAPER" value="">
   <!-- Example of PAPER parameter: "(topMargin, leftMargin, bottomMargin, rightMargin, totalPaperWidth, totalPaperHeight)"
   e.g. "(0, 0, 0, 0, 612, 792)"  where topMargin=0, leftMargin=0, bottomMargin=0, rightMargin=0, totalPaperWidth=612, totalPaperHeight=792
   No value will cause it to print using locale specific default paper size e.g. NA_LETTER for USA, ISO_A4 for Europe etc.
   -->
   <input type="hidden" name="COPIES" value="1">
   <input type="hidden" name="COLLATE_COPIES" value="false">
   <input type="hidden" name="SHOW_PRINT_DIALOG" value="false">
   <input type="hidden" name="IS_USE_PRINTER_MARGINS" value="true">
   <input type="hidden" name="SINGLE_PRINT_JOB" value="true">
   <input type="hidden" name="PASSWORD" value="">
   <input type="hidden" name="PRINT_QUALITY" value="HIGH">
   <input type="hidden" name="SIDE_TO_PRINT" value="ONE_SIDED">
   <input type="hidden" name="STATUS_UPDATE_ENABLED" value="true">
   <input type="hidden" name="DEBUG" value="true">
</li>
</form>
</fieldset>
</td>
  </tr>

  <tr>
    <td >
      <fieldset>
        <legend>Print status </legend>
        <textarea id="status" rows="6" cols="96"  readonly="true"></textarea>
      </fieldset>
    </td>
  </tr>

  
  <tr width="100%">
    <td width="100%" >
      <label id="print_pane"></label>
    </td>
  </tr>

<tr>
  <td>
    <hr size="0.5">
      Copyright &#169; 2008 Activetree, Inc. All rights reserved. <br>
      Web: <a href="http://www.activetree.com" class="bottomlinks1">http://www.activetree.com</a><br>
      Email: sales@activetree.com<br>
      Tel: +1 408 716 8414 Fax: +1 408 716 8450<br>
  </td>
</tr>

</table>


</body>
</html>

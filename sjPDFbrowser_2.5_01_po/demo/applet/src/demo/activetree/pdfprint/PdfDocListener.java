/**
 * Copyright � Activetree, Inc.  All rights reserved.
 * http://www.activetree.com
 *
 * This is a sample program demonstrating the usage of the relevant APIs and carries no warranty.
 */
package demo.activetree.pdfprint;

import com.activetree.common.doc.DocEvent;
import com.activetree.common.doc.DefaultDocListener;
import com.activetree.common.doc.DocPageable;
import com.activetree.common.web.WebDoc;
import com.activetree.common.page.AbstractPage;
import com.activetree.common.attr.reader.CommonAttributeReader;

import java.util.Date;
import java.awt.print.Pageable;
import java.awt.print.Book;
import java.awt.print.PageFormat;

/**
 * Demo PDF listener that tracks about PDF loading, viewing and printing.
 */
public class PdfDocListener extends DefaultDocListener {
  private Object jobId = null;
  private long startTime = -1;
  protected int totalPages = 0;

  protected void jobStarted(DocEvent evt, Object docList) {
    Object ctx = evt.getWebDoc();
    Object jobId = "";
    if (ctx instanceof CommonAttributeReader) {
      CommonAttributeReader webDoc = (CommonAttributeReader)  ctx;
      jobId = webDoc.getAttribute(WebDoc.JOB_ID);
    }
    this.jobId = jobId;
    System.out.println("JOB_STARTED (BEGIN jobID:" + jobId + ", jobName:" + evt.getDetails() + ", DOC_LIST " + docList + ")");
    startTime = new Date().getTime();
  }

  protected void filterPageable(DocEvent evt, Object docList) {
    int evtType = evt.getType();
    Object aDoc = evt.getSource();
    int pageCount = evt.getValue();
    Book docPages = (Book) evt.getDetails();

//    //TODO -- here is an example of how to filter our pages and process selected page
//    //todo -- filter out those pages -- if not needed
//    //Since you are filtering out some pages -- recount the pages you are processing
//    if (docPages != null && docPages.getNumberOfPages()> 0) {
//      //todo: trim first page -- example -- remove later
//      DocPageable newPages = new DocPageable();
//      for (int page=0; page < docPages.getNumberOfPages(); page++) {
//        //put your filtering criteria; e.g. page index etc.; e.g. alternative pages.
//        //Example: let's take odd pages
//        AbstractPage aPage = (AbstractPage) docPages.getPrintable(page);
//        PageFormat aPageFormat = docPages.getPageFormat(page);
//        int rem = page%2;
//        if (rem != 1) {
//          newPages.append(aPage, aPageFormat);
//        }
//      }
//      //set it back to the event
//      evt.setDetails(newPages);
//      //recount the pages here.
//      System.out.println("DOC PAGEABLE (filted out " + (pageCount - newPages.getNumberOfPages()) + " of " + pageCount + " pages [doc=" + aDoc +"])");
//      pageCount = newPages.getNumberOfPages();
//    }
//    //TODO -end example

    totalPages += pageCount;
  }

  protected void pageRenderedForViewer(DocEvent evt, int pageIndex) {
    System.out.println("PAGE RENDERED (for viewing) index of page:" + pageIndex);
  }

  protected void pageRenderedForThumbnailView(DocEvent evt, int thumbnailIndex) {
    System.out.println("PAGE RENDERED (for thumbnail loading) index of page:" + thumbnailIndex);
  }

  protected void pageLoaded(DocEvent evt, int pageIndex) {
    System.out.println("PAGE LOADED (index of page):" + pageIndex);
  }

  protected void pageCount(DocEvent evt, int pageCount) {
    System.out.println(evt.getSource());
    System.out.println("PAGE COUNT: " + pageCount);
  }

  protected void pageConverted(DocEvent evt, int pageCount) {
    Object info = evt.getInfo();
    System.out.println("PAGE RENDERED (for convertion to " + info + ") index " + evt.getValue() + ", page# " + pageCount);
  }

  protected void printStarting(DocEvent evt, Object source, String printerName) {
    System.out.println("Printing STARTING (pages=" + evt.getValue() + ", printerName=" + printerName + ")");
  }

  protected void printableCalled(DocEvent evt, int pageIndex) {
    //todo: when a getPrintable().print(Graphics g) - called by JRE
    System.out.println("Page printed (index of page):" + pageIndex);
  }

  protected void pagePrinted(DocEvent evt, int pageCount) {
    System.out.println("PAGE RENDERED (for printing) index " + evt.getValue() + ", page# " + pageCount);
  }

  protected void printFinished(DocEvent evt, Object source, String printerName) {
    System.out.println("Printing DONE (pages=" + evt.getValue() + ", printerName=" + printerName + ")");
  }

  //already added the failedDoc to the list.
  protected void docError(DocEvent evt, Object failedDoc, int value, Object details) {
    System.out.println("DOC_ERROR: [source=" + failedDoc +"][value="+value +"][message="+details +"]");
  }

  protected void jobFinished(DocEvent evt) {
    Object source = evt.getSource();
    System.out.println("JOB_FINISHED: [source=" + source +"][value="+ evt.getValue() +"][details="+ evt.getDetails() +"]");
    System.out.println("---Job Finished---");
    System.out.println("Following docs processed.\n" + source);
    //int totalPageCount = getTotalPageCount();
    System.out.println("Pages processed: " + totalPages);
    long endTime = new Date().getTime();
    long elapsedTime = endTime - startTime;
    double pagesPerMillis = ((double)totalPages/(double)elapsedTime);
    double pagePerMinute = pagesPerMillis * 60000.00D;
    System.out.println("STATISTICS (Duration: " + elapsedTime +", Speed: " + (int)pagePerMinute + " pages/min)");
    if (failedDocs.size() > 0) {
      System.out.println(">>\nFollwings docs failed. \n" + failedDocs + "\n<<");
    }else {
      System.out.println("All docs successfully processed! [END jobId:" + jobId +"]");
    }
  }

}
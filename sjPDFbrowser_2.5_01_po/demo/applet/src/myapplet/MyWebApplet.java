/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
package myapplet;

import javax.swing.*;
import java.applet.AppletContext;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;

/**
 * This class demonstrates how you can PRINT and VIEW PDF documents from your web applet.
 */
public class MyWebApplet extends JApplet {
  protected String printPageName;
  protected String viewerPageName;

  protected String printPage;
  protected String viewerPage;

  public MyWebApplet() {
    setBackground(SystemColor.white);
  }

  public void init() { 
    //may be the page is else where to call from.
    printPage = getParameter("PRINT_PAGE");
    if (printPage == null || printPage.length() == 0) {
      printPage = null;
    }
    viewerPage = getParameter("VIEWER_PAGE");
    if (viewerPage == null || viewerPage.length() == 0) {
      viewerPage = null;
    }

    //If not else where then it is here relative to this applet URL code base.
    if (printPage == null || viewerPage == null) {
      String codeBase = getDocumentBase().toString();
      int idx = codeBase.lastIndexOf('/');
      String myAppletCodeBase = codeBase.substring(0, idx+1);
      printPage = myAppletCodeBase + printPageName;
      viewerPage = myAppletCodeBase + viewerPageName;
    }
    System.out.println("printPage=" + printPage);
    System.out.println("viewerPage=" + viewerPage);
  }

  public void start() {
    //Show couple of buttons in your applet or whatever appropriate
    //and attach action handler. From action handler call print() or view()
    //or call directly PdfPrinter or PdfViewer classes to print or view
    //the PDF

    //FOR DEMO hard code some DOC that we can PRINT or VIEW
    String docName = getParameter("DOCUMENT");
    if (docName == null) {
      docName = "";
    }

    final JTextField doc = new JTextField(docName);
    doc.setBackground(SystemColor.white);
    JButton print = new JButton("Print");
    print.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        print(printPage, doc.getText());
      }
    });

    JButton view = new JButton("View");
    view.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evt) {
        view(viewerPage, doc.getText());
      }
    });

    JPanel buttonPane = new JPanel(new FlowLayout(FlowLayout.LEFT, 5, 0));
    buttonPane.setBackground(SystemColor.white);
    buttonPane.add(print);
    buttonPane.add(view);

    JPanel top = new JPanel(new GridBagLayout());
    top.setBackground(SystemColor.white);
    top.add(doc, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));
    top.add(buttonPane, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

    setContentPane(top);
  }

  private void print(String printPageUrlString, String doc) {
    AppletContext ctx = getAppletContext();
    new Printer().print(ctx, doc, printPageUrlString);
  }

  private void view(String viewerPageUrlString, String doc) {
    AppletContext ctx = getAppletContext();
    new Viewer().view(ctx, doc, viewerPageUrlString);
  }
}





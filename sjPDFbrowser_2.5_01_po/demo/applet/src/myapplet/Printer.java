/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
package myapplet;

import java.applet.AppletContext;
import java.net.URL;

public class Printer {
  AppletContext context;
  public Printer() {}

  public void print(AppletContext appletContext, String doc, String printPageUrlString) {
    //this page is at same location from where MyWebApplet is loaded using some page.
    //You can use relative path if this page is located else where or can use an absolute URL path
    //e.g. http://host:port/app/printer_page.jsp
    String printUrlString = printPageUrlString;
    printUrlString += "?";
    printUrlString += "DOC_LIST=[" + doc + "]";
    printUrlString += "&JOB_NAME=printFromMyApplet";

    //have your PDF doc here dynamically
    URL docUrl = null;
    try {
      docUrl = new URL(printUrlString);
    }catch(Throwable t) {
      t.printStackTrace();
      //DO something else may be here.
    }
    appletContext.showDocument(docUrl , "print_window"); //just a new print window with this title
  }
}
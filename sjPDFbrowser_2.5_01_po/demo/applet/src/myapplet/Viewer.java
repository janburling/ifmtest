/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
package myapplet;

import java.applet.AppletContext;
import java.net.URL;

public class Viewer {
  public Viewer(){}

  public void view(AppletContext appletContext, String doc, String viewerPageUrlString) {
    String printUrlString = viewerPageUrlString;
    printUrlString += "?";
    printUrlString += "DOCUMENT=" + doc;
    //have your doc here dynamically
    URL docUrl = null;
    try {
      docUrl = new URL(printUrlString);
    }catch(Throwable t) {
      t.printStackTrace();
      //DO something else may be here.
    }
    appletContext.showDocument(docUrl , "viewer_window");
  }
}
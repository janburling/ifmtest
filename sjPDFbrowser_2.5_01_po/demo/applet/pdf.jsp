<%@ page import="java.io.IOException" %>
<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
  <link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
  <meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">

  <style type="text/css">
    body {
      margin-top: 5px;
      margin-left: 5px;
      margin-right: 5px;
      margin-bottom: 5px;
    }
  </style>

  <title>
    Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
  </title>
</head>

<BODY link="#009900" vlink="#009900" TEXT="#4b73af">

<%
  String document = request.getParameter("DOCUMENT");
  String codeBase = getCodeBaseURLString(request);
%>

<%!
  String getCodeBaseURLString(HttpServletRequest req) throws IOException {
    String contextPath = req.getContextPath();  //e.g. "/sample_app"
    //System.out.println("ctx path=" + contextPath);
    String protocol = req.getProtocol();
    if (protocol != null && protocol.equalsIgnoreCase("https")) {
      protocol = "https";
    } else if (protocol != null && protocol.equalsIgnoreCase("http")) {
      protocol = "http";
    } else {
      protocol = "http";
    }
    String host = req.getServerName();
    String port = ":" + req.getServerPort();
    if (port.equalsIgnoreCase(":80")) {
      port = ""; //need not show port 80
    }
    String baseUrlStr = protocol + "://" + host + port + contextPath; // + "/";
    return baseUrlStr;
  }
%>

<div class=square>
  <h2>Silent Print and View PDF from Browser - How to call from your applet</h2>
  <p>
    If you already have an applet program as a web application this applet sample demonstrated how you can call the
    print and view pages from your applet and print/view your PDF documents.
  </p>

    <p><strong>Source code</strong><br>
    This demo applet source code is in this browser edition archive
    at this location.
    <br><code><%=codeBase%>/demo/applet/src/myapplet/</code>
    <br>
    Feel free to look at it and find out how simple it is to integrate the
    silent print from browser edition with your existing applets.
    </p>


  <OBJECT
    codeBase="http://java.sun.com/products/plugin/autodl/jinstall-1_4_2_06-windows-i586.cab#Version=1,4,2,06"
    width="100%" height="65" classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93">
    <PARAM NAME="type" VALUE="application/x-java-applet;version=1.4.2">
    <PARAM NAME="name" VALUE="smartj">
    <PARAM NAME="alt" VALUE="PDF - Print and View from browser">
    <PARAM NAME="CODEBASE" VALUE=".">
    <PARAM NAME="CODE" VALUE="myapplet.MyWebApplet">
    <PARAM NAME="ARCHIVE" VALUE="myapplet.jar">
    <PARAM NAME="DOCUMENT" VALUE="<%=document%>">
    <PARAM NAME="PRINT_PAGE" VALUE="<%=codeBase%>/silent_print_pdf.jsp">
    <PARAM NAME="VIEWER_PAGE" VALUE="<%=codeBase%>/view_pdf.jsp">
    <param name="DEBUG" VALUE="true">
    <COMMENT>
      <EMBED
        type="application/x-java-applet;version=1.4.2"
        name="Java PDF Print"
        alt="PDF - Print and View PDF from browser"
        pluginspage="http://java.sun.com/j2se/"
        WIDTH="100%"
        HEIGHT="65"
        CODEBASE="."
        CODE="myapplet.MyWebApplet"
        ARCHIVE="myapplet.jar"
        DOCUMENT="<%=document%>"
        PRINT_PAGE="<%=codeBase%>/silent_print_pdf.jsp"
        VIEWER_PAGE="<%=codeBase%>/view_pdf.jsp"
        DEBUG="true"
        >

        <NOEMBED>
          No java runtime (please install a Java runtime and try again).
        </NOEMBED>
      </EMBED>
    </COMMENT>
  </OBJECT>

  <p/>
</div>

<hr size="0.5">
Copyright &#169; 2008 Activetree, Inc. All rights reserved. <br>
Web: <a href="http://www.activetree.com" class="bottomlinks1">http://www.activetree.com</a><br>
Email: sales@activetree.com<br>
Tel: +1 408 716 8414 Fax: +1 408 716 8450<br>

</body>
</html>

<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">
<style type="text/css">
  body {
    margin-top: 5px;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 5px;
  }

  td {
    color: "#4b73af";
  }
</style>

<title>
  Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>

<script type="text/javascript" language="javascript">

 function showMessage(msg) {
   if (msg=='') {
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
     return;
   }
   if (document.createTextNode){
     var mytext=document.createTextNode(msg+"\r");
     var dest = document.getElementById("status");
     if (dest) {
       dest.appendChild(mytext);
     }
   } else{
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
   }
}

function printWithParams(params) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    print_pane.innerHTML=output;
  }
}

function printMultipleWithParams(params, count) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    var newdiv = document.createElement('div');
    var newdiv_id = 'div_'+count;
    newdiv.innerHTML = output;
    print_pane.appendChild(newdiv);
  }
}

function enableField() {
  if (document.PRINT_PARAMS_FORM.AUTO_MATCH_PAPER.checked) {
    document.PRINT_PARAMS_FORM.PAPER.disabled = true;
  } else {
    document.PRINT_PARAMS_FORM.PAPER.disabled = false;
  }
}


function print() {
  //collect all the user choices
  var user_pdf_url = document.PRINT_PARAMS_FORM.user_pdf_url.value;
  //alert('user_pdf_url='+user_pdf_url);

  var print_location = getPrintLocation(document.PRINT_PARAMS_FORM.print_locations);
  //alert('print_location='+print_location);

  var pdfs = getPdfs(user_pdf_url,print_location);
  //alert('DOC_LIST='+pdfs);
  //////Got Docs until here.

  var code_base=document.PRINT_PARAMS_FORM.code_base.value;
  //alert('code_base='+code_base);

  var printer_name = document.PRINT_PARAMS_FORM.PRINTER_NAME.value;
  //alert('printer_name='+printer_name);

  var paper = document.PRINT_PARAMS_FORM.PAPER.value;
  //alert('paper='+paper);

  var auto_match_paper = document.PRINT_PARAMS_FORM.AUTO_MATCH_PAPER.checked;
  //alert('auto_match_paper='+auto_match_paper);

  var printer_name_substring_match = document.PRINT_PARAMS_FORM.PRINTER_NAME_SUBSTRING_MATCH.checked;
  //alert('printer_name_substring_match='+printer_name_substring_match);

  var copies = document.PRINT_PARAMS_FORM.COPIES.value;
  //alert('copies='+copies);

  var show_printer_dialog = document.PRINT_PARAMS_FORM.SHOW_PRINT_DIALOG.checked;
  //alert('show_printer_dialog='+show_printer_dialog);

  var job_name = document.PRINT_PARAMS_FORM.JOB_NAME.value;
  //alert('job_name='+job_name);

  var pageScaling = document.PRINT_PARAMS_FORM.PAGE_SCALING.value;
  //alert('pageScaling='+pageScaling);

  var autoRotateAndCenter = document.PRINT_PARAMS_FORM.AUTO_ROTATE_AND_CENTER.checked;
  //alert('autoRotateAndCenter='+autoRotateAndCenter);

  var is_use_printer_margins = document.PRINT_PARAMS_FORM.IS_USE_PRINTER_MARGINS.checked;
  //alert('is_use_printer_margins='+is_use_printer_margins);

  var single_print_job = document.PRINT_PARAMS_FORM.SINGLE_PRINT_JOB.checked;
  //alert('single_print_job='+single_print_job);

  var collate_copies = document.PRINT_PARAMS_FORM.COLLATE_COPIES.checked;
  //alert('collate_copies='+collate_copies);

  var show_print_error_message = document.PRINT_PARAMS_FORM.SHOW_PRINT_ERROR_DIALOG.checked;
  //alert('show_print_error_message='+show_print_error_message);

  var is_debug = document.PRINT_PARAMS_FORM.DEBUG.checked;
  //alert('is_debug='+is_debug);

  //password
  var password = document.PRINT_PARAMS_FORM.PASSWORD.value;
  //alert('password='+password);

  //url authentication
  var urlAuthId = document.PRINT_PARAMS_FORM.URL_AUTH_ID.value;
  //alert('urlAuthId='+urlAuthId);

  //url auth password
  var urlAuthPassword = document.PRINT_PARAMS_FORM.URL_AUTH_PASSWORD.value;
  //alert('urlAuthPassword='+urlAuthPassword);

  var printQuality=document.PRINT_PARAMS_FORM.PRINT_QUALITY.value;
  //alert('printQuality='+printQuality);

  var side=document.PRINT_PARAMS_FORM.SIDE_TO_PRINT.value;
  //alert('sideToPrint='+side);

  var statusUpdateEnabled=document.PRINT_PARAMS_FORM.STATUS_UPDATE_ENABLED.checked;
  //alert('statusUpdateEnabled='+statusUpdateEnabled);

  //Collect all
  var params = code_base+'silent_print_pdf.asp' +
               '?DOC_LIST=' + pdfs +
               '&PRINTER_NAME=' + printer_name +
               '&PAPER=' + paper +
               '&AUTO_MATCH_PAPER=' + auto_match_paper +
               '&PRINTER_NAME_SUBSTRING_MATCH=' + printer_name_substring_match +
               '&COPIES=' + copies +
               '&SHOW_PRINT_DIALOG=' + show_printer_dialog +
               '&JOB_NAME=' + job_name +
               '&PAGE_SCALING=' + pageScaling +
               '&AUTO_ROTATE_AND_CENTER=' + autoRotateAndCenter +
               '&IS_USE_PRINTER_MARGINS=' + is_use_printer_margins +
               '&SINGLE_PRINT_JOB=' + single_print_job +
               '&COLLATE_COPIES=' + collate_copies +
               '&SHOW_PRINT_ERROR_DIALOG=' + show_print_error_message +
               '&DEBUG=' + is_debug +
               '&PASSWORD=' + password +
               '&URL_AUTH_ID=' + urlAuthId +
               '&URL_AUTH_PASSWORD=' + urlAuthPassword +
               '&PRINT_QUALITY=' + printQuality +
               '&SIDE_TO_PRINT=' + side +
               "&STATUS_UPDATE_ENABLED=" + statusUpdateEnabled;
  //alert('params='+params);


  var output = "<IFRAME src='" + params + "' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='100%' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  //alert('output='+output);

  //Now call the script with the params.
  document.getElementById("print_pane").innerHTML = output;
}

function getPrintLocation(locations) {
  for (i = 0; i < locations.length; i++) {
    if (locations[i].checked == true) {
      //alert('location='+locations[i].value);
      return locations[i].value;
    }
  }
}

function getPdfs(user_pdf_url,print_location) {
  var s = '';
  if (print_location == 1 || print_location == 3) {
    if (user_pdf_url != null || user_pdf_url.length != 0) {
      s += '[' + user_pdf_url + ']';
    }
  }
  var pdfs = document.PRINT_PARAMS_FORM.pdfs;
  if (print_location == 2 || print_location == 3) {
    for (i = 0; i < pdfs.length; i++) {
      var apdf = pdfs[i];
      if (apdf.selected) {
        s += '[';
        s += apdf.value;
        s += ']';
      }
    }
  }
  return s;
}

</script>

</head>

<BODY link="#009900" vlink="#009900" TEXT="#4b73af">

<%
  codeBase = "../"
%>

<table border="0" width="800" cellspacing="0" cellpadding="0">
<tr>
<td>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tr bgcolor="white">
  <td colspan="2">
      <h2>Java PDF Silent Print and PDF Viewer</h2>
      Silent print and view existing PDF documents from web browser, standalone
         and Java/J2EE applications.   <p></p>
  </td>
</tr>
<FORM name="PRINT_PARAMS_FORM" action="<%=codeBase%>silent_print_pdf.asp" method="GET" target="print_message_frame">
<input type="hidden" name="code_base" value="<%=codeBase%>">
<tr>
  <td colspan="2">
    <fieldset>
      <legend>Enter/Select PDFs</legend>

      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="2">
            <i>[Examples:</i> Web URL: http://www.mycompany.com/pdf/sample.pdf;
            Local file URL (Windows): file:/c:/pdf/sample.pdf ]
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <input type="text" name="user_pdf_url" value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample1.pdf" size="104">
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <input type="radio" name="print_locations" value="1" checked>Print only above PDF
            <input type="radio" name="print_locations" value="2">Print only from selected URLs below
            <input type="radio" name="print_locations" value="3">Print PDF from both locations
          </td>
        </tr>

        <tr>
          <td colspan="2">
            <select multiple name="pdfs" id="pdfs" size="3">
              <option
                value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample2.pdf"
                selected>http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample2.pdf
              </option>
              <option value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample3.pdf">
                http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample3.pdf
              </option>
              <option
                value="http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample4.pdf">
                http://www.activetree.com/silent_print_pdf_from_browser/demo/sample_pdf/sample4.pdf
              </option>
            </select>
          </td>
        </tr>

      </table>

    </fieldset>
  </td>
</tr>

<tr>
  <td width="50%"></td>
  <td height="2">
    <label id="print_pane"></label>
  </td>
</tr>

<tr>
<td colspan="2">
<fieldset>
<legend>Printing options</legend>

<table border="0" cellpadding="" cellspacing="2">
<tr>
<td>
  Enter printer name to print (if none entered or no match is found it will print to the default printer)
  <input type="text" name="PRINTER_NAME" value="" size="20">
</td>
<td>
Select a paper
<select name="PAPER">
<option value="(0,0,0,0,612,792)" selected>NA Letter [(8.5,11.0) inch, (612,792) pixels]</option>
<option value="(0,0,0,0,612,1008)">NA Legal [(8.5,14.0) inch, (612,1008) pixels]</option>
<option value="(0,0,0,0,720,936)">NA 10x13 Envelope [(10.0,13.0) inch, (720,936) pixels]</option>
<option value="(0,0,0,0,720,1008)">NA 10x14 Envelope [(10.0,14.0) inch, (720,1008) pixels]</option>
<option value="(0,0,0,0,720,1080)">NA 10x15 Envelope [(10.0,15.0) inch, (720,1080) pixels]</option>
<option value="(0,0,0,0,360,504)">NA 5x7 [(5.0,7.0) inch, (360,504) pixels]</option>
<option value="(0,0,0,0,432,648)">NA 6x9 Envelope [(6.0,9.0) inch, (432,648) pixels]</option>
<option value="(0,0,0,0,504,648)">NA 7x9 Envelope [(7.0,9.0) inch, (504,648) pixels]</option>
<option value="(0,0,0,0,576,720)">NA 8x10 [(8.0,10.0) inch, (576,720) pixels]</option>
<option value="(0,0,0,0,648,792)">NA 9x11 Envelope [(9.0,11.0) inch, (648,792) pixels]</option>
<option value="(0,0,0,0,648,864)">NA 9x12 Envelope [(9.0,12.0) inch, (648,864) pixels]</option>
<option value="(0,0,0,0,297,684)">NA Number 10 Envelope [(4.12,9.5) inch, (297,684) pixels]</option>
<option value="(0,0,0,0,324,747)">NA Number 11 Envelope [(4.5,10.37) inch, (324,747) pixels]</option>
<option value="(0,0,0,0,342,792)">NA Number 12 Envelope [(4.75,11.0) inch, (342,792) pixels]</option>
<option value="(0,0,0,0,360,828)">NA Number 14 Envelope [(5.0,11.5) inch, (360,828) pixels]</option>
<option value="(0,0,0,0,279,639)">NA Number 9 Envelope [(3.87,8.87) inch, (279,639) pixels]</option>
<option value="(0,0,0,0,595,841)">ISO A4 [(8.26,11.69) inch, (595,841) pixels]</option>
<option value="(0,0,0,0,2383,3370)">ISO A0 [(33.11,46.81) inch, (2383,3370) pixels]</option>
<option value="(0,0,0,0,1683,2383)">ISO A1 [(23.38,33.11) inch, (1683,2383) pixels]</option>
<option value="(0,0,0,0,73,104)">ISO A10 [(1.02,1.45) inch, (73,104) pixels]</option>
<option value="(0,0,0,0,1190,1683)">ISO A2 [(16.53,23.38) inch, (1190,1683) pixels]</option>
<option value="(0,0,0,0,1190,1683)">ISO A2 [(16.53,23.38) inch, (1190,1683) pixels]</option>
<option value="(0,0,0,0,841,1190)">ISO A3 [(11.69,16.53) inch, (841,1190) pixels]</option>
<option value="(0,0,0,0,419,595)">ISO A5 [(5.82,8.26) inch, (419,595) pixels]</option>
<option value="(0,0,0,0,297,419)">ISO A6 [(4.13,5.82) inch, (297,419) pixels]</option>
<option value="(0,0,0,0,209,297)">ISO A7 [(2.91,4.13) inch, (209,297) pixels]</option>
<option value="(0,0,0,0,147,209)">ISO A8 [(2.04,2.91) inch, (147,209) pixels]</option>
<option value="(0,0,0,0,104,147)">ISO A9 [(1.45,2.04) inch, (104,147) pixels]</option>
<option value="(0,0,0,0,2834,4008)">ISO B0 [(39.37,55.66) inch, (2834,4008) pixels]</option>
<option value="(0,0,0,0,2004,2834)">ISO B1 [(27.83,39.37) inch, (2004,2834) pixels]</option>
<option value="(0,0,0,0,87,124)">ISO B10 [(1.22,1.73) inch, (87,124) pixels]</option>
<option value="(0,0,0,0,1417,2004)">ISO B2 [(19.68,27.83) inch, (1417,2004) pixels]</option>
<option value="(0,0,0,0,1000,1417)">ISO B3 [(13.89,19.68) inch, (1000,1417) pixels]</option>
<option value="(0,0,0,0,708,1000)">ISO B4 [(9.84,13.89) inch, (708,1000) pixels]</option>
<option value="(0,0,0,0,498,708)">ISO B5 [(6.92,9.84) inch, (498,708) pixels]</option>
<option value="(0,0,0,0,354,498)">ISO B6 [(4.92,6.92) inch, (354,498) pixels]</option>
<option value="(0,0,0,0,249,354)">ISO B7 [(3.46,4.92) inch, (249,354) pixels]</option>
<option value="(0,0,0,0,147,209)">ISO A8 [(2.04,2.91) inch, (147,209) pixels]</option>
<option value="(0,0,0,0,124,175)">ISO B9 [(1.73,2.44) inch, (124,175) pixels]</option>
<option value="(0,0,0,0,2599,3676)">ISO C0 [(36.10,51.06) inch, (2599,3676) pixels]</option>
<option value="(0,0,0,0,1836,2599)">ISO C1 [(25.51,36.10) inch, (1836,2599) pixels]</option>
<option value="(0,0,0,0,1298,1836)">ISO C2 [(18.03,25.51) inch, (1298,1836) pixels]</option>
<option value="(0,0,0,0,918,1298)">ISO C3 [(12.75,18.03) inch, (918,1298) pixels]</option>
<option value="(0,0,0,0,649,918)">ISO C4 [(9.01,12.75) inch, (649,918) pixels]</option>
<option value="(0,0,0,0,459,649)">ISO C5 [(6.37,9.01) inch, (459,649) pixels]</option>
<option value="(0,0,0,0,323,459)">ISO C6 [(4.48,6.37) inch, (323,459) pixels]</option>
<option value="(0,0,0,0,311,623)">ISO Designated Long [(4.33,8.66) inch, (311,623) pixels]</option>
<option value="(0,0,0,0,612,792)">Engineering A [(8.5,11.0) inch, (612,792) pixels]</option>
<option value="(0,0,0,0,792,1224)">Engineering B [(11.0,17.0) inch, (792,1224) pixels]</option>
<option value="(0,0,0,0,1224,1584)">Engineering C [(17.0,22.0) inch, (1224,1584) pixels]</option>
<option value="(0,0,0,0,1584,2448)">Engineering D [(22.0,34.0) inch, (1584,2448) pixels]</option>
<option value="(0,0,0,0,2448,3168)">Engineering E [(34.0,44.0) inch, (2448,3168) pixels]</option>
<option value="(0,0,0,0,522,756)">Executive [(7.25,10.5) inch, (522,756) pixels]</option>
<option value="(0,0,0,0,612,936)">Folio [(8.5,13.0) inch, (612,936) pixels]</option>
<option value="(0,0,0,0,396,612)">Invoice [(5.5,8.5) inch, (396,612) pixels]</option>
<option value="(0,0,0,0,311,651)">Italian Envelope [(4.33,9.05) inch, (311,651) pixels]</option>
<option value="(0,0,0,0,792,1224)">Ledger [(11.0,17.0) inch, (792,1224) pixels]</option>
<option value="(0,0,0,0,278,540)">Monarch Envelope [(3.86,7.5) inch, (278,540) pixels]</option>
<option value="(0,0,0,0,261,468)">Personal Envelope [(3.62,6.5) inch, (261,468) pixels]</option>
<option value="(0,0,0,0,612,779)">Quarto [(8.5,10.82) inch, (612,779) pixels]</option>
<option value="(0,0,0,0,419,566)">Oufuko Postcard [(5.82,7.87) inch, (419,566) pixels]</option>
<option value="(0,0,0,0,283,419)">Japanese Postcard [(3.93,5.82) inch, (283,419) pixels]</option>
<option value="(0,0,0,0,2919,4127)">JIS B0 [(40.55,57.32) inch, (2919,4127) pixels]</option>
<option value="(0,0,0,0,2063,2919)">JIS B1 [(28.66,40.55) inch, (2063,2919) pixels]</option>
<option value="(0,0,0,0,90,127)">JIS B10 [(1.25,1.77) inch, (90,127) pixels]</option>
<option value="(0,0,0,0,1459,2063)">JIS B2 [(20.27,28.66) inch, (1459,2063) pixels]</option>
<option value="(0,0,0,0,1031,1459)">JIS B3 [(14.33,20.27) inch, (1031,1459) pixels]</option>
<option value="(0,0,0,0,728,1031)">JIS B4 [(10.11,14.33) inch, (728,1031) pixels]</option>
<option value="(0,0,0,0,515,728)">JIS B5 [(7.16,10.11) inch, (515,728) pixels]</option>
<option value="(0,0,0,0,362,515)">JIS B6 [(5.03,7.16) inch, (362,515) pixels]</option>
<option value="(0,0,0,0,257,362)">JIS B7 [(3.58,5.03) inch, (257,362) pixels]</option>
<option value="(0,0,0,0,181,257)">JIS B8 [(2.51,3.58) inch, (181,257) pixels]</option>
<option value="(0,0,0,0,127,181)">JIS B9 [(1.77,2.51) inch, (127,181) pixels]</option>
<option value="(0,0,0,0,402,941)">JIS Chou (142mm x 332mm) [(5.59,13.07) inch, (402,941) pixels]</option>
<option value="(0,0,0,0,337,785)">JIS Chou (119mm x 277mm) [(4.68,10.90) inch, (337,785) pixels]</option>
<option value="(0,0,0,0,340,666)">JIS Chou (120mm x 235mm) [(4.72,9.25) inch, (340,666) pixels]</option>
<option value="(0,0,0,0,255,581)">JIS Chou (90mm x 205mm) [(3.54,8.07) inch, (255,581) pixels]</option>
<option value="(0,0,0,0,260,666)">JIS Chou (92mm x 235mm) [(3.62,9.25) inch, (260,666) pixels]</option>
<option value="(0,0,0,0,255,637)">JIS Chou (90mm x 225mm) [(3.54,8.85) inch, (255,637) pixels]</option>
<option value="(0,0,0,0,813,1082)">JIS Kaku (287mm x 382mm) [(11.29,15.03) inch, (813,1082) pixels]</option>
<option value="(0,0,0,0,765,1082)">JIS Kaku (270mm x 382mm) [(10.62,15.03) inch, (765,1082) pixels]</option>
<option value="(0,0,0,0,680,941)">JIS Kaku (240mm x 332mm) [(9.44,13.07) inch, (680,941) pixels]</option>
<option value="(0,0,0,0,612,785)">JIS Kaku (216mm x 277mm [(8.50,10.90) inch, (612,785) pixels]</option>
<option value="(0,0,0,0,558,756)">JIS Kaku (197mm x 267mm) [(7.75,10.51) inch, (558,756) pixels]</option>
<option value="(0,0,0,0,538,680)">JIS Kaku (190mm x 240mm) [(7.48,9.44) inch, (538,680) pixels]</option>
<option value="(0,0,0,0,459,649)">JIS Kaku (162mm x 229mm) [(6.37,9.01) inch, (459,649) pixels]</option>
<option value="(0,0,0,0,402,581)">JIS Kaku (142mm x 205mm) [(5.59,8.07) inch, (402,581) pixels]</option>
<option value="(0,0,0,0,337,558)">JIS Kaku (119mm x 197mm) [(4.68,7.75) inch, (337,558) pixels]</option>
<option value="(0,0,0,0,649,918)">JIS Kaku (229mm x 324mm) [(9.01,12.75) inch, (649,918) pixels]</option>
<option value="(0,0,0,0,646,884)">JIS Kaku (228mm x 312mm) [(8.97,12.28) inch, (646,884) pixels]</option>
<option value="(0,0,0,0,340,498)">JIS You (120mm x 176mm) [(4.72,6.92) inch, (340,498) pixels]</option>
<option value="(0,0,0,0,323,459)">JIS You (114mm x 162mm) [(4.48,6.37) inch, (323,459) pixels]</option>
<option value="(0,0,0,0,277,419)">JIS You (98mm x 148mm) [(3.85,5.82) inch, (277,419) pixels]</option>
<option value="(0,0,0,0,297,666)">JIS You (105mm x 235mm) [(4.13,9.25) inch, (297,666) pixels]</option>
<option value="(0,0,0,0,269,615)">JIS You (95mm x 217mm) [(3.74,8.54) inch, (269,615) pixels]</option>
<option value="(0,0,0,0,277,538)">JIS You (98mm x 190mm [(3.85,7.48) inch, (277,538) pixels]</option>
<option value="(0,0,0,0,260,467)">JIS You (92mm x 165mm) [(3.62,6.49) inch, (260,467) pixels]</option>
<option value="(0,0,0,0,649,864)">Architecture A [(9.01,12.00) inch, (649,864) pixels]</option>
<option value="(0,0,0,0,864,1295)">Architecture B [(12.00,17.99) inch, (864,1295) pixels]</option>
<option value="(0,0,0,0,1295,1729)">Architecture C [(17.99,24.01) inch, (1295,1729) pixels]</option>
<option value="(0,0,0,0,1729,2590)">Architecture D [(24.01,35.98) inch, (1729,2590) pixels]</option>
<option value="(0,0,0,0,2590,3455)">Architecture E [(35.98,47.99) inch, (2590,3455) pixels]</option>
<option value="(0,0,0,0,2160,3024)">Architecture E1 [(30.0,42.00) inch, (2160,3024) pixels]</option>
<option value="(0,0,0,0,4767,6740)">DIN 4A0 (1682mm x 2378mm) [(66.22,93.62) inch, (4767,6740) pixels]</option>
<option value="(0,0,0,0,3370,4767)">DIN 2A0 (1189mm x 1682mm) [(46.81,66.22) inch, (3370,4767) pixels]</option>
<!--
Add more papers using this format; value parameter must be like "(widthPixel,heightPixel)"
and description can be anything of your choice for the users.
-->
</select>
<br>
<input type="checkbox" name="AUTO_MATCH_PAPER" onClick="enableField()">
Choose paper source by PDF page size? (defaults to LETTER)
</td>
</tr>

<tr>
  <td>
    <input type="checkbox" name="PRINTER_NAME_SUBSTRING_MATCH" checked>
    Enable partial printer name matching?
  </td>
  <td>
    Number of copies to print <input type="text" name="COPIES" value="1" size="2">
  </td>
</tr>

<tr>
  <td>
    <input type="checkbox" name="SHOW_PRINT_DIALOG"> Show printer and paper selection window?
  </td>
  <td>
    Print job name <input type="text" name="JOB_NAME" value="PDFPrintJob" size="20">
  </td>
</tr>

<tr>
  <td>
    Page Scaling:
    <select name="PAGE_SCALING">
      <option value="NONE">None</option>
      <option value="FIT_TO_PRINTABLE_AREA" selected>Fit To Printable Area</option>
      <option value="SHRINK_TO_PRINTABLE_AREA">Shrink to Printable Area</option>
    </select>
  </td>
  <td>
    <input type="checkbox" name="AUTO_ROTATE_AND_CENTER" checked> Auto rotate and center?  <br>
    <input type="checkbox" name="IS_USE_PRINTER_MARGINS" checked> Use printer margins?
  </td>
</tr>

<tr>
  <td>
    <input type="checkbox" name="SINGLE_PRINT_JOB" checked> Print as a single print job (for multiple PDFs)?
  </td>
  <td>
    <input type="checkbox" name="COLLATE_COPIES" checked> Collate copies (if more than one copies)
  </td>
</tr>

<tr>
  <td>
    <input type="checkbox" name="SHOW_PRINT_ERROR_DIALOG"> Show print error messages if any?
  </td>
  <td>
    <input type="checkbox" name="DEBUG" checked> Show console debug messages while printing?
  </td>
</tr>

<tr>
  <td>
    Doc password: <input type="password" name="PASSWORD" value="" size="20">
  </td>
  <td>
    Print quality
    <select name="PRINT_QUALITY">
      <option value="HIGH" selected>High</option>
      <option value="NORMAL">Normal</option>
      <option value="DRAFT">Draft</option>
    </select>
  </td>
</tr>

<tr>
  <td>
    Media side
    <select name="SIDE_TO_PRINT">
      <option value="ONE_SIDED" selected>One Sided</option>
      <option value="DUPLEX">Duplex</option>
      <option value="TUMBLE">Tumble</option>
      <option value="TWO_SIDED_LONG_EDGE">Two sided long edge</option>
      <option value="TWO_SIDED_SHORT_EDGE">Two sided short edge</option>
    </select>
  </td>
  <td>
    <input type="checkbox" name="STATUS_UPDATE_ENABLED" checked> Printing status messages enabled?
  </td>
</tr>

<tr>
  <td>
    URL Authentication ID: <input type="text" name="URL_AUTH_ID" value="" size="20">
  </td>
  <td>
    URL Authentication Password: <input type="password" name="URL_AUTH_PASSWORD" value="" size="20">
  </td>
</tr>
</table>

</fieldset>
</td>
</tr>

<tr>
  <td height="5">
  </td>
</tr>

<tr>
  <td colspan="2">
    <center>
      <input type="button" name="printButton" value="Print selected PDF documents" onClick="javascript:print();">
    </center>
  </td>
</tr>

  <tr>
    <td >
      <fieldset>
        <legend>Print status </legend>
        <textarea id="status" rows="6" cols="96"  readonly="true"></textarea>
      </fieldset>
    </td>
  </tr>

  
<tr width="100%">
  <td width="100%" >
    <label id="print_pane"></label>
  </td>
</tr>

</FORM>
</table>
</td>
</tr>

<tr>
  <td>
    <hr size="0.5">
      Copyright &#169; 2008 Activetree, Inc. All rights reserved. <br>
      Web: <a href="http://www.activetree.com" class="bottomlinks1">http://www.activetree.com</a><br>
      Email: sales@activetree.com<br>
      Tel: +1 408 716 8414 Fax: +1 408 716 8450<br>
  </td>
</tr>

</table>

</body>
</html>

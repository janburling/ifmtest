<%@ page import="java.io.IOException" %>
<!--
/**
 * Copyright � by ActiveTree Inc., California, USA.
 * All rights reserved.
 */
 -->
<%@ page language="java" contentType="text/html" %>

<html>
<head>
<link REL="stylesheet" HREF="http://www.activetree.com/lib/styles.css" TYPE="text/css">
<meta name="description" content="Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java">

<style type="text/css">
  body {
    margin-top: 5px;
    margin-left: 5px;
    margin-right: 5px;
    margin-bottom: 5px;
  }
</style>
<title>
  Java PDF Print | Java PDF view| print PDF silent| view PDF browser| print PDF browser| view PDF without Adobe Acrobat Reader| print PDF without Adobe Acrobat Reader| Java PDF Print Library | Print PDF with Java | Java PDF silent print | Java PDF print from browser | Java PDF print from URL | Print Dynamic PDF generated from an URL using Java | Java PDF Viewer| Java PDF View | View PDF with Java | Java PDF Viewing API | PDF Viewer Applet | View PDF from Browser| Print PDF from Browser | Silent Print PDF from Browser | Print/View PDF from Java
</title>

<script type="text/javascript" language="javascript">

 function showMessage(msg) {
   if (msg=='') {
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
     return;
   }
   if (document.createTextNode){
     var mytext=document.createTextNode(msg+"\r");
     var dest = document.getElementById("status");
     if (dest) {
       dest.appendChild(mytext);
     }
   } else{
     var dest = document.getElementById("status");
     if (dest) {
       dest.innerHTML=msg;
     }
   }
}

function printWithParams(params) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    print_pane.innerHTML=output;
  }
}

function printMultipleWithParams(params, count) {
  var output="<IFRAME src='"+params+"' id='print_message_frame' name='print_message_frame' marginwidth='0' marginheight='0' width='100%' height='0' hspace='0' vspace='0' frameborder='0' halign='left' valign='top' scrolling='no'> </IFRAME>";
  var print_pane = document.getElementById("print_pane");
  if (print_pane) {
    var newdiv = document.createElement('div');
    var newdiv_id = 'div_'+count;
    newdiv.innerHTML = output;
    print_pane.appendChild(newdiv);
  }
}

</script>

</head>
<BODY link="#009900" vlink="#009900" TEXT="#4b73af" >

<?php
$paramString="&JOB_NAME=PDFPrintJob";
$paramString.="&PAGE_SCALING=FIT_TO_PRINTABLE_AREA&AUTO_ROTATE_AND_CENTER=true";
$paramString.="&SHOW_PRINT_ERROR_DIALOG=false&AUTO_MATCH_PAPER=true&DEBUG=true";
$paramString.="&PRINTER_NAME=&PRINTER_NAME_SUBSTRING_MATCH=false";
$paramString.="&PAPER=&COPIES=1&SHOW_PRINT_DIALOG=false";
$paramString.="&IS_USE_PRINTER_MARGINS=true&SINGLE_PRINT_JOB=true";
$paramString.="&PASSWORD=&COLLATE_COPIES=false";
$paramString.="&PRINT_QUALITY=HIGH&SIDE_TO_PRINT=ONE_SIDED&STATUS_UPDATE_ENABLED=true";

$codeBase="../";
?>


<table border="0" width="800" cellpadding="0" cellspacing="0">
  <tr bgcolor="white">
  <td>
      <h2>Java PDF Silent Print and PDF Viewer</h2>
      Silent print and view existing PDF documents from web browser, standalone and Java/J2EE applications.
  </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <tr>
    <td><fieldset>
         <legend>Print and View PDF from InputStream, Database, byte[] or other source</legend>

  <p>
  <b>Silent print and view dynamically generated PDF from browser</b> - e.g. InputStream, Database or PDF byte[] content from other
    source which can not be referred as an URL directly from the browser. Instead they can be accessed through a server program
    (i.e. an URL) in order to access those kind of PDFs.
    </p>
    <p>This requires you to write a server program to dynamically generated the PDF
    content and write it back to the browser. This still is no different than above two examples just because it is indeed
    an URL and an URL is all that the API needs.
    </p>
    <p>
    <b>Examples:</b> Typical example may be that you have a FORM requires data to be feed to it and then generate a PDF. Document content
    may be stored in a database which you can refer as an existing PDF but you need a server program like this one to
    retrieve the content and wrtie back to browser. The same is true for every other cases when your document is NOT an
    already existing one.
    </p>
<li>
  <b>Silent print PDF from "InputStream, Database, byte[] or other source"</b>
(<a href="javascript:printWithParams('<?php echo "${codeBase}"; ?>silent_print_pdf.php?DOC_LIST=[<?php echo "${codeBase}"; ?>demo/stream_print_demo.php]<?php echo "${paramString}"; ?>');"
   class="bottomlinks1">
  Silent Print</a>) or
  (<a class="bottomlinks1" href="javascript:"
      onClick="window.open('<?php echo "${codeBase}"; ?>view_pdf.php?DOCUMENT=[<?php echo "${codeBase}"; ?>demo/stream_print_demo.php]','','width=880,height=650,resizable=yes,scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no'); return false;">View</a>)
</li>
</fieldset>
</td>
</tr>

  <tr>
    <td >
      <fieldset>
        <legend>Print status </legend>
        <textarea id="status" rows="6" cols="96"  readonly="true"></textarea>
      </fieldset>
    </td>
  </tr>
  
  <tr width="100%">
    <td width="100%" >
      <label id="print_pane"></label>
    </td>
  </tr>

<tr>
  <td>
    <hr size="0.5">
      Copyright &#169; 2008 Activetree, Inc. All rights reserved. <br>
      Web: <a href="http://www.activetree.com" class="bottomlinks1">http://www.activetree.com</a><br>
      Email: sales@activetree.com<br>
      Tel: +1 408 716 8414 Fax: +1 408 716 8450<br>
  </td>
</tr>
</table>


</body>
</html>

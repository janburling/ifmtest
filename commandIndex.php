<?php
defined('YII_DEBUG') or define('YII_DEBUG',true);
// include Yii bootstrap file
$yii	= dirname(__FILE__).'/../yii-1.1.14.f0fee9/framework/yii.php';
$config	= dirname(__FILE__) . '/protected/config/console.php';

require_once($yii);

// add path
$path = dirname(__FILE__) . "/protected/commands";
set_include_path( get_include_path() . PATH_SEPARATOR . $path );
$path = dirname(__FILE__) . "/protected/components";
set_include_path( get_include_path() . PATH_SEPARATOR . $path );

// create application instance and run
Yii::createApplication( 'PHConsoleApplication' , $config)->run();

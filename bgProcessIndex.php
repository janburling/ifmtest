<?php
defined('YII_DEBUG') or define('YII_DEBUG',true);
// include Yii bootstrap file
$yii	= dirname(__FILE__) . '/../yii_113/framework/yii.php';
$config	= dirname(__FILE__) . '/protected/config/console.php';

//echo "yii: $yii\n";
//exit;
require_once($yii);

// add path
$path = dirname(__FILE__) . "/protected/commands";
set_include_path( get_include_path() . PATH_SEPARATOR . $path );

// create application instance and run
Yii::createApplication( 'PHConsoleApplication' , $config)->run();

